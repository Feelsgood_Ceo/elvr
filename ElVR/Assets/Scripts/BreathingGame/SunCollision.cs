﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunCollision : MonoBehaviour {

    private SkyboxControl skyboxControl;

    private void Start()
    {
        skyboxControl = GameObject.Find("SkyboxControl").GetComponent<SkyboxControl>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collided with me: " + this.name);
        ChangeScenarioLighting(gameObject.name);
    }

    /// <summary>
    /// Changes scenario lighting after collision
    /// </summary>
    /// <param name="objectName">Object which collided with the sun</param>
    private void ChangeScenarioLighting(string objectName) {

        if (objectName == "sunrise")
        {
            Debug.Log("Sunrise");
            skyboxControl.SendMessage("IncreaseExposure", true);
        }

        if (objectName == "sunset")
        {
            Debug.Log("Sunset");
            skyboxControl.SendMessage("DecreaseExposure", true);
        }
    }
}
