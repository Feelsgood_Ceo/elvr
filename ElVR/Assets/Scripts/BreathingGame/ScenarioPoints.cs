﻿public enum ScenarioPoints{
    Bosque = 1,
    Mar = 2,
    Escenario3 = 9,
    Escenario4 = 10,
    Escenario5 = 11,
    Escenario6 = 12,
    Escenario7 = 13,
    Escenario8 = 14,
    Escenario9 = 15,
    Escenario10 = 16,
    Escenario11 = 17,
    Escenario12 = 18
}