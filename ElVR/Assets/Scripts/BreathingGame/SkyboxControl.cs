﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxControl : MonoBehaviour {

    public Transform climateOrbit;
    public ClimaControl climateController;

    private float lightingChangeFactor;
    private float newExposureValue;
    private float newAmbientIntensityValue;

    private bool increaseExposure;
    private bool decreaseExposure;

    // Use this for initialization
    void Awake () {
        InitializeController();
	}
	
	// Update is called once per frame
	void FixedUpdate() {
        //ChangeExposureWithRotation();
        ChangeExposureValue();
    }

    private void OnApplicationQuit()
    {
        SetSkyboxExposureValue(1.0f);
        SetAmbientIntensityValue(1.0f);
    }

    /// <summary>
    /// Changes the current exposure value
    /// </summary>
    /// <param name="value">New exposure value</param>
    private void SetSkyboxExposureValue(float value) {
        RenderSettings.skybox.SetFloat("_Exposure", value);
    }

    /// <summary>
    /// Returns current exposure value
    /// </summary>
    /// <returns>Current exposure float value</returns>
    private float GetSkyboxExposureValue() {
        return RenderSettings.skybox.GetFloat("_Exposure");
    }

    /// <summary>
    /// Changes the current Environmental Lighting Intensity Multiplier
    /// </summary>
    /// <param name="value">New intensity value</param>
    private void SetAmbientIntensityValue(float value) {
        RenderSettings.ambientIntensity = value;
    }

    /// <summary>
    /// Returns current Environmental Lighting Intensity Multiplier
    /// </summary>
    /// <returns>Current intensity float value</returns>
    private float GetAmbientIntensityValue() {
        return RenderSettings.ambientIntensity;
    }

    /// <summary>
    /// Gets current orbit rotation
    /// </summary>
    /// <returns>Rotation in euler angles</returns>
    private float GetOrbitRotation() {
        return climateOrbit.rotation.eulerAngles.x;
    }


    /// <summary>
    /// Initializes skybox control
    /// </summary>
    private void InitializeController() {

        increaseExposure = false;

        SetSkyboxExposureValue(1.0f);

        lightingChangeFactor = climateController.Incrementador / 100.0f;
    }

    /// <summary>
    /// Changes exposure according to a local orbit rotation
    /// </summary>
    void ChangeExposureWithRotation() {
        if (GetOrbitRotation() >= 30.0f && GetOrbitRotation() <= 120.0f &&
            GetSkyboxExposureValue() >= -1.0f)
        {
            newExposureValue = GetSkyboxExposureValue() - lightingChangeFactor;
            SetSkyboxExposureValue(newExposureValue);
        }

        if (GetOrbitRotation() >= 280.0f && GetOrbitRotation() <= 360.0f
            && GetSkyboxExposureValue() <= 1.0f)
        {
            newExposureValue = GetSkyboxExposureValue() + lightingChangeFactor;
            SetSkyboxExposureValue(newExposureValue);
        }
    }

    /// <summary>
    /// Changes whether the exposure should increase or not
    /// </summary>
    /// <param name="state">True to increase exposure, false otherwise</param>
    void IncreaseExposure(bool state) {
        increaseExposure = state;
    }

    /// <summary>
    /// Changes whether the exposure should decrease or not
    /// </summary>
    /// <param name="state">True to decrease exposure, false otherwise</param>
    void DecreaseExposure(bool state) {
        decreaseExposure = state;
    }

    /// <summary>
    /// Changes exposure value after receiving a message from the sun collider
    /// </summary>
    void ChangeExposureValue() {
        if (increaseExposure)
        {
            if (GetSkyboxExposureValue() <= 1.0f)
            {
                newExposureValue = GetSkyboxExposureValue() + (lightingChangeFactor * 2.0f);
                newAmbientIntensityValue = GetAmbientIntensityValue() + (lightingChangeFactor * 2.0f);

                SetSkyboxExposureValue(newExposureValue);
                SetAmbientIntensityValue(newAmbientIntensityValue);
            }
            else {
                IncreaseExposure(false);
            }
        }
        else if (decreaseExposure)
        {
            if (GetSkyboxExposureValue() >= 0.10f)
            {
                newExposureValue = GetSkyboxExposureValue() - lightingChangeFactor;
                newAmbientIntensityValue = GetAmbientIntensityValue() - lightingChangeFactor;

                SetSkyboxExposureValue(newExposureValue);
                SetAmbientIntensityValue(newAmbientIntensityValue);
            }
            else
            {
                DecreaseExposure(false);
            }
        }
    }
}
