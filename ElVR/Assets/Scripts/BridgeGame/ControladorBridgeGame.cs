﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorBridgeGame : MonoBehaviour {

	struct MoveDataStruct

	{
		private List<float> x;
		private List<float> y;
		private List<float> z;

		public List<float> X
		{
			get
			{
				return x;
			}

			set
			{
				x = value;
			}
		}

		public List<float> Y
		{
			get
			{
				return y;
			}

			set
			{
				y = value;
			}
		}

		public List<float> Z
		{
			get
			{
				return z;
			}

			set
			{
				z = value;
			}
		}

	}
	public enum BridgeGameState
	{
		LOGIN,
		NOSESION,
		WELCOME,
		WELCOME2,
		TUTORIAL,
		TUTORIAL2,
		TUTORIAL3,
		TUTORIAL4,
		TUTORIAL5,
		TUTORIAL6,
		SPAWN1,
		SPAWN2,
		SPAWN3,
		COUNTER,
		ELVRMOVEMENT,
		PLAYING,
		GAMEOVER,
		ENDGAME
	}

	public static ControladorBridgeGame instance;
	public bool pruebaEditor;

	public int E_posicion;
	public int E_Duracion;
	public int E_ModoEjercicio; // 1 por tiempo 2 libre


	public static int MaderasPuestas;

	public Transform maderas;
	public Transform hand;
	public Transform[] posiciones;
	public Text txtmaderasColocadas;
	public static bool isFinish=false;




	public Text timer;
	private float promExito;
	private int winner;

	public Image gearVrImageField;
	public BridgeGameState bridgeGameState;
	private bool isConeccting;
	private bool isDosificated;
	//Keys to connect
	public bool keyExercise;
	public bool keyGps;
	//Data from phone
	private float latitud=0;
	private float longitud=0;


	//MOVES
	private List<MoveDataStruct> movesData;
	private List<float> dataXpos;
	private List<float> dataYpos;
	private List<float> dataZpos;
	private float dataCounter;

	//TIMER
	private float secondsCount;
	private float secondsCount2;
	private int minuteCount;
	private int hourCount;

	//Textures for tutorial
	public Sprite connectingSprt;
	public Sprite tutorialFish1Sprt;
	public Sprite tutorialFish2Sprt;
	public Sprite tutorialFish3Sprt;
	public Sprite tutorialFish4Sprt;
	public Sprite tutorialFish5Sprt;
	public Sprite tutorialFish6Sprt;
	public Sprite tutorialFish7Sprt;
	public Sprite tutorialFish8Sprt;
	public Sprite gameFinishSprt;
	public Sprite noSesionSprt;





	public BridgeGameState GetBridgeGameState()
	{
		return bridgeGameState;
	}

	void Awake()
	{
		instance = this;
	}

	void Start () {
		keyGps = true;

		//Initialize the list variables
		movesData = new List<MoveDataStruct>();
		dataXpos = new List<float>();
		dataYpos = new List<float>();
		dataZpos = new List<float>();
		dataCounter = 0.3f;
	}

	public void UpdateUI(){
		secondsCount += Time.deltaTime;
		secondsCount2 += Time.deltaTime;
		txtmaderasColocadas.text = "" + MaderasPuestas;
		if (E_ModoEjercicio==1) {
			if (secondsCount2 < E_Duracion + 1) {
				timer.text = hourCount + "h:" + minuteCount + "m:" + (int)secondsCount + "s";
			} else {

				bridgeGameState = BridgeGameState.GAMEOVER;
			}
		} else {
			if (isFinish) {
				bridgeGameState = BridgeGameState.GAMEOVER;
			}
		}
			

		if(secondsCount >= 60){
			minuteCount++;
			secondsCount = 0;
		}else if(minuteCount >= 60){
			hourCount++;
			minuteCount = 0;
		}    
	}



	// Update is called once per frame
	void Update () {
		switch (bridgeGameState){
		case BridgeGameState.LOGIN:
			/*
			if (!isConeccting) {
				PostData ("paciente@paciente.com", "paciente");
				gearVrImageField.sprite = connectingSprt;
				isConeccting = true;
			}

			if(keyExercise && keyGps)
			{
				if (!isDosificated) {
					PostOnline ("3", "1", longitud, latitud);
					isDosificated = true;
				}
			}  
			*/             
			bridgeGameState = BridgeGameState.COUNTER;
			break;
		case BridgeGameState.COUNTER:

			Debug.Log ("counter");
			gearVrImageField.sprite = tutorialFish1Sprt;
			if (E_posicion == 1) {
				maderas.position = posiciones [0].position;
				maderas.rotation = posiciones [0].rotation;
			} else if (E_posicion == 2) {
				maderas.position = posiciones [1].position;
				maderas.rotation = posiciones [1].rotation;
			} else if (E_posicion == 3) {
				maderas.position = posiciones [2].position;
				maderas.rotation = posiciones [2].rotation;
			} else {
				maderas.position = posiciones [3].position;
				maderas.rotation = posiciones [3].rotation;
			}
			bridgeGameState = BridgeGameState.PLAYING;
			break;	


		case BridgeGameState.PLAYING:

			UpdateUI ();

			if (SteamVR_LaserPointer.isGet) {

				dataCounter += Time.deltaTime;

				if (dataCounter >= 0.3f)
				{
					SaveMoveData(hand.transform.position.x, hand.transform.position.y, hand.transform.position.z);
					dataCounter = 0f;
				}
			}

			if (SteamVR_LaserPointer.isInPosition) {
				SaveMoveToJson(dataXpos,dataYpos,dataZpos);
				SteamVR_LaserPointer.isInPosition = false;
			}



			break;


		case BridgeGameState.GAMEOVER:

			promExito = 100;
			promExito = Mathf.Round (promExito * 100f) / 100f;

			Debug.Log (promExito);

			if (promExito > 60) {
				winner = 1;
			} else {
				winner = 0;
			}
				
			PostMovesData ();
			DosificationController.instance.PostCompletaEjercicio3 ();
			bridgeGameState = BridgeGameState.ENDGAME;

			break;
		case BridgeGameState.ENDGAME:

			break;
		default:
			break;
		}     
	}



	public void SetKeyGps(bool status)
	{
		keyGps = status;
	}

	public void SaveMoveData(float x, float y, float z)
	{
		dataXpos.Add(x);
		dataYpos.Add(y);
		dataZpos.Add(z);


	}

	public void SaveMoveToJson(List<float> x, List<float> y, List<float> z)
	{
		MoveDataStruct auxStruc = new MoveDataStruct();
		auxStruc.X = new List<float>();
		auxStruc.Y = new List<float>();
		auxStruc.Z = new List<float>();

		for (int i = 0; i < x.Count; i++)
		{
			auxStruc.X.Add(x[i]);
			auxStruc.Y.Add(y[i]);
			auxStruc.Z.Add(z[i]);

			Debug.Log ("x:"+x[i]+ "Y:"+y[i] + "Z:"+z[i]);
		}

		movesData.Add(auxStruc);

		dataXpos.Clear();
		dataYpos.Clear();
		dataZpos.Clear();
	}

	void PostMovesData()
	{
		string movimientos = "[{";
		for(int i = 0; i < movesData.Count; i ++)
		{

			movimientos += "\"movimiento\":[{";
			for (int j = 0; j < movesData[i].X.Count; j++)
			{
				movimientos += "\"x\":" + "\"" + movesData[i].X[j] + "\"" + ",";
				movimientos += "\"y\":" + "\"" + movesData[i].Y[j] + "\"" + ",";
				movimientos += "\"z\":" + "\"" + movesData [i].Z [j] + "\"";
				movimientos += "}";
				if (j != movesData[i].X.Count - 1)
					movimientos += ",{";
				else
					movimientos += "]}";
			}
			if (i != movesData.Count - 1)
				movimientos += ",{";
		}
		movimientos += "]";

		Debug.Log (movimientos);
		//string testTiros = "[{\"turno\": [{\"x\": \"-47.016\",\"y\": \"1.00015\",\"z\": \"-144.79\",\"nivel\": \"1\",\"acierto\": \"1\"}, {\"x\": \"-46.314\",\"y\": \"0.76515\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.296\",\"y\": \"0.96715\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.889\",\"y\": \"0.71315\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-47.068\",\"y\": \"1.60115\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}]}]";
		//gearVrTxtField2.text = auxProm.ToString();
		//DosificationController.instance.SaveEvolutionBallGame("33.2", "1", "12", "4", "4", "3",testTiros);
	//	DosificationController.instance.SaveEvolutionBridgeGame(promExito.ToString(), winner.ToString(), cantidadObjAparecieron.ToString(),"7", DosificationController.instance.exercise4Data.id_ejercicio, movimientos);
		//WWWForm phoneData = new WWWForm();
		//phoneData.AddField("tiros", tiros);
		//WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
		////StartCoroutine(PostGetEnumerator(www));
		//StartCoroutine("PostTurnDataEnumerator", www);
	}
	void PostGetExersice(string id, string ideE)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("id_ejercicio", ideE);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
		StartCoroutine("PostGetEnumerator", www);
	}
	void PostOnline(string id, string status, float lat, float lon)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("status", status);
		phoneData.AddField("latitud", lat.ToString());
		phoneData.AddField("longitud", lon.ToString());

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
		StartCoroutine("PostOnlineEnumerator", www);
	}
	void PostData(string dni, string pass)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("email", dni);
		phoneData.AddField("pass", pass);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
		StartCoroutine("PostdataEnumerator", www);

	}
	IEnumerator PostOnlineEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
	//		PostGetExersice("3", "7");
			/////////////////////////////////////////////////////////////////////
			if (bridgeGameState == BridgeGameState.LOGIN) {
				bridgeGameState = BridgeGameState.COUNTER;
			}
			////////////////////////////////////////////////////////////////////			
			StopCoroutine("PostOnlineEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostGetEnumerator(WWW www)
	{   
		yield return www;
		if (www.error != "Null")
		{
			Debug.Log("Dosificacion");          
			print(www.text);

			DosificationController.instance.SaveDataExcersise4(www);
			Debug.Log(www.text);
			if (DosificationController.instance.exercise4Data.status)
			{
				if(bridgeGameState == BridgeGameState.LOGIN)
					bridgeGameState = BridgeGameState.COUNTER;
			}
			else
			{
			}
			StopCoroutine("PostGetEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostdataEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			DosificationController.instance.SaveDataPacient(www);
			keyExercise = true;
			StopCoroutine("PostdataEnumerator");
		}
		else
		{
			StopAllCoroutines();
			Debug.Log(www.error);
		}
	}











	/*
	public Transform control;
	public Transform Target;
	public float firingAngle = 45.0f;
	public float gravity = 9.8f;

	public Transform Projectile;      

	LineRenderer lr;
	int line_res;

	float velocity;
	float angle;
	float g;
	float dt;

	float Vy_old;
	float Vy;
	float Vx;

	void Awake()
	{    
	}

	void Start()
	{          
		StartCoroutine(SimulateProjectile());
	}

	IEnumerator SimulateProjectile()
	{
		Projectile.position = control.position + new Vector3(0, 0.0f, 0);
		// Short delay added before Projectile is thrown
		yield return new WaitForSeconds(1.5f);

		// Move projectile to the position of throwing object + add some offset if needed.

		// Calculate distance to target
		float target_Distance = Vector3.Distance(Projectile.position, Target.position);

		// Calculate the velocity needed to throw the object to the target at specified angle.
		float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

		// Extract the X  Y componenent of the velocity
		float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
		float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

		// Calculate flight time.
		float flightDuration = target_Distance / Vx;

		// Rotate projectile to face the target.
		Projectile.rotation = Quaternion.LookRotation(Target.position - Projectile.position);

		float elapse_time = 0;

		while (elapse_time < flightDuration)
		{
			Projectile.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

			elapse_time += Time.deltaTime;

			yield return null;
		}
	}  
	void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
	{
		GameObject myLine = new GameObject();
		myLine.transform.position = start;
		myLine.AddComponent<LineRenderer>();
		LineRenderer lr = myLine.GetComponent<LineRenderer>();
		lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
		lr.SetColors(color, color);
		lr.SetWidth(0.1f, 0.1f);
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
		GameObject.Destroy(myLine, duration);
	}
	// Update is called once per frame
	void FixedUpdate()
	{
	}

	void Update () {
		//DrawLine (control.position, Projectile.position, Color.red, 2f);
		DrawLine(control.position, Projectile.position,Color.red,5f);
	}
*/
}
