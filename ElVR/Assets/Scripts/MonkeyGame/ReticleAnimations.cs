﻿using UnityEngine;

public class ReticleAnimations : MonoBehaviour {

    public Transform reticleParent;

    private Animator reticleAnimator;
    private SpriteRenderer[] reticleImages;

    private void Start()
    {
        GetAnimatorFromParent();
        GetReticleElements();
    }

    /// <summary>
    /// Gets animator component from parent
    /// </summary>
    private void GetAnimatorFromParent() {
        reticleAnimator = reticleParent.GetComponent<Animator>();
    }

    /// <summary>
    /// Get all reticle parent elements
    /// </summary>
    private void GetReticleElements() {
        int elementCount = reticleParent.GetChildCount();

        reticleImages = new SpriteRenderer[elementCount];

        for (int i = 0; i < elementCount; i++)
        {
            reticleImages[i] = reticleParent.GetChild(i).GetComponent<SpriteRenderer>();
        }
    }

    /// <summary>
    /// Changes reticle size via animation
    /// </summary>
    /// <param name="focusReticle">True to focus, false otherwise</param>
    public void IncreaseReticleSize(bool focusReticle) {
        reticleAnimator.SetBool("In", focusReticle);
        reticleAnimator.SetBool("Out", !focusReticle);

        if (focusReticle)
            ChangeReticleColor(Color.green);
        else
            ChangeReticleColor(Color.white);
    }

    /// <summary>
    /// Resets the reticle bools
    /// </summary>
    public void ResetReticle() {
        reticleAnimator.SetBool("In", false);
        reticleAnimator.SetBool("Out", false);

        ChangeReticleColor(Color.white);
    }

    /// <summary>
    /// Changes current reticle color
    /// </summary>
    /// <param name="color"></param>
    void ChangeReticleColor(Color color) {
        foreach (SpriteRenderer sprite in reticleImages) {
            sprite.color = color;
        }
    }
}
