﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereAnimation : MonoBehaviour {

    private float[] sphereScale = { 0.05f, 0.1f };
    public float scaleFactor;

    // Update is called once per frame
    private void FixedUpdate()
    {
        ScaleAnimation();
    }

    /// <summary>
    /// Animates the sphere (scalewise)
    /// </summary>
    void ScaleAnimation()
    {

        this.transform.localScale =
            new Vector3(
                this.transform.localScale.x + (Time.fixedDeltaTime * scaleFactor),
                this.transform.localScale.y + (Time.fixedDeltaTime * scaleFactor),
                this.transform.localScale.z + (Time.fixedDeltaTime * scaleFactor)
                );

        if (this.transform.localScale.x >= sphereScale[1] ||
            this.transform.localScale.x <= sphereScale[0])
        {
            scaleFactor = scaleFactor * -1;
        }
    }
}
