﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetup : MonoBehaviour {

    private void Awake()
    {
        SetGameFrameRate();
    }

    /// <summary>
    /// Sets game default Frame Rate
    /// </summary>
    private void SetGameFrameRate() {
        Application.targetFrameRate = 60;
    }
    
}
