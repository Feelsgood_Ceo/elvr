﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingAnimation : MonoBehaviour {

    public Transform loadingSphere;
    public float timeFactor;
    public Vector3[] spherePositions;

    // Update is called once per frame
    private void FixedUpdate () {
        HorizontalAnimation();
    }

    /// <summary>
    /// Changes the multiplier factor sign
    /// </summary>
    /// <param name="factor">Factor to change</param>
    /// <returns>Factor with inverted sign</returns>
    float ChangeFactor(float factor) {
        return factor * -1;
    }

    /// <summary>
    /// Animates the sphere (horizontally)
    /// </summary>
    void HorizontalAnimation() {

        loadingSphere.localPosition =
            new Vector3(
                loadingSphere.localPosition.x + (Time.fixedDeltaTime * timeFactor),
                loadingSphere.localPosition.y,
                loadingSphere.localPosition.z
            );


        if (loadingSphere.localPosition.x >= spherePositions[1].x)
        {
            loadingSphere.localPosition = spherePositions[0];
        }
    }
}
