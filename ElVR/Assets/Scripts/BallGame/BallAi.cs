﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAi : MonoBehaviour {

    public enum BallAiStates
    {
        BORN,
        CHARGING,
        THROWING,
        TRAVEL
    }

    public enum StateLooky
    {
        NORMAL,
        WIN,
        LOSE
    }
    public static BallAi instance;
    public bool myTurn;
    public Transform spawPoint;
    public GameObject aiArm;
    public GameObject sphere;
    public List<Transform> throwingTriggers;
    public int streak;
    public ParticleSystem winParticles;
    public ParticleSystem loseParticles;
    public AudioClip winClip;
    public AudioClip loseClip;
    public int level;
    public int score;

    public BallAiStates ballAiState;
    private StateLooky stateLooky;

    private float lerpTime;
    private Vector3 gravity = new Vector3(0f, -0.005f, 0f);
    private Vector3 direction;
    private Vector3 velocity;

    private AudioSource audioSource;

    void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        score = 0;
        streak = 0;
        myTurn = true; 
        ballAiState = BallAiStates.BORN;
        lerpTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        if(myTurn && BallGameController.instance.GetBallGameState() == BallGameController.BallGameState.PLAYING)
        {
            //BallGameController.instance.informationTxt.text = "Turno jugador 2";
            ballAiState = BallAiStates.CHARGING;
            myTurn = false;
        }

        switch (ballAiState)
        {
            case BallAiStates.BORN:
                sphere.transform.position = spawPoint.transform.position;
                aiArm.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                break;
            case BallAiStates.CHARGING:
                print("charging");
                sphere.transform.position = spawPoint.transform.position;
                //aiArm.transform.localEulerAngles = Vector3.Lerp(aiArm.transform.localEulerAngles, new Vector3(120f, aiArm.transform.localEulerAngles.y, aiArm.transform.localEulerAngles.z), lerpTime);
                aiArm.transform.localEulerAngles += new Vector3(0f, 0f, 50 * Time.deltaTime);
                //lerpTime += Time.deltaTime;
                if(aiArm.transform.localEulerAngles.z >=60f)
                {
                    ballAiState = BallAiStates.THROWING;
                }
                break;
            case BallAiStates.THROWING:
                sphere.transform.position = spawPoint.transform.position;
                //aiArm.transform.localEulerAngles = Vector3.Lerp(aiArm.transform.localEulerAngles, new Vector3(transform.localEulerAngles.x, aiArm.transform.localEulerAngles.y, -100f), lerpTime);             
                //lerpTime += Time.deltaTime;
                aiArm.transform.localEulerAngles -= new Vector3(0f, 0f, 300 * Time.deltaTime);
                int levelAux =0;
                if(int.Parse(DosificationController.instance.exerciseData.nivel) != 3)
                {
                    levelAux = int.Parse(DosificationController.instance.exerciseData.nivel);
                }
                if (Vector3.Distance(throwingTriggers[levelAux].transform.position,spawPoint.transform.position)<0.2f)
                {
                    ballAiState = BallAiStates.TRAVEL;
                    direction = spawPoint.transform.position - aiArm.transform.position;
                    velocity = direction.normalized * 0.2f;
                }
                break;
            case BallAiStates.TRAVEL:             
                velocity += gravity;
                sphere.transform.position += velocity;
                break;
            default:
                break;
        }
    }

    public void CalculateBallAITravel(GameObject obj)
    {
        if (obj.gameObject.tag == sphere.tag)
        {
            audioSource.clip = winClip;
            audioSource.Play();
            winParticles.gameObject.transform.position = sphere.transform.position;
            winParticles.Play();
            BallGameController.instance.bunny2.GetComponent<Animator>().SetTrigger("BunnyHappy");
            score += 10;
            streak++;
            if(level<BallGameController.instance.limitLevel)
            {
                level++;
                BallGameController.instance.aiSlider.value += 0.25f;
            }
            
        }
        else
        {
            audioSource.clip = loseClip;
            audioSource.Play();
            loseParticles.gameObject.transform.position = sphere.transform.position;
            loseParticles.Play();
            BallGameController.instance.bunny2.GetComponent<Animator>().SetTrigger("BunnySad");
            if (score>=5)
            score -= 5;
            streak = 0;
        }
        
        ballAiState = BallAiStates.BORN;
    }
}
