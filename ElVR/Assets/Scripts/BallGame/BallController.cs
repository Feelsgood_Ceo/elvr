﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    public bool AI;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if(BallGameController.instance.GetBallState() == BallGameController.BallStates.THROWING || BallAi.instance.ballAiState == BallAi.BallAiStates.TRAVEL)
        {
            if (!AI)
            {
                BallGameController.instance.calculateBallTravelResult(col.gameObject);             
                BallAi.instance.myTurn = true;
            }
                
            else
            {
                BallAi.instance.CalculateBallAITravel(col.gameObject);
                BallGameController.instance.myTurn = true;
                BallGameController.instance.starsParticles.Play();
                //BallGameController.instance.gearVrMessage.SetActive(true);
                BallGameController.instance.gearVrImageField.gameObject.SetActive(true);
                BallGameController.instance.ball.SetActive(true);
                BallGameController.instance.cloudsParticles.gameObject.transform.position = BallGameController.instance.ball.transform.position;
                BallGameController.instance.cloudsParticles.Play();
            }
                
        }
        

        print(BallGameController.instance.GetBallState());
    }
}
