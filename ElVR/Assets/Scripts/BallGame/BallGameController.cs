﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class BallGameController : MonoBehaviour {

    struct TurnDataStruct
        
    {
        private List<float> x;
        private List<float> y;
        private List<float> z;
        private int nivel;
        private List<int> acierto;

        public List<float> X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public List<float> Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }

        public List<float> Z
        {
            get
            {
                return z;
            }

            set
            {
                z = value;
            }
        }

        public int Nivel
        {
            get
            {
                return nivel;
            }

            set
            {
                nivel = value;
            }
        }

        public List<int> Acierto
        {
            get
            {
                return acierto;
            }

            set
            {
                acierto = value;
            }
        }
    }

    public enum BallGameState
    {
        LOGIN,
        NOSESION,
        WATERMELON,
        WELCOME,
        WELCOME2,
        TUTORIAL,
        TUTORIAL2,
        TUTORIAL3,
        TUTORIAL4,
        TUTORIAL5,
        TUTORIAL6,
        SPAWN1,
        SPAWN2,
        SPAWN3,
        COUNTER,
        ELVRMOVEMENT,
        PLAYING,
        GAMEOVER,
        ENDGAME
    }
    public enum BallStates
    {
        BORN,
        HANDED,
        THROWING,
        DEAD
    }

    //front
    public GameObject frontCollider;

    //Main text messages
    public Transform gearVrPointTransform;
    public GameObject gearVrMessage;
    public Text gearVrTxtField1;
    public Text gearVrTxtField2;
    public Image gearVrImageField;

    //Audio sources
    public AudioClip audioClipWelcome;
    public AudioClip audioPressTrigger;
    public AudioClip audioSit;
    public AudioClip audioSelectApple;
    public AudioClip audioMoveArm;
    public AudioClip audioReleaseTrigger;
    public AudioClip audioHitWorm;
    

    //Main GUI text
    public GameObject guiMessagesContainer;

    //MainCanvas rendered in screen
    public Image gearVr2AlertImage;
    public Image gearVr2PressTrigger;

    //Textures for tutorial
    public Sprite connectingSprt;
    public Sprite tutorialBall1Sprt;
    public Sprite tutorialBall2Sprt;
    public Sprite tutorialBall3Sprt;
    public Sprite tutorialBall4Sprt;
    public Sprite tutorialBall5Sprt;
    public Sprite tutorialBall6Sprt;
    public Sprite tutorialBall7Sprt;
    public Sprite tutorialBall8Sprt;
    public Sprite gameFinishSprt;
    public Sprite noSesionSprt;

    //Textures for gui
    public Sprite goal1Sprt;
    public Sprite goal2Sprt;
    public Sprite goal3Sprt;
    public Sprite goal4Sprt;
    //Pop-up for turns
    public Sprite popUpYourTurn;
    

    //Sliders
    public Slider playerSlider;
    public Slider aiSlider;
    //Sliders backgrounds
    public Image playerSliderBackground;
    public Image aiSliderBackground;

    //El vr movement
    public GameObject elVr;
    public List<Transform> elVrPos;
    public float elVrspeed;
    public float elVrTime;
    public int auxIndex = 0;


    //This float is for manage de time between gamestates
    public float gameCounter = 0f;

    //Only for debug , delete the component for release
    public Text debugTxt;
    public Text levelTxt;
    public Text streakTxt;

    public Text scoreAiTxt;
    public Text levelAiTxt;
    public Text streakAiTxt;

    public Text informationTxt;

    public Text timeElapsedTxt;
    public float timeElapsed;

    public bool myTurn;

    //Materials for change the color of the ball
    public Material blueMaterial;
    public Material yellowMaterial;
    public Material greenMaterial;

    //Baskets
    public GameObject basketBlue;
    public GameObject basketYellow;
    public GameObject basketGreen;

    public GameObject mainBasketCap;

    //Tutorial Points
    public List<GameObject> tutorialPoints;

    //Ball
    public GameObject ball;
    //hand or controll model for reference
    public GameObject model;
    public GameObject arm;
    public GameObject hand;
    public GameObject handFbx;
    //hand position transform
    public Transform rightHandAnchor;
    //spawn point for new balls
    public Transform spawnPoint;
    //spawn points for clouds
    public Transform spawnCloudsP1;

    //Particles
    public ParticleSystem failParticles;
    public ParticleSystem winParticles;
    public ParticleSystem starsParticles;
    public ParticleSystem cloudsParticles;

    //Asistent
    public GameObject watermelon;

    //AudioClips for effects
    public AudioClip failSoundEffect;
    public AudioClip winSoundEffect;

    //singleton
    public static BallGameController instance;

    [Header("Photon Handler")]
    //Connection to photon
    public ConnectAndJoinRandom connectToPhotonScript;

    public PlayableDirector TL;

    //status
    private bool isConeccting;
    //level dificult
    private int level;

    //LIMIT LEVEL TO WIN
    public int limitLevel;

    //Ball gravity
    private Vector3 gravity = new Vector3(0f, -0.005f, 0f);
    //Ball speed
    private float speed = 0.2f;
    //Direction to throw the ball
    private Vector3 direction;
    //Velocity to apply to the ball
    private Vector3 velocity;

    //if the ball is in her hand
    private bool ballHanded;

    //true if ball enters to the correct basket
    private bool hasAPoint;
    //tag for knows the ball actual color
    private string ballTag;

    //score
    private int score;
    //Streak
    private int streak;
    //Repetitions for the exercise
    private int repetitions;
    private int applesUses;
    
    

    //AudioSource for effects
    private AudioSource audioSource;
    private AudioSource [] audioSources;
    //Times to spawn the tutorial
    private int tutorialTimes = 0;
    private BallStates ballStates;
    private BallGameState ballGameState;

    //Initial level pos
    public List<GameObject> levelPos;

    //Bool to hides the tutorialPoints
    private bool hideTutorial;

    private int winner;

    //Detect when trigger is pressed
    private bool isTriggerPress;

    //Helper to controlThePlaybackAudio
    private bool canPlayTuTo;
    private bool canPlayTrigger;

    //Helpers to store the positions before pass to saveTurnData function
    private float auxXinit;
    private float auxYinit;
    private float auxZinit;
    private float auxXfin;
    private float auxYfin;
    private float auxZfin;
    private bool auxkey = true;

    //Data to send to server in every turn
    private List<TurnDataStruct> turnsData;
    private List<float> dataXpos;
    private List<float> dataYpos;
    private List<float> dataZpos;
    private List<int> dataAcierto;
    private int dataNivel;
    private float dataCounter;
    //private TurnDataStruct auxStruc;

    //Keys to connect
    private bool keyExercise;
    private bool keyGps;

    //Data from phone
    private float latitud;
    private float longitud;

    //Bunnys for manage animations
    public GameObject bunny1;
    public GameObject bunny2;



    void Awake()
    {
        instance = this;
    }
	void Start () {
        keyExercise = false;
        keyGps = false;
        canPlayTuTo = true;
        canPlayTrigger = true;
        isConeccting = false;
        isTriggerPress = false;
        ballGameState = BallGameState.LOGIN;
        playerSlider.value = 0f;
        aiSlider.value = 0f;
        //change with the value of the server
        timeElapsed = 500f;
        myTurn = false;
        streak = 0;
        applesUses = 0;
        repetitions = 0;
        //read from the server
        level = 0;
        //--------------------
        hideTutorial = true;
        audioSource = GetComponent<AudioSource>();
        
        audioSources = GetComponents<AudioSource>();
        score = 0;
        ballHanded = false;
        hasAPoint = false;
        int aux = Random.Range(0, 3);
        //SetNewBallMaterial(aux);
        ChangeMaterial(greenMaterial);
        ballTag = "green";
        ball.GetComponent<Renderer>().material = greenMaterial;
        ballStates = BallStates.BORN;
        ball.transform.position = spawnPoint.transform.position;
        gearVrTxtField2.gameObject.SetActive(false);

        //Initialize the list variables
        turnsData = new List<TurnDataStruct>();
        dataXpos = new List<float>();
        dataYpos = new List<float>();
        dataZpos = new List<float>();
        dataAcierto = new List<int>();
        dataCounter = 2f;

        //auxStruc = new TurnDataStruct();
        //auxStruc.X = new List<float>();
        //auxStruc.Y = new List<float>();
        //auxStruc.Z = new List<float>();
        //auxStruc.Acierto = new List<int>();

    }
	
	void Update () {
        
        timeElapsedTxt.text = ((int)timeElapsed).ToString();
        
        //Rotates the model that simulates the hand of the player
		model.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
        //Rotates the arm of the player
        arm.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);

        //Shows in screen the score of the player
        debugTxt.text = " " + score;
        //levelTxt.text = "Nivel: " + level;
        //streakTxt.text = "Racha: " + streak;

        //Shows in screen the score of the AI
        scoreAiTxt.text = "" + BallAi.instance.score;
        //levelAiTxt.text = "Nivel: " + BallAi.instance.level;
        //streakAiTxt.text = "Racha: " + BallAi.instance.streak;

        gearVrMessage.transform.position = Vector3.MoveTowards(gearVrMessage.transform.position, gearVrPointTransform.transform.position, 2f * Time.deltaTime);
        gearVrMessage.transform.forward = gearVrPointTransform.transform.forward;

        if(DosificationController.instance.exerciseData != null)
        {
             //print(DosificationController.instance.exerciseData.distancia);              
        }

        if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
        {
            handFbx.GetComponent<Animator>().SetBool("isTaking", true);
            isTriggerPress = true;
        }else
        {
            handFbx.GetComponent<Animator>().SetBool("isTaking", false);
        }
        //ALERT WHEN THE USER IS SEEING AROUND
        /*if (VRRaycaster.instance.touchedGameObjectGaze.tag != frontCollider.tag)
        {
            if (ballGameState == BallGameState.PLAYING)
                gearVrMessage.SetActive(true);
            gearVr2AlertImage.gameObject.SetActive(true);
        }else
        {
            
            gearVr2AlertImage.gameObject.SetActive(false);
        }*/

            switch (ballGameState)
        {
            case BallGameState.LOGIN:
                if(!isConeccting)
                {
                    PostData("paciente@paciente.com", "paciente");
                    gearVrImageField.sprite = connectingSprt;
                    isConeccting = true;
                }
                if(keyExercise && keyGps)
                {
                    
                    PostOnline("3","1", longitud , latitud);
                   // ballGameState = BallGameState.WELCOME;
                }               
                //gearVrMessage.SetActive(true);
                //gearVrTxtField2.text = "Conectando...";
                break;
            case BallGameState.NOSESION:
                gearVrImageField.sprite = noSesionSprt;
                break;
            /*case BallGameState.WATERMELON:
                cloudsParticles.transform.position = watermelon.transform.position;
                cloudsParticles.Play();
                watermelon.SetActive(true);
                
                break;
            case BallGameState.WELCOME:
                //gearVrMessage.SetActive(true);
                int momentDay = int.Parse(DosificationController.instance.exerciseData.momento);
                if (momentDay == 1)
                    DayAndTimeController.instance.ChangeMoment(DayAndTimeController.MomentOfDay.DAY);
                if(momentDay == 2)
                    DayAndTimeController.instance.ChangeMoment(DayAndTimeController.MomentOfDay.AFTERNOON);
                if(momentDay == 3)
                    DayAndTimeController.instance.ChangeMoment(DayAndTimeController.MomentOfDay.NIGHT);
                timeElapsed = int.Parse(DosificationController.instance.exerciseData.duracion);
                gearVrTxtField2.text = "";
                
                SetSlidersBackground();
                limitLevel = int.Parse(DosificationController.instance.exerciseData.grado) - 1;
                print("Funcionando");
                gearVrImageField.sprite = tutorialBall1Sprt;
               // if(VRRaycaster.instance.touchedGameObjectGaze.tag==frontCollider.tag)
               // {
                    gameCounter += Time.deltaTime;

                
                // }
                //if (gameCounter >= 7)
                if(gameCounter >=8 && canPlayTrigger)
                {
                    watermelon.GetComponent<Animator>().Play("Sandia_anim_botonFront");
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                
                if (gameCounter >= 3 && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {
                    audioSource.clip = audioSit;
                    audioSource.Play();
                    ballGameState = BallGameState.WELCOME2;
                    gameCounter = 0f;
                    canPlayTrigger = true;
                }
                break;
            case BallGameState.WELCOME2:
                gearVrImageField.sprite = tutorialBall2Sprt;

                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 6 && canPlayTrigger)
                {
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                if (gameCounter >= 4 && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {
                    audioSource.clip = audioSelectApple;
                    audioSource.Play();
                    ballGameState = BallGameState.TUTORIAL;
                    gameCounter = 0f;
                    canPlayTrigger = false;
                }
                break;
            case BallGameState.TUTORIAL:
                //gearVrTxtField1.text = "Instrucciones";
                //gearVrTxtField2.text = "Aqui habra una imagen";
                gearVrImageField.sprite = tutorialBall3Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 6 && canPlayTrigger)
                {
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                if (gameCounter >= 4f && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {
                    audioSource.clip = audioMoveArm;
                    audioSource.Play();
                    ballGameState = BallGameState.TUTORIAL2;
                    gameCounter = 0f;
                    canPlayTrigger = true;
                }               
                break;
            case BallGameState.TUTORIAL2:
                gearVrImageField.sprite = tutorialBall4Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 1.2f)
                {
                    ballGameState = BallGameState.TUTORIAL3;
                    gameCounter = 0f;
                }
                break;
            case BallGameState.TUTORIAL3:
                gearVrImageField.sprite = tutorialBall5Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 1.2f)
                {
                    ballGameState = BallGameState.TUTORIAL4;
                    gameCounter = 0f;
                }
                break;
            case BallGameState.TUTORIAL4:
                gearVrImageField.sprite = tutorialBall6Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 3 && canPlayTrigger)
                {
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                if (gameCounter >= 3f && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {
                    audioSource.clip = audioReleaseTrigger;
                    audioSource.Play();
                    ballGameState = BallGameState.TUTORIAL5;
                    gameCounter = 0f;
                    canPlayTrigger = true;
                }
                break;
            case BallGameState.TUTORIAL5:
                gearVrImageField.sprite = tutorialBall7Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 6 && canPlayTrigger)
                {
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                if (gameCounter >= 3f && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {
                    audioSource.clip = audioHitWorm;
                    audioSource.Play();
                    ballGameState = BallGameState.TUTORIAL6;
                    gameCounter = 0f;
                    canPlayTrigger = true;
                }
                break;
            case BallGameState.TUTORIAL6:
                gearVrImageField.sprite = tutorialBall8Sprt;
                //if (VRRaycaster.instance.touchedGameObjectGaze.tag == frontCollider.tag)
                //{
                    gameCounter += Time.deltaTime;
                //}
                if (gameCounter >= 5 && canPlayTrigger)
                {
                    audioSources[1].clip = audioPressTrigger;
                    audioSources[1].Play();
                    canPlayTrigger = false;
                }
                if (gameCounter >= 3f && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote))
                {

                    ballGameState = BallGameState.COUNTER;
                    gameCounter = 0f;
                    canPlayTrigger = true;
                }
                break;*/
            case BallGameState.COUNTER:
               // gearVrTxtField2.text = ((int)gameCounter).ToString();
               // gameCounter -= Time.deltaTime;
               // if(gameCounter <= 0f)
               // {
                gameCounter = 0f;
                gearVrImageField.sprite = popUpYourTurn;
                gearVrImageField.gameObject.SetActive(false);
                cloudsParticles.gameObject.transform.position = spawnCloudsP1.position;
                cloudsParticles.Play();
                guiMessagesContainer.SetActive(true);
                ballGameState = BallGameState.SPAWN1;
               // }
                break;
            case BallGameState.SPAWN1:
                gameCounter += Time.deltaTime;
                if(gameCounter >= 3f)
                {
                    
                    gameCounter = 0f;
                    cloudsParticles.gameObject.transform.position = levelPos[0].transform.position;
                    cloudsParticles.Play();
                    basketGreen.SetActive(true);
                    basketYellow.SetActive(true);
                    ballGameState = BallGameState.SPAWN2;                   
                }                
                break;
            case BallGameState.SPAWN2:
                gameCounter += Time.deltaTime;
                if(gameCounter>=3f)
                {
                    gameCounter = 0f;
                    cloudsParticles.gameObject.transform.position = spawnPoint.position;
                    cloudsParticles.Play();
                    elVr.transform.position = elVrPos[0].position;
                    elVr.transform.localEulerAngles = new Vector3(0, 90, 0);
                    ballGameState = BallGameState.ELVRMOVEMENT;
                }
                break;
            case BallGameState.ELVRMOVEMENT:
                
                elVr.transform.position = Vector3.Lerp(elVrPos[auxIndex].position, elVrPos[auxIndex + 1].position, elVrTime);
                elVrTime += Time.deltaTime * elVrspeed;
                if(auxIndex == 2)
                {
                    elVrspeed = 2f;
                }
                if(elVrTime >=1)
                {
                    auxIndex++;
                    elVrTime = 0;

                }
                if(auxIndex >= elVrPos.Count-1)
                {
                    elVr.transform.localEulerAngles = new Vector3(0, -90, 0);
                    ballGameState = BallGameState.PLAYING;
                }
                break;
            case BallGameState.PLAYING:
                if(timeElapsed>0f)
                timeElapsed -= Time.deltaTime;
                if (!myTurn)
                {
                    ball.GetComponent<Rigidbody>().useGravity = true;
                    ball.SetActive(false);
                    //mainBasketCap.SetActive(true);
                    return;
                }
                gameCounter += Time.deltaTime;
                if (gameCounter >= 4f)
                {
                    //gearVrMessage.SetActive(false);
                    gearVrImageField.gameObject.SetActive(false);
                    gameCounter = 0f;
                }
                if(timeElapsed<=0f)
                {
                    ballGameState = BallGameState.GAMEOVER;
                }
                //Shows when is player turn
                //informationTxt.text = "Turno jugador 1";
                //mainBasketCap.SetActive(false);
                //Hides the tutorial points
                if (hideTutorial)
                {
                    HideTutorial();
                }

                //Function to get the trigger of the remote
                if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
                {
                    handFbx.GetComponent<Animator>().SetBool("isTaking", true);
                    if (VRRaycaster.instance.touchedGameObject.tag == "ball")
                    {
                        ballHanded = true;
                        
                        ballStates = BallStates.HANDED;
                    }

                }
                else
                {
                    if (ballHanded)
                    {
                        
                        ballStates = BallStates.THROWING;
                        ballHanded = false;
                        auxkey = true;
                    }
                    handFbx.GetComponent<Animator>().SetBool("isTaking", false);

                }

                ControlBallStates();
                ControlDificult();
                break;
            case BallGameState.GAMEOVER:
                gearVrMessage.SetActive(true);
                gearVrTxtField2.gameObject.SetActive(true);
                gearVrImageField.sprite = gameFinishSprt;             
                //float auxProm = (streak * 100) / applesUses;
                //auxProm = Mathf.Round(auxProm * 100f) / 100f;
                //DosificationController.instance.SaveEvolutionBallGame(auxProm.ToString(), winner.ToString(), applesUses.ToString(), level.ToString(), "4", DosificationController.instance.exerciseData.id_ejercicio);
                PostTurnsData(); //-----------------------------------------------------------
                DosificationController.instance.PostCompletaEjercicio();
                //PostOffline(DosificationController.instance.dataPacient.id_paciente, "0");
                ballGameState = BallGameState.ENDGAME;
                break;
            case BallGameState.ENDGAME:

                break;
            default:
                break;
        }     

    }

    public void ControlBallStates()
    {
        switch (ballStates)
        {
            case BallStates.BORN:
                ball.GetComponent<Rigidbody>().useGravity = true;
                break;
            case BallStates.HANDED:
                

                ball.GetComponent<Rigidbody>().useGravity = false;
                ball.transform.position = hand.transform.position;
                direction = hand.transform.position - arm.transform.position;
                velocity = direction.normalized * speed;

                //CAPTURING DE BALL DATA
                dataCounter += Time.deltaTime;

                if (dataCounter >= 2f)
                {
                    SaveTurnData(ball.transform.position.x, ball.transform.position.y, ball.transform.position.z, level, 0);
                    dataCounter = 0f;
                }
                break;
            case BallStates.THROWING:
                if (auxkey)
                {
                    SaveTurnData(ball.transform.position.x, ball.transform.position.y, ball.transform.position.z, level, 0);
                    auxkey = false;
                }
                //hideTutorial = true;
                velocity += gravity;
                ball.transform.position += velocity;
                break;
            case BallStates.DEAD:

                if (hasAPoint)
                {
                    score += 10;
                    streak++;
                    bunny1.GetComponent<Animator>().SetTrigger("BunnyHappy");
                    if(level< limitLevel)
                    {
                        level++;
                        playerSlider.value += 0.25f;
                    }
                    for (int i = 0; i < dataAcierto.Count; i++)
                    {
                        dataAcierto[i] = 1;
                    }
                }
                else
                {
                    BallGameController.instance.bunny2.GetComponent<Animator>().SetTrigger("BunnySad");
                    if (score > 0)
                        score -= 5;
                    
                }
                //int aux = Random.Range(0, 3);
                //SetNewBallMaterial(aux);
                ball.transform.position = spawnPoint.transform.position;
                repetitions++;              
                ballStates = BallStates.BORN;
                myTurn = false;
                auxkey = true;
                dataCounter = 2f;
                applesUses++;
                SaveTurnToJson(dataXpos,dataYpos,dataZpos,dataNivel,dataAcierto);
                    

                
                break;
            default:
                break;
        }
    }

    public void WaterMelonTrigger()
    {
        ballGameState = BallGameState.COUNTER;
        connectToPhotonScript.enabled = true;
        //keyExercise = true;
        audioSource.clip = audioClipWelcome;
        audioSource.Play();
        watermelon.GetComponent<Animator>().Play("Sandia_anim_mostrar"); // Here!
    }
    //Controls the dificult
    public void ControlDificult()
    {
        if (level == limitLevel)
        {
            ballGameState = BallGameState.GAMEOVER;
            gearVrTxtField2.text = "Ganaste!";
            winner = 1;
        }

        if(BallAi.instance.level == limitLevel)
        {
            ballGameState = BallGameState.GAMEOVER;
            gearVrTxtField2.text = "Lo haras mejor la proxima vez";
            winner = 0;
        }
        

        basketGreen.transform.position = new Vector3(levelPos[level].transform.position.x, basketGreen.transform.position.y, basketGreen.transform.position.z);
        basketYellow.transform.position = new Vector3(levelPos[BallAi.instance.level].transform.position.x, basketYellow.transform.position.y, basketYellow.transform.position.z);
    }

    //Shows a line of points to guide the movement that the players need to do
    public void ShowTutorial()
    {
        for (int i = 0; i < tutorialPoints.Count; i++)
        {
            if (tutorialPoints[i].transform.localScale.x < 0.3f)
            {
                if (i == (tutorialPoints.Count - 1))
                    tutorialTimes++;
                tutorialPoints[i].transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
                break;
            }
        }
    }

    //Hides all points of the tutorial
    public void HideTutorial()
    {
        for (int i = 0; i < tutorialPoints.Count; i++)
        {
            if (tutorialPoints[i].transform.localScale.x >= 0f)
            {
                tutorialPoints[i].transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
                break;
            }
        }
    }

    public void ChangeMaterial(Material mat)
    {
        ball.GetComponent<Renderer>().material = mat;
    }

    public void SetNewBallMaterial(int index)
    {
        switch(index)
        {
            case 0:
                ChangeMaterial(blueMaterial);
                ballTag = "blue"; 
                break;
            case 1:
                ChangeMaterial(yellowMaterial);
                ballTag = "yellow";
                break;
            case 2:
                ChangeMaterial(greenMaterial);
                ballTag = "green";
                break;
        }
    }

    public void calculateBallTravelResult(GameObject obj)
    {
        debugTxt.text = obj.name;
        if(obj.gameObject.tag == ballTag)
        {
            streak++;
            hasAPoint = true;
            audioSource.clip = winSoundEffect;
            audioSource.Play();
            winParticles.gameObject.transform.position = ball.transform.position;
            winParticles.Play();
        }else
        {
            hasAPoint = false;
            audioSource.clip = failSoundEffect;
            audioSource.Play();
            failParticles.gameObject.transform.position = ball.transform.position;
            failParticles.Play();
        }
        applesUses++;
        ballStates = BallStates.DEAD;
    }

    public BallStates GetBallState()
    {
        return ballStates;
    }

    public BallGameState GetBallGameState()
    {
        return ballGameState;
    }

    public int GetLevel()
    {
        return level;
    }

    public void SetKeyGps(bool status)
    {
        keyGps = status;
    }

    public void SetCoordinates(float longitud, float latitud)
    {
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public float GetLatitiud()
    {
        return latitud;
    }

    public float GetLongitud()
    {
        return longitud;
    }

    public void SetLevel(int lvl)
    {
        level = lvl;
    }

    public void SetSlidersBackground()
    {
        switch(limitLevel)
        {
            case 1:
                playerSliderBackground.sprite = goal1Sprt;
                aiSliderBackground.sprite = goal1Sprt;
                break;
            case 2:
                playerSliderBackground.sprite = goal2Sprt;
                aiSliderBackground.sprite = goal2Sprt;
                break;
            case 3:
                playerSliderBackground.sprite = goal3Sprt;
                aiSliderBackground.sprite = goal3Sprt;
                break;
            case 4:
                playerSliderBackground.sprite = goal4Sprt;
                aiSliderBackground.sprite = goal4Sprt;
                break;
        }
    }

    public void SaveTurnData(float x, float y, float z, int level, int success)
    {
        dataXpos.Add(x);
        dataYpos.Add(y);
        dataZpos.Add(z);
        dataNivel = level;
        dataAcierto.Add(success);
    }

    public void SaveTurnToJson(List<float> x, List<float> y, List<float> z, int level, List<int> success)
    {
        TurnDataStruct auxStruc = new TurnDataStruct();
        auxStruc.X = new List<float>();
        auxStruc.Y = new List<float>();
        auxStruc.Z = new List<float>();
        auxStruc.Acierto = new List<int>();

        for (int i = 0; i < x.Count; i++)
        {
            auxStruc.X.Add(x[i]);
            auxStruc.Y.Add(y[i]);
            auxStruc.Z.Add(z[i]);
            auxStruc.Acierto.Add(success[i]);
        }
        auxStruc.Nivel = level;

        turnsData.Add(auxStruc);

        dataXpos.Clear();
        dataYpos.Clear();
        dataZpos.Clear();
        dataNivel = 0;
        dataAcierto.Clear();
    }

    
    

    // To connect with the server;

    void PostData(string dni, string pass)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("email", dni);
        phoneData.AddField("pass", pass);

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
        // StartCoroutine(PostdataEnumerator(www));
        StartCoroutine("PostdataEnumerator", www);
        
    }
    void PostOnline(string id, string status, float lat, float lon)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("id_paciente", id);
        phoneData.AddField("status", status);
        phoneData.AddField("latitud", lat.ToString());
        phoneData.AddField("longitud", lon.ToString());

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
        //StartCoroutine(PostOnlineEnumerator(www));
        StartCoroutine("PostOnlineEnumerator", www);
    }
    //void PostOffline(string id, string status)
    //{
    //    WWWForm phoneData = new WWWForm();
    //    phoneData.AddField("id_paciente", id);
    //    phoneData.AddField("status", status);

    //    //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
    //    WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
    //    //StartCoroutine(PostOnlineEnumerator(www));
    //    StartCoroutine("PostOfflineEnumerator", www);
    //}
    void PostGetExersice(string id, string ideE)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("id_paciente", id);
        phoneData.AddField("id_ejercicio", ideE);

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
        //StartCoroutine(PostGetEnumerator(www));
        StartCoroutine("PostGetEnumerator", www);
    }
    void PostTurnsData()
    {
        string tiros = "[{";
        for(int i = 0; i < turnsData.Count; i ++)
        {

            tiros += "\"turno\":[{";
            for (int j = 0; j < turnsData[i].X.Count; j++)
            {
                tiros += "\"x\":" + "\"" + turnsData[i].X[j] + "\"" + ",";
                tiros += "\"y\":" + "\"" + turnsData[i].Y[j] + "\"" + ",";
                tiros += "\"z\":" + "\"" + turnsData[i].Z[j] + "\"" + ",";
                tiros += "\"nivel\":" + "\"" + turnsData[i].Nivel + "\"" + ",";
                tiros += "\"acierto\":" + "\"" + turnsData[i].Acierto[j] + "\"";
                tiros += "}";
                if (j != turnsData[i].X.Count - 1)
                    tiros += ",{";
                else
                    tiros += "]}";
            }
            if (i != turnsData.Count - 1)
                tiros += ",{";
        }
        tiros += "]";

        //string testTiros = "[{\"turno\": [{\"x\": \"-47.016\",\"y\": \"1.00015\",\"z\": \"-144.79\",\"nivel\": \"1\",\"acierto\": \"1\"}, {\"x\": \"-46.314\",\"y\": \"0.76515\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.296\",\"y\": \"0.96715\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.889\",\"y\": \"0.71315\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-47.068\",\"y\": \"1.60115\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}]}]";

        

        float auxProm = (streak * 100) / applesUses;
        auxProm = Mathf.Round(auxProm * 100f) / 100f;
        //gearVrTxtField2.text = auxProm.ToString();
        //DosificationController.instance.SaveEvolutionBallGame("33.2", "1", "12", "4", "4", "3",testTiros);
        DosificationController.instance.SaveEvolutionBallGame(auxProm.ToString(), winner.ToString(), applesUses.ToString(), level.ToString(), "4", DosificationController.instance.exerciseData.id_ejercicio, tiros);
        //WWWForm phoneData = new WWWForm();
        //phoneData.AddField("tiros", tiros);
        //WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
        ////StartCoroutine(PostGetEnumerator(www));
        //StartCoroutine("PostTurnDataEnumerator", www);
    }

    IEnumerator PostdataEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            //print(www.text);
            DosificationController.instance.SaveDataPacient(www);
            //PostOnline("3", "1","1231","-1231");
            keyExercise = true;
            StopCoroutine("PostdataEnumerator");
        }
        else
        {
            StopAllCoroutines();
            //ballGameState = BallGameState.WELCOME;
            Debug.Log(www.error);
        }
    }
    IEnumerator PostOnlineEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            Debug.Log("Online :D");
            PostGetExersice("3", "4");
            //print(www.text);
            StopCoroutine("PostOnlineEnumerator");
        }
        else
        {
            print("Error");
            Debug.Log(www.error);
        }
    }
    //IEnumerator PostOfflineEnumerator(WWW www)
    //{
    //    yield return www;
    //    if (www.error != "Null")
    //    {
    //        StopCoroutine("PostOfflineEnumerator");
    //    }
    //    else
    //    {
    //        print("Error");
    //        Debug.Log(www.error);
    //    }
    //}
    IEnumerator PostGetEnumerator(WWW www)
    {   
        yield return www;
        if (www.error != "Null")
        {
            Debug.Log("Dosificacion");          
            print(www.text);

            DosificationController.instance.SaveDataExcersise(www);
            //DosificationController.instance.SaveEvolutionBallGame("33.2","1","12","4","4","3");
            //PostTurnsData();
            if (DosificationController.instance.exerciseData.status)
            {
                gearVrImageField.sprite = null;
                
                

                TL.Play();

                yield return new WaitUntil(() => TL.time >= TL.duration - .25f);

                ballGameState = BallGameState.COUNTER;
                connectToPhotonScript.enabled = true;

                //keyExercise = true;
                //audioSource.clip = audioClipWelcome;
                //audioSource.Play();
            }
            else
            {
                ballGameState = BallGameState.NOSESION;
            }




            StopCoroutine("PostGetEnumerator");
        }
        else
        {
            print("Error");
            Debug.Log(www.error);
        }
    }
    IEnumerator PostTurnDataEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            print("Turn data sended");
            
            StopCoroutine("PostTurnDataEnumerator");
        }
        else
        {
            print("Error in post turn data");
            Debug.Log(www.error);
        }
    }
    

    void OnApplicationQuit()
    {
        
    }
}
