﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class LoginScreenController : MonoBehaviour {


    void Awake()
    {
        UnityEngine.XR.XRSettings.enabled = false;
    }
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartApplication()
    {
        UnityEngine.XR.XRSettings.enabled = true;
        UnityEngine.XR.XRSettings.LoadDeviceByName(SystemInfo.deviceName);
    }
}
