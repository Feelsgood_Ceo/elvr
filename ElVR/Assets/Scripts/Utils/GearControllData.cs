﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GearControllData : MonoBehaviour {

    public enum ControllerStates
    {
        SETUP,
        CALIBRATION,
        TRACKING
    }

    //Testing model
    public GameObject sword;

    //AlertPanel
    public GameObject alertPanel;
    public Text statusTxt;

    public Text debugTxt;
    public ControllerStates controllerStates;

    //Timers
    public float calibrationTime = 0f;
    public float calibrationAnimation = 0f;

    //Calibrated variables
    private Vector3 pointZeroPos;


    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTrackedRemote);
        Quaternion rotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
        sword.transform.localRotation = rotation;
        debugTxt.text = "X: " + pos.x + "/ Y: " + pos.y + "/ Z: " + pos.z;
        StartController();
	}

    public void StartController()
    {
        switch (controllerStates)
        {
            case ControllerStates.SETUP:
                statusTxt.text = "";
                if(OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger,OVRInput.Controller.Active))
                {
                    controllerStates = ControllerStates.CALIBRATION;
                    statusTxt.text = "CALIBRATING";
                }
                break;
            case ControllerStates.CALIBRATION:
                calibrationTime += Time.deltaTime;
                calibrationAnimation += Time.deltaTime;
                if(calibrationAnimation >= 0.5f)
                {
                    statusTxt.text += ".";
                    calibrationAnimation = 0f;
                }
                if(calibrationTime >= 3f)
                {
                    pointZeroPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.Active);
                    calibrationTime = 0f;
                    controllerStates = ControllerStates.TRACKING;
                }
                break;
            case ControllerStates.TRACKING:

                alertPanel.SetActive(false);
                break;
            default:
                break;
        }
    }
}
