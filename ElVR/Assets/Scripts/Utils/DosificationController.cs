﻿using System.Collections;
using System.IO;
#if UNITY_STANDALONE
using System.Runtime.Serialization.Formatters.Binary; 
#endif
using UnityEngine;

public class DosificationController : MonoBehaviour {

	public static DosificationController instance;

	public DataPacient dataPacient;
	public ExerciseData exerciseData;
	public Exercise2Data exercise2Data;
	public Exercise3Data exercise3Data;
	public Exercise4Data exercise4Data;
	public ExerciseOncologicoData exerciseOncologicoData;
	public ExerciseData exerciseDataOffline;
    //GPS data
    public float latitud;
    public float longitud;
    //Keys to connect
    public bool keyLogin = false;
    public bool keyOnline = false;
    public bool keyExercise = false;
    public bool keyGps = false;


    void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start () {

	}

    // Update is called once per frame
    void Update() {

    }

    //CONECTION WITH THE SERVER-------------------------------------------------------------
    //
    //LOGIN
    public void PostLogin(string dni, string pass)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("email", dni);
        phoneData.AddField("pass", pass);

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
        // StartCoroutine(PostdataEnumerator(www));
        StartCoroutine("PostLoginEnumerator", www);

    }
    //CORRUTINE FOR LOGIN
    IEnumerator PostLoginEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            SaveDataPacient(www);
            keyLogin = true;
            StopCoroutine("PostLoginEnumerator");
        }
        else
        {
            StopAllCoroutines();
            Debug.Log(www.error);
        }
    }
    //POST ONLINE
    public void PostOnline(string id, string status, float lat, float lon)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("id_paciente", id);
        phoneData.AddField("status", status);
        phoneData.AddField("latitud", lat.ToString());
        phoneData.AddField("longitud", lon.ToString());

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
        //StartCoroutine(PostOnlineEnumerator(www));
        StartCoroutine("PostOnlineEnumerator", www);
    }
    //CORRUTINE FOR ONLINE
    IEnumerator PostOnlineEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            Debug.Log("Online :D");
            keyOnline = true;
            //PostGetExersice("3", "4");
            //print(www.text);
            StopCoroutine("PostOnlineEnumerator");
        }
        else
        {
            print("Error");
            Debug.Log(www.error);
        }
    }
    //GET EXERCISE DATA
    public void PostGetExersice(string id, string ideE)
    {
        WWWForm phoneData = new WWWForm();
        phoneData.AddField("id_paciente", id);
        phoneData.AddField("id_ejercicio", ideE);

        //statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
        WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
        //StartCoroutine(PostGetEnumerator(www));
        StartCoroutine("PostGetEnumerator", www);
    }
    //CORRUTINE FOR EXERCISE DATA
    IEnumerator PostGetEnumerator(WWW www)
    {
        yield return www;
        if (www.error != "Null")
        {
            Debug.Log("Dosificacion");
            print(www.text);

            SaveDataExcersise(www);
            keyExercise = true;
            //DosificationController.instance.SaveEvolutionBallGame("33.2","1","12","4","4","3");
            //PostTurnsData();
            StopCoroutine("PostGetEnumerator");
        }
        else
        {
            print("Error");
            Debug.Log(www.error);
        }
    }
    //--------------------------------------------------------------------------------------

    public void SetKeyGps(bool val)
    {
        keyGps = val;
    }
    public void SetKeyExercise(bool val)
    {
        keyExercise = val;
    }
    public void SetGpsData(float latitud, float longitud)
    {
        this.latitud = latitud;
        this.longitud = longitud;
    }
    public void SaveDataPacient(WWW www)
	{
		string mainData = www.text;
		dataPacient = new DataPacient();
		dataPacient = DataPacient.CreateFromJSON(mainData);
	}

	public void SaveDataExcersise(WWW www)
	{
		string mainData = www.text;
		exerciseData = new ExerciseData();
		try
		{
			exerciseData = ExerciseData.CreateFromJSON(mainData);
		}
		catch (System.Exception)
		{
			exerciseData.status = false;
			throw;
		}
	}
	public void SaveDataExcersise2(WWW www)
	{
		string mainData = www.text;
		exercise2Data = new Exercise2Data();
		try
		{
			exercise2Data = Exercise2Data.CreateFromJSON(mainData);
		}
		catch (System.Exception)
		{
			exercise2Data.status = false;
			throw;
		}

	}

	public void SaveDataExcersise3(WWW www)
	{
		string mainData = www.text;
		exercise3Data = new Exercise3Data();
		try
		{
			exercise3Data = Exercise3Data.CreateFromJSON(mainData);
		}
		catch (System.Exception)
		{
			exercise3Data.status = false;
			throw;
		}

	}

	public void SaveDataExcersise4(WWW www)
	{
		string mainData = www.text;
		exercise4Data = new Exercise4Data();
		try
		{
			exercise4Data = Exercise4Data.CreateFromJSON(mainData);
		}
		catch (System.Exception)
		{
			exercise4Data.status = false;
			throw;
		}

	}

	public void SaveDataExcersiseOncologico(WWW www)
	{
		string mainData = www.text;
		exerciseOncologicoData = new ExerciseOncologicoData();
		try
		{
			exerciseOncologicoData = ExerciseOncologicoData.CreateFromJSON(mainData);
		}
		catch (System.Exception)
		{
			exerciseOncologicoData.status = false;
			throw;
		}

	}

	public void SaveEvolutionBallGame(string prom, string stat, string cant, string niv, string tip, string idEx,string tiros)
	{
		//promedio, status, cantidad, nivel, tipo e id_ejercicio
		print("Evolucion guardada");
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", tip);
		phoneData.AddField("id_ejercicio", idEx);
		phoneData.AddField("promedio", prom);
		phoneData.AddField("status", stat);
		phoneData.AddField("cantidad", cant);
		phoneData.AddField("nivel", niv);
		phoneData.AddField("tiros", tiros);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
		//StartCoroutine(SaveEvoEnumerator(www));

		StartCoroutine("SaveEvoEnumerator", www);
		print(phoneData.ToString());
	}

	public void SaveEvolutionFishGame(string prom, string stat, string cant,string tip,string idEx,string movimientos)
	{
		//id_ejercicio ,promedio, status, cantidad, movimientos 
		print("Evolucion guardada");
		Debug.Log (prom+"/"+stat+"/"+cant+"/"+tip+"/"+idEx+"/"+movimientos);
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_ejercicio", idEx);
		phoneData.AddField("promedio", prom);
		phoneData.AddField("tipo", tip);
		phoneData.AddField("status", stat);
		phoneData.AddField("cantidad", cant);
		phoneData.AddField("movimientos", movimientos);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);

		StartCoroutine("SaveEvoEnumerator2", www);
		print(phoneData.ToString());
	}

	public void SaveEvolutionFrangeGame(string prom, string stat, string cant,string tip,string idEx,string movimientos)
	{
		//id_ejercicio ,promedio, status, cantidad, movimientos 
		print("Evolucion guardada");
		Debug.Log (prom+"/"+stat+"/"+cant+"/"+tip+"/"+idEx+"/"+movimientos);
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_ejercicio", idEx);
		phoneData.AddField("promedio", prom);
		phoneData.AddField("tipo", tip);
		phoneData.AddField("status", stat);
		phoneData.AddField("cantidad", cant);
		phoneData.AddField("movimientos", movimientos);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);

		StartCoroutine("SaveEvoEnumerator3", www);
		print(phoneData.ToString());
	}

	public void SaveEvolutionBridgeGame(string prom, string stat, string cant,string tip,string idEx,string movimientos)
	{
		//id_ejercicio ,promedio, status, cantidad, movimientos 
		print("Evolucion guardada");
		Debug.Log (prom+"/"+stat+"/"+cant+"/"+tip+"/"+idEx+"/"+movimientos);
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_ejercicio", idEx);
		phoneData.AddField("promedio", prom);
		phoneData.AddField("tipo", tip);
		phoneData.AddField("status", stat);
		phoneData.AddField("cantidad", cant);
		phoneData.AddField("movimientos", movimientos);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);

		StartCoroutine("SaveEvoEnumerator4", www);
		print(phoneData.ToString());
	}


	public void SaveEvolutionOncologicoGame(string prom, string stat, string cantAtra,string cantApa,string tip,string idEx,string movimientos)
	{
		//id_ejercicio ,promedio, status, cantidad, movimientos 
		print("Evolucion guardada");
		Debug.Log (prom+"/"+stat+"/"+cantAtra+"/"+cantApa+"/"+tip+"/"+idEx+"/"+movimientos);
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_ejercicio", idEx);
		phoneData.AddField("promedio", prom);
		phoneData.AddField("tipo", tip);
		phoneData.AddField("status", stat);
		phoneData.AddField("cantidadAnimalesAparecieron", cantApa);
		phoneData.AddField("cantidadAnimalesAtrapados", cantAtra);
		phoneData.AddField("movimientos", movimientos);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);

		StartCoroutine("SaveEvoEnumeratorOncologico", www);
		print(phoneData.ToString());
	}

	public void SaveServerDataToPhone(string id_paciente, string id_ejercicio, object data)
	{
#if UNITY_STANDALONE
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + id_paciente + "_" + id_ejercicio + "_" + "Info.dat");
        bf.Serialize(file, data);
        file.Close(); 
#endif
    }

    public void LoadDataFromPhone(string id_paciente, string id_ejercicio, object data)
	{
#if UNITY_STANDALONE
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + "fecha" + "_" + id_paciente + "_" + id_ejercicio + "_" + "Info.dat");
        data = (ExerciseData)bf.Deserialize(file);
        file.Close(); 
#endif
    }

    public void PostCompletaEjercicio()
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", "4");
		phoneData.AddField("id_ejercicio", exerciseData.id_ejercicio);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostCompletaEjercicioEnumerator", www);
	}

	public void PostCompletaEjercicio2()
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", "5");
		phoneData.AddField("id_ejercicio", exercise2Data.id_ejercicio);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostCompletaEjercicioEnumerator", www);
	}

	public void PostCompletaEjercicio3()
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", "6");
		phoneData.AddField("id_ejercicio", exercise3Data.id_ejercicio);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostCompletaEjercicioEnumerator", www);
	}

	public void PostCompletaEjercicio4()
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", "7");
		phoneData.AddField("id_ejercicio", exercise4Data.id_ejercicio);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostCompletaEjercicioEnumerator", www);
	}

	public void PostCompletaEjercicioOncologico()
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("tipo", "8");
		phoneData.AddField("id_ejercicio", exerciseOncologicoData.id_ejercicio);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostCompletaEjercicioEnumerator", www);
	}

	public void PostOffline(string id, string status, float lon, float lat)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("status", status);
		phoneData.AddField("longitud", lon.ToString());
		phoneData.AddField("latitud", lat.ToString());

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
		//StartCoroutine(PostOnlineEnumerator(www));
		StartCoroutine("PostOfflineEnumerator", www);
	}

	IEnumerator PostOfflineEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			StopCoroutine("PostOfflineEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}

	IEnumerator SaveEvoEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			print("guardado");
			print(www.text);
			//	PostOffline(exerciseData.id_paciente, "0", ControladorFishingGame.instance.GetLongitud(),ControladorFishingGame.instance.GetLatitiud());
			PostOffline(exerciseData.id_paciente, "0",215615f,532324f);
			StopCoroutine("SaveEvoEnumerator");
		}
		else
		{	
			print(":(");
			Debug.Log(www.error);
		}
	}
	IEnumerator SaveEvoEnumerator2(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			print("guardado");
			print(www.text);
			//	PostOffline(exercise2Data.id_paciente, "0", ControladorFishingGame.instance.GetLongitud(),ControladorFishingGame.instance.GetLatitiud());
			PostOffline(exercise2Data.id_paciente, "0",215615f,532324f);
			StopCoroutine("SaveEvoEnumerator2");
		}
		else
		{	
			print(":(");
			Debug.Log(www.error);
		}
	}

	IEnumerator SaveEvoEnumerator3(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			print("guardado");
			print(www.text);
			PostOffline(exercise3Data.id_paciente, "0",215615f,532324f);
			StopCoroutine("SaveEvoEnumerator3");
		}
		else
		{	
			print(":(");
			Debug.Log(www.error);
		}
	}

	IEnumerator SaveEvoEnumerator4(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			print("guardado");
			print(www.text);
			PostOffline(exercise4Data.id_paciente, "0",215615f,532324f);
			StopCoroutine("SaveEvoEnumerator4");
		}
		else
		{	
			print(":(");
			Debug.Log(www.error);
		}
	}
	IEnumerator SaveEvoEnumeratorOncologico(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			print("guardado");
			print(www.text);
			PostOffline(exerciseOncologicoData.id_paciente, "0",215615f,532324f);
			StopCoroutine("SaveEvoEnumeratorOncologico");
		}
		else
		{	
			print(":(");
			Debug.Log(www.error);
		}
	}

	IEnumerator PostCompletaEjercicioEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			//PostOffline(exerciseData.id_paciente, "0");
			StopCoroutine("PostCompletaEjercicioEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
}

[System.Serializable]
public class DataPacient
{
	public string id_paciente;
	public string id_doctor;
	public string fecha;
	public string imagen;
	public string dni;
	public string nombre;
	public string apellido;
	public string sexo;
	public string edad;
	public string fono;
	public string nacimiento;
	public string estado_civil;
	public string grupo_sanguineo;
	public string dni_acom;
	public string nombres_acom;
	public string apellidos_acom;
	public string edad_acom;
	public string fono_acom;
	public string profesion_acom;
	public string profesion;
	public string id_clinica;
	public string id_user;
	public string id_pais;
	public string id_provincia;
	public string id_localidad;
	public string enf_personales;
	public string enf_familiares;
	public string otros_personales;
	public string otros_familiares;
	public string observaciones;
	public string control;
	public string online;
	public string email_user;
	public string pass_user;
	public string type;
	public string status;
	public string digitos;
	public string rol;

	public static DataPacient CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<DataPacient>(jsonString);
	}
}

[System.Serializable]
public class ExerciseData
{
	public string id_ejercicio;
	public string id_escenario;
	public string id_paciente;
	public string momento;
	public string grado;
	//public string repeticiones;
	public string nivel;
	public string duracion;
	public string fecha;
	public bool status;
	public string promedio;
	public string gano;
	public string cantidad;
	public string nivel_final;

	public static ExerciseData CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<ExerciseData>(jsonString);        
	}
}

[System.Serializable]
public class Exercise2Data
{
	public string id_ejercicio;
	public string id_paciente;
	public string fecha;
	public string duracion;
	public string altura;
	public string velocidad;
	public string espera;
	public bool tipo_guia;
	public string guiado;
	public string pez_rojo;
	public string distancia;
	public bool status;

	public static Exercise2Data CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<Exercise2Data>(jsonString);        
	}
}

[System.Serializable]
public class Exercise3Data
{
	public string id_ejercicio;
	public string id_paciente;
	public string fecha;
	public int posicion;
	public int velocidad;
	public int tipo_ejercicio;
	public int tiempo_cantidad;

	public bool status;

	public static Exercise3Data CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<Exercise3Data>(jsonString);        
	}
}

[System.Serializable]
public class Exercise4Data
{
	public string id_ejercicio;
	public string id_paciente;
	public string fecha;
	public int posicion;
	public int tipo_ejercicio;
	public int tiempo;

	public bool status;

	public static Exercise4Data CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<Exercise4Data>(jsonString);        
	}
}

[System.Serializable]
public class ExerciseOncologicoData
{
	public string id_ejercicio;
	public string id_paciente;
	public string fecha;
	public int tipo_ejercicio;
	public int tiempo_espera;
	public int tiempo;
	public string repeticiones;
	public bool status;

	public static ExerciseOncologicoData CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<ExerciseOncologicoData>(jsonString);        
	}
}