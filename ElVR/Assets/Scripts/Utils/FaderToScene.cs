﻿using FI.TAGS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FaderToScene : MonoBehaviour
{

    public enum FaderStates
    {
        FADINGIN,
        FADINGOUT,
        CHANGING
    }

    public static FaderToScene instance;
    public GameObject gazePanel;
    public bool active;
    public float speed;
    public int sceneNumber;

    public FaderStates faderStates;
    //For fading
    private Color fadeColor;
    private float timerFade;

    void Awake()
    {
        instance = this;
        fadeColor = gazePanel.GetComponent<MeshRenderer>().material.color;
    }
    // Use this for initialization
    void Start()
    {
        faderStates = FaderStates.FADINGOUT;
        timerFade = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            switch (faderStates)
            {
                case FaderStates.FADINGIN:
                    if (fadeColor.a < 1f)
                        fadeColor.a += 0.3f * Time.deltaTime * speed;

                    timerFade += Time.deltaTime;

                    if (timerFade > 2f)
                    {
                        faderStates = FaderStates.CHANGING;
                        timerFade = 0f;
                    }

                    break;
                case FaderStates.FADINGOUT:
                    if (fadeColor.a > 0f)
                        fadeColor.a -= 0.3f * Time.deltaTime * speed;
                    timerFade += Time.deltaTime;

                    if (timerFade > 2f)
                    {
                        active = false;
                        timerFade = 0f;
                    }
                    break;
                case FaderStates.CHANGING:
                    //SceneManager.LoadScene(ScenesTags.intro);
                    active = false;
                    break;
                default:
                    break;
            }
            gazePanel.GetComponent<MeshRenderer>().material.color = fadeColor;
        }
    }
}
