﻿using UnityEngine;
using UnityEngine.Events;

public class VRRaycaster : MonoBehaviour
{
    public enum ControllType
    {
        GAZE,
        REMOTE
    }

    [System.Serializable]
    public class Callback : UnityEvent<Ray, RaycastHit> { }

    public static VRRaycaster instance;
    public Transform leftHandAnchor = null;
    public Transform rightHandAnchor = null;
    public Transform centerEyeAnchor = null;
    public LineRenderer lineRenderer = null;
    public GameObject gazePointer = null;
    public float maxRayDistance = 500.0f;
    public LayerMask excludeLayers;
    public VRRaycaster.Callback raycastHitCallback;
    public ControllType controllType;

    //Object that stores the object that hit the raycast
    public GameObject touchedGameObject;
    public GameObject touchedGameObjectGaze;

    public bool useGaze;
    public bool inGame;
    
    void Awake()
    {
        instance = this;
        inGame = false;
        controllType = ControllType.REMOTE;
        if (leftHandAnchor == null)
        {
            Debug.LogWarning("Assign LeftHandAnchor in the inspector!");
            GameObject left = GameObject.Find("LeftHandAnchor");
            if (left != null)
            {
                leftHandAnchor = left.transform;
            }
        }
        if (rightHandAnchor == null)
        {
            Debug.LogWarning("Assign RightHandAnchor in the inspector!");
            GameObject right = GameObject.Find("RightHandAnchor");
            if (right != null)
            {
                rightHandAnchor = right.transform;
            }
        }
        if (centerEyeAnchor == null)
        {
            Debug.LogWarning("Assign CenterEyeAnchor in the inspector!");
            GameObject center = GameObject.Find("CenterEyeAnchor");
            if (center != null)
            {
                centerEyeAnchor = center.transform;
            }
        }
        lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer == null)
        {
            Debug.LogWarning("Assign a line renderer in the inspector!");
            
            Color start = Color.white;        
            Color end = Color.white;
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            
            lineRenderer.receiveShadows = false;
            lineRenderer.widthMultiplier = 0.02f;
            lineRenderer.startColor = start;
            lineRenderer.endColor = end;
            
        }
    }

    Transform Pointer
    {
        get
        {
            OVRInput.Controller controller = OVRInput.GetConnectedControllers();
            if ((controller & OVRInput.Controller.LTrackedRemote) != OVRInput.Controller.None)
            {
                useGaze = false;
                gazePointer.SetActive(false);
                return leftHandAnchor;

            }
            else if ((controller & OVRInput.Controller.RTrackedRemote) != OVRInput.Controller.None)
            {
                useGaze = false;
                gazePointer.SetActive(false);
                return rightHandAnchor;
            }
            // If no controllers are connected, we use ray from the view camera. 
            // This looks super ackward! Should probably fall back to a simple reticle!
            if(!inGame) //--- new line
            {
                gazePointer.SetActive(true);               
            }
            useGaze = true;
            return centerEyeAnchor;

            //if(controllType == ControllType.GAZE)
            //{
            //    gazePointer.SetActive(true);
            //    return centerEyeAnchor;
            //}
            //else
            //{
            //    gazePointer.SetActive(false);
            //    return rightHandAnchor;
            //}

        }

        }
    

    void Update()
    {
        Transform pointer = Pointer;
        if (pointer == null)
        {
            return;
        }

        Ray laserPointer = new Ray(pointer.position, pointer.forward);
        Ray laserPointerGaze = new Ray(centerEyeAnchor.position, centerEyeAnchor.forward);

        if (lineRenderer != null)
        {
            
            if(!useGaze)
            {
                lineRenderer.SetPosition(0, laserPointer.origin);
                lineRenderer.SetPosition(1, laserPointer.origin + laserPointer.direction * maxRayDistance);
            }
                

        }

        RaycastHit hitGaze;
        if(Physics.Raycast(laserPointerGaze, out hitGaze, maxRayDistance, ~excludeLayers))
        {
            if (raycastHitCallback != null)
            {
                raycastHitCallback.Invoke(laserPointer, hitGaze);

            }
            touchedGameObjectGaze = hitGaze.transform.gameObject;
        }



        RaycastHit hit;
        if (Physics.Raycast(laserPointer, out hit, maxRayDistance, ~excludeLayers))
        {
            if (lineRenderer != null && !useGaze && !inGame)
            {
                if (controllType == ControllType.REMOTE)
                {
                    lineRenderer.SetPosition(1, hit.point);
                }
            }

            if (raycastHitCallback != null)
            {
                raycastHitCallback.Invoke(laserPointer, hit);

                //hit.transform.gameObject.SetActive(false);
                //hit.transform.gameObject.GetComponent<Renderer>().material.color = Color.cyan;
            }

            touchedGameObject = hit.transform.gameObject;
        }
    }
}
