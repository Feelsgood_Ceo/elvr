﻿using System.Collections;
using UnityEngine;

public class LocationService : MonoBehaviour {
    IEnumerator Start()
    {
        print("entro1");
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;
        print("entro2");
        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            //PhoneDataManager.instance.statusTxt.text = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
            // PhoneDataManager.instance.SetLocalitation(Input.location.lastData.latitude, Input.location.lastData.longitude);
            //PhoneDataManager.instance.isReadingGPS = false;
            //------------------------------------------------------!!!!!
            //THIS LINES CHANGES A VALUE FROM THE VARIABLE KEYGPS, THE KEY IS NO LOGER AVALIABLE IN THIS SCRIPT, INSTEAD READ THE VARIABLE
            //FROM THE DOSIFICATION CONTROLLER
            //-------------------------------------------------------!!!!!
            //BallGameController.instance.SetCoordinates(Input.location.lastData.latitude, Input.location.lastData.longitude);
            //BallGameController.instance.SetKeyGps(true);
            DosificationController.instance.SetGpsData(Input.location.lastData.latitude, Input.location.lastData.longitude);
            DosificationController.instance.SetKeyGps(true);

        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
}
