﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayAndTimeController : MonoBehaviour {

    //Variable to be modify from the server
    public enum MomentOfDay
    {
        DAY,
        AFTERNOON,
        NIGHT
    }

    //Singleton
    public static DayAndTimeController instance;

    //Main directional Light from the stage
    public GameObject sun;
    public MomentOfDay momentOfDay;

    //Component light from the directional light game object
    private Light lightOfSun;
    
	void Awake()
    {
        instance = this;
    }
	void Start () {
        lightOfSun = sun.GetComponent<Light>();
        ChangeMoment(momentOfDay);
    }

    //This function can be call from another script
    public void ChangeMoment(MomentOfDay moment)
    {
        switch (moment)
        {
            case MomentOfDay.DAY:
                lightOfSun.intensity = 0.9f;
                break;
            case MomentOfDay.AFTERNOON:
                lightOfSun.intensity = 0.4f;
                break;
            case MomentOfDay.NIGHT:
                lightOfSun.intensity = 0.1f;
                break;
            default:
                break;
        }
    }


}
