﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fading : MonoBehaviour {

    public GameObject fadePanel;
    public static Fading instance;
    public bool fading;
    public float speed;

    private Color fadeColor;
    private float timerFade;

    void Awake()
    {
        instance = this;
        fadeColor = fadePanel.GetComponent<MeshRenderer>().material.color;
    }

    void Start()
    {
        fading = false;
        timerFade = 0f;
    }

    void Update()
    {
        if(fading)
        {
            if(fadeColor.a<1f)
            fadeColor.a += 0.3f*Time.deltaTime * speed;

            timerFade += Time.deltaTime;

            if (timerFade > 2f)
            {
                fading = false;
                timerFade = 0f;
            }
        }
        else
        {
            if(fadeColor.a>0f)
            fadeColor.a -= 0.1f * Time.deltaTime * speed;

           
        }

        
        fadePanel.GetComponent<MeshRenderer>().material.color = fadeColor;
    }
   
}
