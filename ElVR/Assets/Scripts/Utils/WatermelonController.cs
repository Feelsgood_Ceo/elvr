﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatermelonController : MonoBehaviour {

    enum WatermelonStates
    {
        IDLE
    }

    private WatermelonStates watermelonStates;
    private float runningTime = 0f;

    // Use this for initialization
    void Start () {
        watermelonStates = WatermelonStates.IDLE;
	}
	
	// Update is called once per frame
	void Update () {
        switch (watermelonStates)
        {
            case WatermelonStates.IDLE:
                Vector3 newPos = transform.position;
                float deltaHeight3 = (Mathf.Sin(runningTime + Time.deltaTime) - Mathf.Sin(runningTime));
                newPos.y += deltaHeight3 * 0.05f;
                runningTime += Time.deltaTime;
                transform.position = newPos;
                break;
            default:
                break;
        }
    }

    //Call the state change in main script BallGameController
    public void ChangeToWelcome()
    {
        BallGameController.instance.WaterMelonTrigger();
    }
}
