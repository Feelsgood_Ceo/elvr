﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;
using System.Collections;

public struct PointerEventArgs
{
    public uint controllerIndex;
    public uint flags;
    public float distance;
    public Transform target;
}

public delegate void PointerEventHandler(object sender, PointerEventArgs e);


public class SteamVR_LaserPointer : MonoBehaviour
{
	public int TipoLaser;
	public float turnSpeed;
	public float maxDistanceRay;
	public float hitTimeBreak;
	private float hitTime;
	public Transform HELP;
	public bool bHit;
    public bool active = true;
	public Material mat;
    public Color color;
	public Color color2;
    public float thickness = 0.002f;
    public GameObject holder;
    public GameObject pointer;
    public bool addRigidBody = false;
    public Transform reference;
	private Vector3 referenceposition;
    public event PointerEventHandler PointerIn;
    public event PointerEventHandler PointerOut;
	private Material newMaterial;
    Transform previousContact = null;
	public bool isTriggerPress=false;
	public static bool isGet=false;
	public bool isSet = false;
	public static bool isInPosition;
	// Use this for initialization
	void Start ()
    {
        holder = new GameObject();
        holder.transform.parent = this.transform;
        holder.transform.localPosition = Vector3.zero;
		holder.transform.localRotation = Quaternion.identity;

		pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointer.transform.parent = holder.transform;
        pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
        pointer.transform.localPosition = new Vector3(0f, 0f, 50f+0.5f);
		pointer.transform.localRotation = Quaternion.identity;
		BoxCollider collider = pointer.GetComponent<BoxCollider>();
        if (addRigidBody)
        {
            if (collider)
            {
                collider.isTrigger = true;
            }
            Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
            rigidBody.isKinematic = true;
        }
        else
        {
            if(collider)
            {
                Object.Destroy(collider);
            }
        }
   //     newMaterial = new Material(Shader.Find("Unlit/Color"));
		mat.SetColor("_Color", color);
		pointer.GetComponent<MeshRenderer>().material = mat;
	}

    public virtual void OnPointerIn(PointerEventArgs e)
    {
        if (PointerIn != null)
            PointerIn(this, e);
    }

    public virtual void OnPointerOut(PointerEventArgs e)
    {
        if (PointerOut != null)
            PointerOut(this, e);
    }

	void Update(){
		if (TipoLaser == 1) {
			UpdateLaser ();
		} else {
			UpdateLaser2 ();
		}
	}
    // Update is called once per frame
	void UpdateLaser ()
    {
		float dist = maxDistanceRay;

    //    SteamVR_TrackedController controller = GetComponent<SteamVR_TrackedController>();

        Ray raycast = new Ray(transform.position, transform.forward);
        RaycastHit hit;
		bHit = Physics.Raycast(raycast, out hit,maxDistanceRay);

		if (ControladorBridgeGame.instance.pruebaEditor) {

			if(Input.GetKey(KeyCode.DownArrow)){
				HELP.Rotate(Vector3.left, -turnSpeed * Time.deltaTime);
			}
			if(Input.GetKey(KeyCode.UpArrow)){
				HELP.Rotate(Vector3.left, turnSpeed * Time.deltaTime);
			}
			if(Input.GetKey(KeyCode.LeftArrow))
				HELP.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);

			if(Input.GetKey(KeyCode.RightArrow))
				HELP.Rotate(Vector3.up, turnSpeed * Time.deltaTime);

			if (Input.GetKey (KeyCode.G)) {
				isTriggerPress = true;
			} else {
				if (isSet) {
					ControladorBridgeGame.MaderasPuestas++;
					reference.position = hit.transform.position;
					reference.rotation = hit.transform.rotation;
					reference.localScale = hit.transform.localScale;
					hit.transform.gameObject.SetActive (false);
				} else {
				
					//reference.position = referenceposition;
					if (reference != null)
						reference.GetComponent<Collider> ().enabled = true;
				}
				reference = null;
				isTriggerPress = false;
				isGet = false;
			}
		} else {
			if(OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote)){
				isTriggerPress = true;
			}else{
				if (isSet) {
					ControladorBridgeGame.MaderasPuestas++;
					reference.position = hit.transform.position;
					reference.rotation = hit.transform.rotation;
					reference.localScale = hit.transform.localScale;
					hit.transform.gameObject.SetActive (false);
				}else{
					//reference.position = referenceposition;
					if(reference!=null)
						reference.GetComponent<Collider> ().enabled = true;
				}
				isTriggerPress = false;
				isGet = false;
			}
		}

		if (bHit) {
			
			if (hit.transform.name == "MADERA"&&isTriggerPress&&!isGet) {
				isGet = true;
				reference = hit.transform;
			//	referenceposition = reference.position;
				reference.GetComponent<Collider> ().enabled = false;
				mat.SetColor("_Color", color2);
			}

			if (isGet) {
				reference.position = hit.point;
			} else {
				mat.SetColor("_Color", color);
			}

			if (isGet && hit.transform.name == "posicionPorPoner") {
				isSet = true;
				hit.transform.GetComponent<MeshRenderer> ().enabled=true;
			} else {
				isSet = false;
			}
				
		} else {
			if (reference != null)
				reference.position = raycast.GetPoint (maxDistanceRay-1f);
			isSet = false;
		}


        if(previousContact && previousContact != hit.transform)
        {
            PointerEventArgs args = new PointerEventArgs();
            args.distance = 0f;
            args.flags = 0;
            args.target = previousContact;
			if (previousContact.transform.name == "posicionPorPoner") {
				previousContact.transform.GetComponent<MeshRenderer> ().enabled=false;
			}
            OnPointerOut(args);
            previousContact = null;
        }
        if(bHit && previousContact != hit.transform)
        {
            PointerEventArgs argsIn = new PointerEventArgs();			
            argsIn.distance = hit.distance;
            argsIn.flags = 0;
            argsIn.target = hit.transform;
            OnPointerIn(argsIn);
            previousContact = hit.transform;

        }
        if(!bHit)
        {
            previousContact = null;
        }
		if (bHit && hit.distance < maxDistanceRay)
        {
            dist = hit.distance;
        }
			
		pointer.transform.localScale = new Vector3(thickness, thickness,dist);
		pointer.transform.localPosition = new Vector3(0f, 0f, (dist/2f)+0.5f);
    }

	IEnumerator destruye(Transform tra){
		yield return new WaitForSeconds (0.4f);
		Debug.Log ("eliminando");
		Destroy (tra.gameObject);
		ControladorHeadGame.isCreate = false;
	}
	 
	void UpdateLaser2()
	{
		if(Input.GetKey(KeyCode.DownArrow)){
			HELP.Rotate(Vector3.left, -turnSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.UpArrow)){
			HELP.Rotate(Vector3.left, turnSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.LeftArrow))
			HELP.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.RightArrow))
			HELP.Rotate(Vector3.up, turnSpeed * Time.deltaTime);

		float dist = maxDistanceRay;

		Ray raycast = new Ray(transform.position, transform.forward);
		RaycastHit hit;
		bHit = Physics.Raycast(raycast, out hit,maxDistanceRay);

		if (bHit) {

			if (hit.transform.name == "POINT") {
				hitTime += Time.deltaTime;
				Debug.Log (hitTime);
				if (hitTimeBreak <= hitTime && hit.transform.GetComponent<ControladorAnimalPrefab>().animalState==ControladorAnimalPrefab.AnimalState.IDLE) {
					ControladorHeadGame.instance.sonidoFeedBack.Play ();
					hit.transform.GetComponent<ControladorAnimalPrefab> ().isCaptured = true;
				/*	hit.transform.GetComponent<Collider> ().enabled = false;
					hit.transform.GetComponent<MeshRenderer> ().enabled = false;
					hit.transform.GetChild (0).gameObject.SetActive (true);
					StartCoroutine (destruye (hit.transform));
				*/}
			} else {
				hitTime = 0;
			}
				
		} else {
			hitTime = 0;
		}


		if(previousContact && previousContact != hit.transform)
		{
			PointerEventArgs args = new PointerEventArgs();
			args.distance = 0f;
			args.flags = 0;
			args.target = previousContact;
			OnPointerOut(args);
			previousContact = null;
		}
		if(bHit && previousContact != hit.transform)
		{
			PointerEventArgs argsIn = new PointerEventArgs();			
			argsIn.distance = hit.distance;
			argsIn.flags = 0;
			argsIn.target = hit.transform;
			OnPointerIn(argsIn);
			previousContact = hit.transform;

		}
		if(!bHit)
		{
			previousContact = null;
		}
		if (bHit && hit.distance < maxDistanceRay)
		{
			dist = hit.distance;
		}

		pointer.transform.localScale = new Vector3(thickness*dist, thickness*dist, 1f);
		pointer.transform.localPosition = new Vector3(0f, 0f,(dist/2f)+0.5f);
	}
}
