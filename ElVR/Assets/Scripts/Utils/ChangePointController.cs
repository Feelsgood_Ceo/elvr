﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePointController : MonoBehaviour {
    
    public List<GameObject> points;
    public GameObject cameraObj;
    [Header("Controller")]
    public GameObject controllerModel;

    private int cameraPos = 1;
    private bool canTrigger;
    private float timer;
	
	void Start () {
        timer = 0f;
        canTrigger = true;
        ChangecameraObj();
        
    }	
	
	void Update () {

        bool MoveCam = false;

#if UNITY_EDITOR
        
        MoveCam = Input.GetKeyDown(KeyCode.Space);
        if (MoveCam && canTrigger)
        {
            Fading.instance.fading = true;
			if (cameraPos == points.Count)
            {
                
                FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
                FaderToScene.instance.active = true;
            }
            else
            {
                cameraPos++;
            }
            canTrigger = false;
        }

#elif UNITY_ANDROID

        MoveCam = OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote;
        MoveCam = MoveCam && OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad);

        if(MoveCam && canTrigger )
        {
            Vector2 touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
            if(touchPosition.x > touchPosition.y)
            {
                Fading.instance.fading = true;
                if (cameraPos == points.Count)
                {
                    FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
                    FaderToScene.instance.active = true;
                }
                else
                {
                    cameraPos++;
                }
                canTrigger = false;
            }
        }
        
#endif



        if (!canTrigger)
        {
            timer += Time.deltaTime;

            if(timer >= 2f)
            {
                ChangecameraObj();
            }
            if(timer >= 3f)
            {
                canTrigger = true;
                timer = 0f;
            }
        }

    }

    public void ChangecameraObj()
    {
        /*switch (cameraPos)
        {
            case 0:
                cameraObj.transform.position = points[0].transform.position;
                cameraObj.transform.rotation = points[0].transform.rotation;
                break;
            case 1:
                cameraObj.transform.position = points[1].transform.position;
                cameraObj.transform.rotation = points[1].transform.rotation;
                break;
            case 2:
                cameraObj.transform.position = points[2].transform.position;
                cameraObj.transform.rotation = points[2].transform.rotation;
                break;
        }*/

		cameraObj.transform.position = points [cameraPos-1].transform.position;
		cameraObj.transform.rotation = points [cameraPos-1].transform.rotation;

    }

    public void ActiveZoomButton()
    {
        controllerModel.GetComponent<Animator>().Play("ControlBtnHomeZoom"); // Here!
    }
}
