﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class ForceCamPosition : MonoBehaviour {

    public Transform camera;
    public Transform player;

    public Transform canvas;
	// Use this for initialization
	void Awake () {

        /*Vector3 LensPos =
        InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.Head);

        cam.transform.localPosition =
            new Vector3(cam.transform.position.x, LensPos.y + 40.0f, cam.transform.position.z);*/
        //InputTracking.Recenter();


        Application.targetFrameRate = 60;
        //QualitySettings.vSyncCount = 2;

    }

    private void Start()
    {
        canvas.transform.position = player.transform.position;
    }

    // Update is called once per frame
    void Update () {
        //cam.transform.localPosition = new Vector3(cam.transform.localPosition.x, 0.0f, cam.transform.localPosition.z);
        //Debug.Log(cam.transform.position.y);
        //Debug.Log("Head rotation: " + InputTracking.GetLocalRotation(UnityEngine.XR.XRNode.Head));

        //UICamera.transform.localRotation = camera.transform.localRotation;


        /*if (Input.GetKeyDown(KeyCode.Space))
            RecenterCamera();

        if (Input.GetKeyDown(KeyCode.Return))
            StartCoroutine(LocalPositionY());*/
    }

    IEnumerator LocalPositionY()
    {
        Debug.Log("Repositioning");
        Vector3 LensPos =
        InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.Head);

        Debug.Log(LensPos.y);
        Debug.Log(camera.position.y);
        yield return new WaitForEndOfFrame();

        //StartCoroutine(RecenterCamera());
    }

    /// <summary>
    /// Recentering camera
    /// </summary>
    IEnumerator RecenterCamera() {
        Debug.Log("Recentering camera...");
        InputTracking.Recenter();
        yield return new WaitForEndOfFrame();
    }
}
