﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using System.Linq;
using System;
#if !UNITY_EDITOR
using Windows.ApplicationModel.Core;
#endif

public class BreathCameraChange : MonoBehaviour {

    public Transform[] points;
    public Transform player;
    public Text referenceText;
    public GameObject bubble;
    
    public int index = 0;

    private void Awake()
    {
        //XRDevice.SetTrackingSpaceType(TrackingSpaceType.Stationary);
        InputTracking.disablePositionalTracking = true;

        InputTracking.Recenter();

        referenceText.text = "ESCENARIO ACTUAL: P" + (index + 1);
        ChangeCameraPosition(index);

        /*var array = System.Enum.GetValues(typeof(ScenarioPoints));

        for (int i = 0; i < array.Length; i++) {
            Debug.Log("i: " + i + ", content: " + array.GetValue(i));
        }

        Debug.Log("4 está definido?: " + System.Enum.IsDefined(typeof(ScenarioPoints), 4));
        Debug.Log("18 está definido?: " + System.Enum.IsDefined(typeof(ScenarioPoints), 18));
        Debug.Log(System.Enum.Parse(typeof(ScenarioPoints), System.Enum.GetName(typeof(ScenarioPoints), 18)));

        Debug.Log((ScenarioPoints)4);

        bool isValueDefined = System.Enum.IsDefined(typeof(ScenarioPoints), 18);

        if (isValueDefined) {
            //Getting Enum Name value
            string scenarioName = System.Enum.GetName(typeof(ScenarioPoints), 18);
            
            //Creating a enum array
            var scenarioPoints = System.Enum.GetValues(typeof(ScenarioPoints));

            //Checking index value
            for (int i = 0; i < scenarioPoints.Length; i++)
            {
                if (scenarioPoints.GetValue(i).ToString() == scenarioName)
                {
                    Debug.Log("index: " + i);
                    break;
                }
            }

            //Setting value
        }*/

        
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            ChangeIndex(+1);
            ChangeCameraPosition(index);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ChangeIndex(-1);
            ChangeCameraPosition(index);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {

            Application.Quit();

            #if !UNITY_EDITOR
            CoreApplication.Exit();
            #endif
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            bubble.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            bubble.SetActive(false);
        }

        //RenderSettings.ambientIntensity -= 0.1f;
    }

    void ChangeIndex(int value) {
        index = index + value;

        if (index == points.Length)
            index = 0;
        else if (index < 0)
            index = points.Length - 1;

        
    }

    void ChangeCameraPosition(int index) {
        player.transform.position =
            points[index].transform.position;

        player.transform.rotation =
            points[index].transform.rotation;

        referenceText.text = "ESCENARIO ACTUAL: P" + (index + 1);

        GameObject.FindGameObjectWithTag("MainCamera").transform.localPosition =
            Vector3.zero;

        GameObject.FindGameObjectWithTag("MainCamera").transform.localRotation =
            new Quaternion(0, 0, 0, 0);

        ForcePosition(index);

        //InputTracking.Recenter();
    }

    void ForcePosition(int position) {
        GameObject.Find("GameModeBreathing").SendMessage("ForceActualPosition", position);
    }
}
