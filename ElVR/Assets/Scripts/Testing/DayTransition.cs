﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayTransition : MonoBehaviour {

    //Skybox materials
    public Material day;
    public Material night;
    public Material noon;

    public GameObject sun;

    private Light sunLight;
    private Transform sunTransform;

    public Transform puntoCentro;

    public float rotationFactor;
    public ClimaControl clima;

    float factor;

    // Use this for initialization
    void Start () {
        //sunLight = sun.GetComponent<Light>();
        //sunTransform = sun.transform;

        RenderSettings.skybox.SetFloat("_Exposure", 1.0f);

        factor = clima.Incrementador / 25.0f;
        Debug.Log(factor);

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            RenderSettings.skybox = day;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            RenderSettings.skybox = night;
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            RenderSettings.skybox = noon;
        }

        /*sunTransform.localRotation =
            new Quaternion(
                sunTransform.localRotation.x + rotationFactor,
                sunTransform.localRotation.y + rotationFactor,
                sunTransform.localRotation.z + rotationFactor,
                sunTransform.localRotation.z);*/

        Debug.Log(puntoCentro.rotation.eulerAngles.x);

        //float val = (puntoCentro.rotation.eulerAngles.x / 360.0f) -0.25f;

        //Debug.Log(puntoCentro.rotation.eulerAngles.x -90.0f);

        //Debug.Log(puntoCentro.rotation.eulerAngles.x / 360.0f);
        //Debug.Log(puntoCentro.localRotation.x / 90.0f);

        //RenderSettings.skybox.SetFloat("_Exposure", -val);

        if (puntoCentro.rotation.eulerAngles.x > 0 && puntoCentro.rotation.eulerAngles.x < 90 &&
            RenderSettings.skybox.GetFloat("_Exposure") > 0.0f) {

            float exposure = RenderSettings.skybox.GetFloat("_Exposure") - factor;

            RenderSettings.skybox.SetFloat("_Exposure", exposure );

        }

        if (puntoCentro.rotation.eulerAngles.x > 270 && puntoCentro.rotation.eulerAngles.x < 360
            && RenderSettings.skybox.GetFloat("_Exposure") <= 1.0f)
        {

            float exposure = RenderSettings.skybox.GetFloat("_Exposure") + factor;

            RenderSettings.skybox.SetFloat("_Exposure", exposure);

        }
    }

    
}
