﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorFishingGame : MonoBehaviour {

	struct MoveDataStruct

	{
		private List<float> x;
		private List<float> y;
		private List<float> z;

		public List<float> X
		{
			get
			{
				return x;
			}

			set
			{
				x = value;
			}
		}

		public List<float> Y
		{
			get
			{
				return y;
			}

			set
			{
				y = value;
			}
		}

		public List<float> Z
		{
			get
			{
				return z;
			}

			set
			{
				z = value;
			}
		}
			
	}

	public enum FishingGameState
	{
		LOGIN,
		NOSESION,
		WELCOME,
		WELCOME2,
		TUTORIAL,
		TUTORIAL2,
		TUTORIAL3,
		TUTORIAL4,
		TUTORIAL5,
		TUTORIAL6,
		SPAWN1,
		SPAWN2,
		SPAWN3,
		COUNTER,
		ELVRMOVEMENT,
		PLAYING,
		GAMEOVER,
		ENDGAME
	}

	public static ControladorFishingGame instance;

	public int E_tiempoEjercicio;
	public int E_Dificultad;
	public float E_Velocidad;
	public float E_TiempoIdle;
	public bool E_isRandom=true;
	public string E_Orden;
	public int E_probabilidadPezRojo;
	public float E_DistanciaPeces;
	public Transform Points;
	public Transform StartPoint;
	public Transform FinishPoint;
	public Transform CestoPoint;
	public Transform Red;
	public Transform[] SpawnPoints;
	public Transform prefabPez;
	public Transform TimerTransform;
	public Color[] colores;
	public static bool isSwimming=false;
	public static bool isSaved=false;
	//TIMER
	public string timerText;
	private float secondsCount;
	private float secondsCount2;
	private int minuteCount;
	private int hourCount;

	//Textures for tutorial
	public Sprite connectingSprt;
	public Sprite tutorialFish1Sprt;
	public Sprite tutorialFish2Sprt;
	public Sprite tutorialFish3Sprt;
	public Sprite tutorialFish4Sprt;
	public Sprite tutorialFish5Sprt;
	public Sprite tutorialFish6Sprt;
	public Sprite tutorialFish7Sprt;
	public Sprite tutorialFish8Sprt;
	public Sprite gameFinishSprt;
	public Sprite noSesionSprt;

	//Game
	private int peces;
	private int pecesAtrapados;
	private int winner;
	private float auxPromExito;

	private List<MoveDataStruct> movesData;
	private List<float> dataXpos;
	private List<float> dataYpos;
	private List<float> dataZpos;
	private float dataCounter;


	public List<int> segundosPezRojo=null;
	private Transform pezSwimming;
	private int contador;
	private int random;

	public Image gearVrImageField;
	private bool isConeccting;
	private bool isDosificated;
	public FishingGameState fishingGameState;
	//Keys to connect
	public bool keyExercise;
	public bool keyGps;
	//Data from phone
	private float latitud=0;
	private float longitud=0;


	void Awake()
	{
		instance = this;
	}
	// Use this for initialization
	void Start () {
		keyExercise = false;
		keyGps = true;

		//Initialize the list variables
		movesData = new List<MoveDataStruct>();
		dataXpos = new List<float>();
		dataYpos = new List<float>();
		dataZpos = new List<float>();
		dataCounter = 0.5f;


		fishingGameState = FishingGameState.LOGIN;

	}

	public void UpdateTimerUI(){
		//set timer UI
		secondsCount += Time.deltaTime;
		secondsCount2 += Time.deltaTime;
	//	if (secondsCount2 < int.Parse (DosificationController.instance.exercise2Data.duracion)+1) {
		if (secondsCount2 < E_tiempoEjercicio+1) {
			TimerTransform.GetComponent<TextMesh> ().text = hourCount + "h:" + minuteCount + "m:" + (int)secondsCount + "s";

		} else {
			fishingGameState = FishingGameState.GAMEOVER;
		}
		//TimerTransform.GetComponent<TextMesh> ().text = "" + isSwimming;
		if(secondsCount >= 60){
			minuteCount++;
			secondsCount = 0;
		}else if(minuteCount >= 60){
			hourCount++;
			minuteCount = 0;
		}    
	}

	// Update is called once per frame
	void Update () {

		switch (fishingGameState)
		{
		case FishingGameState.LOGIN:
			if (!isConeccting) {
				PostData ("paciente@paciente.com", "paciente");
				gearVrImageField.sprite = connectingSprt;
				isConeccting = true;

			}
			if(keyExercise && keyGps)
			{
				if (!isDosificated) {
					PostOnline ("3", "1", longitud, latitud);
					isDosificated = true;
				}
			}               
			//gearVrMessage.SetActive(true);
			//gearVrTxtField2.text = "Conectando...";

			break;
		case FishingGameState.COUNTER:
			Debug.Log ("counter");
			E_tiempoEjercicio = int.Parse (DosificationController.instance.exercise2Data.duracion);
			E_Dificultad = int.Parse (DosificationController.instance.exercise2Data.altura);
			E_Velocidad = float.Parse (DosificationController.instance.exercise2Data.velocidad);
			E_TiempoIdle = float.Parse (DosificationController.instance.exercise2Data.espera);
			E_isRandom = DosificationController.instance.exercise2Data.tipo_guia;
			E_Orden=DosificationController.instance.exercise2Data.guiado;
			E_probabilidadPezRojo=int.Parse(DosificationController.instance.exercise2Data.pez_rojo);
			E_DistanciaPeces=float.Parse(DosificationController.instance.exercise2Data.distancia);


			for(int i=0;i<(E_tiempoEjercicio*E_probabilidadPezRojo/100);i++){
				int random2 = Random.Range (0, E_tiempoEjercicio);
				if (segundosPezRojo.Contains (random2)) {
					i--;
				} else {
					segundosPezRojo.Add (random2);
				}
			}

			//	Points.position = new Vector3 (Points.position.x,Points.position.y,E_DistanciaPeces);
			randomSpawn ();
			//	Debug.Log (random+1);
			LlamarPez (SpawnPoints [random]);
			isSwimming = true;
			fishingGameState = FishingGameState.PLAYING;
			break;	
		case FishingGameState.PLAYING:
			UpdateTimerUI ();
			if (isSwimming == false && random != -1) {
				//	Debug.Log (random+1);
				if (segundosPezRojo.Contains ((int)secondsCount2)) {
					LlamarPez2 (SpawnPoints [random]);
				}
				LlamarPez (SpawnPoints [random]);
				isSwimming = true;
			}


			if (pezSwimming.GetComponent<ControladorPez>().isCaptured) {
			//CAPTURING DE BALL DATA
				dataCounter += Time.deltaTime;

				if (dataCounter >= 0.5f)
				{
					SaveMoveData(Red.transform.position.x, Red.transform.position.y, Red.transform.position.z);
					dataCounter = 0f;
				}
			}
			if (isSaved) {
				pecesAtrapados++;
				SaveMoveToJson(dataXpos,dataYpos,dataZpos);
		//		ControladorFishingGame.isSwimming = false;
				isSaved = false;
			}

			break;
		case FishingGameState.GAMEOVER:
			//Cada pez en velocidad 1 toma en promedio 5 segundos en llegar a su destino + el tiempo de idle en el punto,la velocidad y tiempo
			//es IP por lo que uso esta formula para hallar la cantidad de peces que deberian salir
			float auxPecesPorSalir = E_tiempoEjercicio / ((5 + E_TiempoIdle) / E_Velocidad);
			Debug.Log (auxPecesPorSalir);
			//promedio la cantidad de peces que deberian haber salido con la cantidad de peces que salieron ( 60% y 40%)
			float promPecesPorSalir = auxPecesPorSalir * 0.6f + peces * 0.4f;
			Debug.Log (peces);
			Debug.Log (promPecesPorSalir);
			auxPromExito = (pecesAtrapados * 100) / promPecesPorSalir;
			auxPromExito = Mathf.Round (auxPromExito * 100f) / 100f;

			Debug.Log (auxPromExito);

			if (auxPromExito > 60) {
				winner = 1;
			} else {
				winner = 0;
			}

			PostMovesData ();
			DosificationController.instance.PostCompletaEjercicio2 ();
			fishingGameState = FishingGameState.ENDGAME;

			break;
		case FishingGameState.ENDGAME:
			
			break;
		default:
			break;
		}     
	}

	public void randomSpawn(){
		
		if (E_isRandom == true) {
			random = Random.Range (0, (E_Dificultad * 4) - 1);
		} else {
		//	Debug.Log (contador);
			if (contador >= E_Orden.Length) {
				contador = 0;
			}
			if (E_Orden.Substring (contador + 1, 1) != ",") {
				random = int.Parse (E_Orden.Substring (contador, 2)) - 1;
				contador=contador+3;
			} else {
				random = int.Parse (E_Orden.Substring (contador, 1)) - 1;
				contador=contador+2;
			}
		}
	}

	public void LlamarPez(Transform destino){
		
		peces++;

		Transform Pez = Instantiate (prefabPez) as Transform;
		pezSwimming = Pez;
		Pez.position = StartPoint.position;
		Pez.name = "Pez";
		Pez.GetComponent<ControladorPez>().StartPoint = StartPoint;
		Pez.GetComponent<ControladorPez>().FinishPoint = FinishPoint;
		Pez.GetComponent<ControladorPez>().DestinoPoint = destino;
		Pez.GetComponent<ControladorPez>().CestoPoint = CestoPoint;
		SpawnPoints[16].position = new Vector3 (destino.position.x, SpawnPoints [16].position.y, destino.position.z);

		Pez.GetComponent<ControladorPez>().velocidad = E_Velocidad;
		Pez.GetComponent<ControladorPez>().tiempo = E_TiempoIdle;
		if (random == 0 || random == 4 || random == 8 || random == 12) {
			Pez.GetChild (0).GetComponent<Renderer> ().material.color = colores [0];
			Pez.GetChild (1).GetComponent<Renderer> ().material.color = colores [0];
		} else if (random == 1 || random == 5 || random == 9 || random == 13) {
			Pez.GetChild (0).GetComponent<Renderer> ().material.color = colores [1];
			Pez.GetChild (1).GetComponent<Renderer> ().material.color = colores [1];
		}else if (random==2 || random==6 || random==10 || random==14) {
			Pez.GetChild (0).GetComponent<Renderer> ().material.color = colores [2];
			Pez.GetChild (1).GetComponent<Renderer> ().material.color = colores [2];
		}else{
			Pez.GetChild (0).GetComponent<Renderer> ().material.color = colores [3];
			Pez.GetChild (1).GetComponent<Renderer> ().material.color = colores [3];
		}
		randomSpawn ();
	}
	public void LlamarPez2(Transform destino){
		Transform Pez = Instantiate (prefabPez) as Transform;
		Pez.position = FinishPoint.position;
		Pez.name = "PezRojo";
		Pez.GetComponent<ControladorPez>().StartPoint = FinishPoint;
		Pez.GetComponent<ControladorPez>().FinishPoint = StartPoint;
		Pez.GetComponent<ControladorPez>().DestinoPoint = destino;
		Pez.GetComponent<ControladorPez>().velocidad = E_Velocidad;
		Pez.GetComponent<ControladorPez>().tiempo = E_TiempoIdle;
		Pez.GetComponent<ControladorPez>().pezrojo = true;
		Pez.GetChild (0).GetComponent<Renderer> ().material.color = colores [4];
		Pez.GetChild (1).GetComponent<Renderer> ().material.color = colores [4];
	}




	public void SetKeyGps(bool status)
	{
		keyGps = status;
	}

	public void SaveMoveData(float x, float y, float z)
	{
		dataXpos.Add(x);
		dataYpos.Add(y);
		dataZpos.Add(z);


	}

	public void SaveMoveToJson(List<float> x, List<float> y, List<float> z)
	{
		MoveDataStruct auxStruc = new MoveDataStruct();
		auxStruc.X = new List<float>();
		auxStruc.Y = new List<float>();
		auxStruc.Z = new List<float>();

		for (int i = 0; i < x.Count; i++)
		{
			auxStruc.X.Add(x[i]);
			auxStruc.Y.Add(y[i]);
			auxStruc.Z.Add(z[i]);

			Debug.Log ("x:"+x[i]+ "Y:"+y[i] + "Z:"+z[i]);
		}

		movesData.Add(auxStruc);

		dataXpos.Clear();
		dataYpos.Clear();
		dataZpos.Clear();
	}


	void PostMovesData()
	{
		string movimientos = "[{";
		for(int i = 0; i < movesData.Count; i ++)
		{

			movimientos += "\"movimiento\":[{";
			for (int j = 0; j < movesData[i].X.Count; j++)
			{
				movimientos += "\"x\":" + "\"" + movesData[i].X[j] + "\"" + ",";
				movimientos += "\"y\":" + "\"" + movesData[i].Y[j] + "\"" + ",";
				movimientos += "\"z\":" + "\"" + movesData [i].Z [j] + "\"";
				movimientos += "}";
				if (j != movesData[i].X.Count - 1)
					movimientos += ",{";
				else
					movimientos += "]}";
			}
			if (i != movesData.Count - 1)
				movimientos += ",{";
		}
		movimientos += "]";

		Debug.Log (movimientos);
		//string testTiros = "[{\"turno\": [{\"x\": \"-47.016\",\"y\": \"1.00015\",\"z\": \"-144.79\",\"nivel\": \"1\",\"acierto\": \"1\"}, {\"x\": \"-46.314\",\"y\": \"0.76515\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.296\",\"y\": \"0.96715\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.889\",\"y\": \"0.71315\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-47.068\",\"y\": \"1.60115\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}]}]";
		//gearVrTxtField2.text = auxProm.ToString();
		//DosificationController.instance.SaveEvolutionBallGame("33.2", "1", "12", "4", "4", "3",testTiros);
		DosificationController.instance.SaveEvolutionFishGame(auxPromExito.ToString(), winner.ToString(), peces.ToString(),"5", DosificationController.instance.exercise2Data.id_ejercicio, movimientos);
		//WWWForm phoneData = new WWWForm();
		//phoneData.AddField("tiros", tiros);
		//WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
		////StartCoroutine(PostGetEnumerator(www));
		//StartCoroutine("PostTurnDataEnumerator", www);
	}


	void PostGetExersice(string id, string ideE)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("id_ejercicio", ideE);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
		//StartCoroutine(PostGetEnumerator(www));
		StartCoroutine("PostGetEnumerator", www);
	}
	void PostOnline(string id, string status, float lat, float lon)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("status", status);
		phoneData.AddField("latitud", lat.ToString());
		phoneData.AddField("longitud", lon.ToString());

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
		//StartCoroutine(PostOnlineEnumerator(www));
		StartCoroutine("PostOnlineEnumerator", www);
	}
	void PostData(string dni, string pass)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("email", dni);
		phoneData.AddField("pass", pass);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostdataEnumerator", www);

	}
	IEnumerator PostOnlineEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			PostGetExersice("3", "5");
			//print(www.text);
			StopCoroutine("PostOnlineEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostGetEnumerator(WWW www)
	{   
		yield return www;
		if (www.error != "Null")
		{
			Debug.Log("Dosificacion");          
			print(www.text);

			DosificationController.instance.SaveDataExcersise2(www);
			//DosificationController.instance.SaveEvolutionFishGame("33.2","1","12","4","4","3");
			//PostTurnsData();
			Debug.Log(www.text);
			if (DosificationController.instance.exercise2Data.status)
			{
				Debug.Log("aqui");
	//			connectToPhotonScript.enabled = true;
				if(fishingGameState == FishingGameState.LOGIN)
				fishingGameState = FishingGameState.COUNTER;
				//keyExercise = true;
	//			audioSource.clip = audioClipWelcome;
	//			audioSource.Play();
			}
			else
			{
			//	ballGameState = FishGameState.NOSESION;
			}
			StopCoroutine("PostGetEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostdataEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			//print(www.text);
			DosificationController.instance.SaveDataPacient(www);
			//PostOnline("3", "1","1231","-1231");
			keyExercise = true;
			StopCoroutine("PostdataEnumerator");
		}
		else
		{
			StopAllCoroutines();
			//ballGameState = BallGameState.WELCOME;
			Debug.Log(www.error);
		}
	}
}
