﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPez : MonoBehaviour {
	public Transform StartPoint;
	public Transform DestinoPoint;
	public Transform FinishPoint;
	public Transform CestoPoint;
	public float velocidad;
	public float tiempo;
	public bool pezrojo;

	private string nameDestino;
	public bool isPoint=false;
	public bool isHidden=false;
	public bool isCaptured=false;
	// Use this for initialization
	void Start () {
		nameDestino = DestinoPoint.name;
		//Debug.Log (nameDestino);
	}

	void OnTriggerEnter(Collider colisionador){
		if (pezrojo) { //POSICIONES INVERTIDAS
			if (colisionador.transform.name == "StartPoint") {
				Destroy (this.gameObject);
			} else if (colisionador.transform.name == nameDestino) {
				isPoint = true;
				StartCoroutine (IdlePez ());
			}
		} else { // POSICIONES NORMALES
			if (colisionador.transform.name == "FinishPoint") {
				if (isCaptured == false) {
					Destroy (this.gameObject);
					ControladorFishingGame.isSwimming = false;
					isCaptured = false;
				} else {
					
				}
			}

			else if (colisionador.transform.name == "Cesto") {
				ControladorFishingGame.isSwimming = false;
				ControladorFishingGame.isSaved = true;
				Destroy (this.gameObject);
			} else if (colisionador.transform.name == nameDestino) {
				isPoint = true;
				StartCoroutine (IdlePez ());
			}
		}
	}

	void OnTriggerStay(Collider colisionador){
		if (pezrojo) {
		} else {
			if (colisionador.transform.name == "Cube" && isPoint == true && isHidden == false && isCaptured == false) {
				this.transform.GetChild (5).gameObject.SetActive (true);
				isCaptured = true;
				//DestinoPoint = CestoPoint;
				DestinoPoint = colisionador.transform.GetChild (0);
			}
			if (colisionador.transform.name == "PezRojo") {
				isHidden = true;
			}
		}
	}

	void OnTriggerExit(Collider colisionador){
		if (colisionador.transform.name == nameDestino) {
			isPoint = false;
		}
		if (colisionador.transform.name == "PezRojo") {
			isHidden = false;
		}
	}

	IEnumerator IdlePez(){
		yield return new WaitForSeconds (tiempo);
		if(isCaptured==false)
		DestinoPoint = FinishPoint;
	}

	// Update is called once per frame
	void Update () {
		this.transform.position = Vector3.MoveTowards (this.transform.position,DestinoPoint.position,velocidad*Time.deltaTime);
		this.transform.LookAt (DestinoPoint.position);
	}
}
