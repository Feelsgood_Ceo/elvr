﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MalbersAnimations;
using MalbersAnimations.Utilities;
using UnityEngine.UI;

public class DogController : MonoBehaviour {

    public enum DogStates
    {
        PATROL,
        IDLE,
        CALLING,
        CATCH,
        GIVE
    }

    #region public variables

    //AI for patrol
    [Header("AI patrol")]
    public GameObject mouthTransform;
	public AudioSource audioEntregaVara;
	public AudioSource audioCaricias;
	public AudioSource audioLlamado;
	public AudioSource audioAgarrandoVara;
    public Text DogStateText;
    #endregion

    #region private variables
    private DogStates dogStates;
    private float dogPatrolTimer;
    private int callingTimes = 0;
    //AI patrol
    private bool playerCalling;
    private bool playerStickLaunched = false;
    private float attentionTime;
    private bool upAnim = false;
	private bool pancitaAnim = false, hibarkAnim = false;
	private int movesTimes =0;
	private bool isExecuteCouroutine=false;
    #endregion
    // Use this for initialization
    void Start () {
        playerCalling = false;
	}
	
	// Update is called once per frame
	public void UpdateDog () {
        DogStateText.text = dogStates.ToString();
        //       print("DogState: " + dogStates);
        Vector3 controllerMovent = OVRInput.GetLocalControllerAcceleration(OVRInput.Controller.RTrackedRemote);
		print (dogStates + " " + Vector3.Distance (transform.position, IntroductionMainController.instance.hand.transform.position));
        switch (dogStates)
        {
            case DogStates.PATROL:
                GetComponent<LookAt>().active = false;
                dogPatrolTimer += Time.deltaTime;
                if (dogPatrolTimer >= Random.Range(20, 50))
                {
                    ChangeWaypoints();
                    dogPatrolTimer = 0f;
                }
                if (IntroductionMainController.instance.stickLaunched)
                {
                    dogStates = DogStates.CATCH;
                    IntroductionMainController.instance.stickLaunched = false;
                }
				
				if (controllerMovent.x < -4 && controllerMovent.z < -4 && !IntroductionMainController.instance.stickLaunched)
				{
					dogStates = DogStates.CALLING;
					callingTimes++;
                    hibarkAnim = false;
				}
	
                break;
            case DogStates.IDLE:
                break;
		case DogStates.CALLING:
			//print (Vector3.Distance (transform.position, IntroductionMainController.instance.playerPosition.transform.position) + "HAND" + Vector3.Distance (transform.position, IntroductionMainController.instance.hand.transform.position));
			if (!playerCalling) {
				GetComponent<AnimalAIControl> ().target = IntroductionMainController.instance.playerPosition;
				//     GetComponent<Animator>().SetBool("saltar", true);
				playerCalling = true;
				attentionTime = 0;
				upAnim = false;
				pancitaAnim = false;
				movesTimes = 0;
				GetComponent<LookAt> ().active = true;
			}
                
			if (playerCalling) {
				attentionTime += Time.deltaTime;

				//	GetComponent<Animator>().SetTrigger("saltar", false);

				/*if (attentionTime >= 30f) {
					dogStates = DogStates.PATROL;
					//        GetComponent<Animator>().SetBool("saltar", false);
					attentionTime = 0f;
					playerCalling = false;
					callingTimes = 0;
                        
					IntroductionMainController.instance.sleepZone.SetActive (false);
				}*/
			}

				if (Vector3.Distance(transform.position, IntroductionMainController.instance.playerPosition.transform.position) <= 2f){
                    


                if (Vector3.Distance (transform.position, IntroductionMainController.instance.hand.transform.position)<=.7f&&!pancitaAnim ) {

					/* @Cesar
					 * FIX: THE REACTION OF THE DOG NOW IS TRIGGERED BY THE DISTANCE BETWEEN THE DOG AND THE HAND
					*/
					    if (Input.GetKeyDown(KeyCode.W) || controllerMovent.magnitude > .5f) {
						    if (isExecuteCouroutine == false) {
							    StartCoroutine (TiempoCaricias (1.5f));
							    isExecuteCouroutine = true;
						    }
						    movesTimes++;
					    }

                        if(movesTimes >= 1 && movesTimes < 3 && !hibarkAnim && !pancitaAnim)
                        {
                            if (!audioLlamado.isPlaying)
                                audioLlamado.Play();

                            GetComponent<Animator>().SetTrigger("hiBark");
                            hibarkAnim = true;

                            StartCoroutine(hiBark());
                        }

					    if (movesTimes >= 3) {
						    if(!audioCaricias.isPlaying)
							    audioCaricias.Play ();

						    GetComponent<Animator> ().SetTrigger ("pancita");
						    pancitaAnim = true;
						    StartCoroutine (help ());
					    }
					}
				}

                if(IntroductionMainController.instance.stickLaunched)
                {
                    dogStates = DogStates.CATCH;
                    IntroductionMainController.instance.stickLaunched = false;
                }
                
                break;
            case DogStates.CATCH:
                if(!playerStickLaunched)
                {
                    GetComponent<AnimalAIControl>().target = IntroductionMainController.instance.stick.transform;
                    playerStickLaunched = true;
				if (!audioLlamado.isPlaying) {
					audioLlamado.Play ();
				}
                }
            //    print(Vector3.Distance(transform.position, IntroductionMainController.instance.stick.transform.position));
                if(Vector3.Distance(transform.position,IntroductionMainController.instance.stick.transform.position)<=2f)
                {
				Debug.Log ("CATCH TO GIVE");
				if (!audioAgarrandoVara.isPlaying) {
					audioAgarrandoVara.Play ();
				}
                    dogStates = DogStates.GIVE;
                    playerStickLaunched = false;
                }

                break;
            case DogStates.GIVE:
                if (!playerStickLaunched)
                {
                    GetComponent<AnimalAIControl>().target = IntroductionMainController.instance.bubbleWaypoint.transform;
                    playerStickLaunched = true;
                }
                IntroductionMainController.instance.stick.transform.position = mouthTransform.transform.position;
                if (Vector3.Distance(transform.position, IntroductionMainController.instance.bubbleWaypoint.transform.position) <= 2f)
                {
				if (!audioEntregaVara.isPlaying) {
					audioEntregaVara.Play ();
				}
					GetComponent<Animator>().SetTrigger("happyTail");
                    IntroductionMainController.instance.notRespawn = false;
                    dogStates = DogStates.CALLING;
                    IntroductionMainController.instance.stick.GetComponent<StickController>().ActiveSpawn();
                    playerStickLaunched = false;
                
                }
                upAnim = false;
                break;
            default:
                break;
        }
    }
	IEnumerator TiempoCaricias(float time){
		yield return new WaitForSeconds (time);
		movesTimes = 0;
		isExecuteCouroutine = false;
	}
	IEnumerable help2(){
		yield return new WaitForSeconds (4f);
	}

	IEnumerator help(){
		yield return new WaitForSeconds (5.2f);
		pancitaAnim = false;
		//dogStates = DogStates.PATROL;
	}

    IEnumerator hiBark()
    {
        yield return new WaitForSeconds(4.9f);
        hibarkAnim = false;
        
    }
    public void ActiveCatch()
    {
        dogStates = DogStates.CATCH;
    }
       
    public void ActivePatrol()
    {
        dogStates = DogStates.PATROL;
    }

    public DogStates GetState()
    {
        return dogStates;
    }
    public void OnCollisionEnter(Collision col)
	{
    }
    public void ChangeWaypoints()
    {
        GetComponent<AnimalAIControl>().target = IntroductionMainController.instance.waypoints[Random.Range(0, IntroductionMainController.instance.waypoints.Count)].transform;
    }
    public void SetWaypoint(Transform transform)
    {
        GetComponent<AnimalAIControl>().target = transform;
    }


}
