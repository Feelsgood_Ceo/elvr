﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubblePicker : MonoBehaviour {

    public enum BubbleStates
    {
        SPAWN,
        IDLE,
        EXPLOTING,
        DEAD
    }

    #region Public Variables

    public BubbleStates bubbleStates;
    public Transform spawnPoint;
    public Transform stopPoint;
    public GameObject objectContainer;
    public ParticleSystem exploteParticles;
    #endregion

    #region Private Variables
    private float runningTime = 0;
    private float spawnSpeed = 1.5f;

    #endregion
    // Use this for initialization
    void Start () {
        bubbleStates = BubbleStates.SPAWN;
        this.transform.position = spawnPoint.position;
	}
	
	// Update is called once per frame
	public void UpdateBubble () {

		//print("bubbleStates: " + bubbleStates);
        switch (bubbleStates)
        {
		case BubbleStates.SPAWN:
			transform.position += new Vector3 (0f, spawnSpeed * Time.deltaTime, 0f);
			objectContainer.transform.position = transform.position;
			objectContainer.GetComponent<Rigidbody> ().useGravity = false;
			objectContainer.GetComponent<Rigidbody> ().isKinematic = true;
			IntroductionMainController.instance.stick.GetComponent<Collider> ().enabled = false;
			this.GetComponent<Collider> ().enabled = false;
                if(Vector3.Distance(transform.position,stopPoint.position)<=0.1f)
                {
                    bubbleStates = BubbleStates.IDLE;
                }
                break;
            case BubbleStates.IDLE:
                Vector3 newPos = transform.position;
                float deltaHeight = (Mathf.Sin(runningTime + Time.deltaTime) - Mathf.Sin(runningTime));
                newPos.y += deltaHeight * 0.05f;
                runningTime += Time.deltaTime;
                transform.position = newPos;
                objectContainer.transform.position = transform.position;

			IntroductionMainController.instance.stick.GetComponent<Collider> ().enabled = true;
			this.GetComponent<Collider> ().enabled = true;

                break;
            case BubbleStates.EXPLOTING:
                GetComponent<MeshRenderer>().enabled = false;
                GetComponent<SphereCollider>().enabled = false;
                exploteParticles.Play();
                //objectContainer.GetComponent<Rigidbody>().isKinematic = false;
                //if(!IntroductionMainController.instance.stickTaken)
                //objectContainer.GetComponent<Rigidbody>().useGravity = true;
                bubbleStates = BubbleStates.DEAD;    

			IntroductionMainController.instance.stick.GetComponent<Collider> ().enabled = true;
			this.GetComponent<Collider> ().enabled = true;
                break;
            case BubbleStates.DEAD:
                transform.position = spawnPoint.position;
                GetComponent<MeshRenderer>().enabled = true;
                GetComponent<SphereCollider>().enabled = true;
			IntroductionMainController.instance.stick.GetComponent<Collider> ().enabled = true;
			this.GetComponent<Collider> ().enabled = true;
                break;
            default:
                break;
        }
    }
}
