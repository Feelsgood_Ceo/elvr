﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MalbersAnimations;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class IntroductionMainController : MonoBehaviour {

    enum IntroductionStates
    {
        WATERMELONSPEAK,
        EXITPARTICLES,
        WATERMELONEXIT,
        GAME
    }

    public enum StickStates
    {
        IDLE,
        HANDED,
        THROWING
    }

    #region public variables
    public static IntroductionMainController instance;
    [Header("Game elements")]
    public GameObject dog;
    [Header("Watermelon elements")]
    public GameObject watermelon;
    public ParticleSystem watermelonSpawnParticles;
	public float turnSpeed;
	public Transform HELP;
	public Transform posStick;
    public Transform playerPosition;
    public GameObject bubbleWaypoint;
    public Transform arm;
    public Transform hand;
    public GameObject handFbx;
    public Transform stick;
    public List<GameObject> waypoints;
    public GameObject bubble;
    public GameObject sleepZone;
    [Header("GUI")]
    public Text timerTxt;
    [HideInInspector]
    public bool stickTaken = false;
    [HideInInspector]
    public bool notRespawn = false;
    public bool stickLaunched = false;

    public PlayableDirector TL;
    #endregion

    void Awake()
    {
        instance = this;
    }
    #region private variables
    private IntroductionStates introduccionStates;
    public StickStates stickState;
    private float dogPatrolTimer = 0f;
    //For hand
    private bool isTriggerPress;
    //For throwing the stick
    private Vector3 gravity = new Vector3(0f, -0.005f, 0f);
    private Vector3 direction;
    private Vector3 velocity;
	private bool canRotate = false;
    //For dog
    private bool canFollow = false;
    private bool canRetrive = false;
    private bool canFinish = true;
    //For ui
    private float timerText = 60f;
    //For change introduction state when watermelon appears
    private float timerWatermelonSpawn = 0f;
    //Key to play animation particles just one time
    private bool canSpawnParticles = true;
    #endregion

    // Use this for initialization
    void Start () {
        introduccionStates = IntroductionStates.WATERMELONSPEAK ;
        stickState = StickStates.IDLE;
	}
	
	// Update is called once per frame
	void Update () {
		
		///////////////////////////////////////////////////////////////////////////////
		/// 
        if(Input.GetKeyDown(KeyCode.Space))
        {
            stickState = StickStates.HANDED;
            bubble.GetComponent<BubblePicker>().bubbleStates = BubblePicker.BubbleStates.EXPLOTING;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            stickState = StickStates.THROWING;
        }

		if(Input.GetKey(KeyCode.DownArrow)){
			HELP.Rotate(Vector3.left, -turnSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.UpArrow)){
			HELP.Rotate(Vector3.left, turnSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.LeftArrow))
			HELP.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.RightArrow))
			HELP.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
		/////////////////////////////////////////////////////////////////////////////////////
        /*if (TestingTools.instance.canSwitchScene)
        {
            FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
            FaderToScene.instance.active = true;
        }*/
        //print(introduccionStates);
        switch (introduccionStates)
        {
            case IntroductionStates.WATERMELONSPEAK:
                watermelon.SetActive(true);
                if(canSpawnParticles)
                {
                    watermelonSpawnParticles.Play();
                    canSpawnParticles = false;
                }
                
                timerWatermelonSpawn += Time.deltaTime;
                if (timerWatermelonSpawn >= 12f)
                {
                    timerWatermelonSpawn = 0f;
                    introduccionStates = IntroductionStates.EXITPARTICLES;
                    canSpawnParticles = true;
                }
                if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
                {
                    handFbx.GetComponent<Animator>().SetBool("isTaking", true);
                    isTriggerPress = true;
                }
                break;
            case IntroductionStates.EXITPARTICLES:
                if (canSpawnParticles)
                {
                    watermelonSpawnParticles.Play();
                    canSpawnParticles = false;
                }
                timerWatermelonSpawn += Time.deltaTime;
                if(timerWatermelonSpawn>=1.5f)
                {
                    timerWatermelonSpawn = 0f;
                    introduccionStates = IntroductionStates.WATERMELONEXIT;
                }
                if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
                {
                    handFbx.GetComponent<Animator>().SetBool("isTaking", true);
                    isTriggerPress = true;
                }
                break;
            case IntroductionStates.WATERMELONEXIT:
                if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
                {
                    handFbx.GetComponent<Animator>().SetBool("isTaking", true);
                    isTriggerPress = true;
                }

                watermelon.SetActive(false);

                if (TL.time >= TL.duration - .25f)
                    introduccionStates = IntroductionStates.GAME;
               
                break;
            case IntroductionStates.GAME:
                
                dog.GetComponent<DogController>().UpdateDog();
                bubble.GetComponent<BubblePicker>().UpdateBubble();
                //For controller animation
                if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                    (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
                {
                    handFbx.GetComponent<Animator>().SetBool("isTaking", true);
                    isTriggerPress = true;
                }
                else
                {
                   handFbx.GetComponent<Animator>().SetBool("isTaking", false);
                    isTriggerPress = false;
                    if(stickTaken)
                    {
						
                        stickState = StickStates.THROWING;
                        stickTaken = false;
                    }
                }
                if (Vector3.Distance(hand.position,bubble.transform.position)<0.2f && bubble.GetComponent<BubblePicker>().bubbleStates == BubblePicker.BubbleStates.IDLE)
                {              
                    if(isTriggerPress)
                    {
                        bubble.GetComponent<BubblePicker>().bubbleStates = BubblePicker.BubbleStates.EXPLOTING;
                        stickTaken = true;
                    }else
                    {
                        stickTaken = false;
                    }
                } 
			if(isTriggerPress && stickTaken && bubble.GetComponent<BubblePicker>().bubbleStates != BubblePicker.BubbleStates.SPAWN)
                {
                    stickState = StickStates.HANDED;
                }

                ControlStickStates();

                timerText -= Time.deltaTime;
                timerTxt.text = "Time: " + timerText.ToString().Remove(4);
                if (timerText <= 0f)
                {
                    StartCoroutine(ChangeScene());
                }            
                break;
            default:
                break;
        }
    }

    IEnumerator ChangeScene()
    {
        FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
        FaderToScene.instance.active = true;

        yield return new WaitForSeconds(1.5f);

        SceneManager.LoadScene(2);
    }

    void ControlStickStates()
    {
        switch (stickState)
        {
		case StickStates.IDLE:
			stick.GetComponent<MeshRenderer> ().enabled = true;
			stick.GetChild(0).GetComponent<MeshRenderer> ().enabled = false;
                //if(canFollow)
                //{
                //    dog.GetComponent<DogController>().SetWaypoint(stick.transform);
                //    canFollow = false;
                  
                //}
                //if(Vector3.Distance(dog.transform.position,stick.transform.position)<=0.5f && notRespawn && !canRetrive)
                //{
                //    dog.GetComponent<DogController>().SetWaypoint(bubbleWaypoint.transform);
                //    canRetrive = true;
                //}
                //if(canRetrive)
                //{
                //    stick.transform.position = dog.GetComponent<DogController>().mouthTransform.transform.position;
                //}
                //if(Vector3.Distance(dog.transform.position, bubbleWaypoint.transform.position) <= 0.2f && !canFinish)
                //{
                //    canFinish = true;
                //    canRetrive = false;
                //    notRespawn = false;
                //    canFollow = false;
                //    stick.GetComponent<StickController>().ActiveSpawn();
                //    dog.GetComponent<DogController>().ActivePatrol();
                //}
                break;
		case StickStates.HANDED:
			canRotate = false;
			stick.position = posStick.position;
			stick.rotation = posStick.rotation;
			stick.GetComponent<MeshRenderer> ().enabled = false;
			stick.GetChild(0).GetComponent<MeshRenderer> ().enabled = true;
         //       stick.position = hand.position;
         //       stick.localEulerAngles = hand.eulerAngles;
                direction = hand.transform.position - arm.transform.position;
                velocity = direction.normalized * 0.2f;
			//	stickLaunched = false;
                break;
            case StickStates.THROWING:
			if(canRotate==false){
			stick.localEulerAngles = hand.eulerAngles;
			canRotate=true;
			}
			stick.GetComponent<MeshRenderer> ().enabled = true;
			stick.GetChild(0).GetComponent<MeshRenderer> ().enabled = false;
                velocity += gravity;
                stick.transform.position += velocity;
                //stickLaunched = true;
                //if (dog.GetComponent<DogController>().GetState() == DogController.DogStates.CALLING)
                //{
                    //canFollow = true;
                    //canFinish = false;
                    //notRespawn = true;
                    //dog.GetComponent<DogController>().ActiveCatch();
                notRespawn = true;
                //}
                break;
            default:
                break;
        }
    }

    public void ActiveBubble()
    {
        stick.GetComponent<StickController>().ActiveSpawn();
    }
    public void SetStickIdle()
    {
        stickState = StickStates.IDLE;
    }
}






























