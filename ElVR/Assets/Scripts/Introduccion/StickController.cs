﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickController : MonoBehaviour {

    private float timerReadyForSpawn = 0f;
    private bool canSpawn = false;
	private bool help=false;

    void Start()
    {

    }

    void Update()
    {
        if(canSpawn)
        {
            
            timerReadyForSpawn += Time.deltaTime;
            if (timerReadyForSpawn >= 0.5f)
            {
                //IntroductionMainController.instance.SetStickIdle();
                IntroductionMainController.instance.bubble.GetComponent<BubblePicker>().bubbleStates = BubblePicker.BubbleStates.SPAWN;
                canSpawn = false;
                timerReadyForSpawn = 0f;
                IntroductionMainController.instance.stickLaunched = false;
            }
        }
    }

    void OnCollisionEnter(Collision coll)
	{
    }

    void OnTriggerEnter(Collider col)
    {
		if (IntroductionMainController.instance.stickState == IntroductionMainController.StickStates.THROWING) {
			if (col.gameObject.tag == "ground") {
				IntroductionMainController.instance.SetStickIdle ();
				IntroductionMainController.instance.stickLaunched = true;
				if (!IntroductionMainController.instance.notRespawn) {
					canSpawn = true;
				}
			}
		}
    }

	void OnTriggerStay(Collider col)
	{
		if (IntroductionMainController.instance.stickState == IntroductionMainController.StickStates.THROWING) {
			if (col.gameObject.tag == "ground" && help == false) {
				help = true;
				IntroductionMainController.instance.SetStickIdle ();
				IntroductionMainController.instance.stickLaunched = true;
				if (!IntroductionMainController.instance.notRespawn) {
					canSpawn = true;
				}
			}
		}
	}


    
    public void ActiveSpawn()
    {
        canSpawn = true;
    }
}
