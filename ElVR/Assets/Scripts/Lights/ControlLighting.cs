﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlLighting : MonoBehaviour {

	// Use this for initialization
	private void Awake () {
        SceneManager.sceneLoaded += ResetExposure;
    }

    /// <summary>
    /// Reset skybox exposure
    /// </summary>
    private void ResetExposure(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex > 0)
            RenderSettings.skybox.SetFloat("_Exposure", 1.0f);
    }
}
