﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProveController : MonoBehaviour {

    public enum ProveStates
    {
        SPAWN,
        UPFACE,
        IDLETOMOVE,
        IDLE,
        MOVE,
    }

    #region Private Variables
    private ProveStates proveStates;

    private float timeToStartMove = 0f;
    private float runningTime = 0f;
    //for lerp
    private float dt = 0f;
    private int index = 1;
    [SerializeField]
    //for animations
    private Animator animator;
    //for peach
    private bool canTalk = false;
    //sound
    private AudioSource [] audioSource;
    //for sound
    private float timerSpeach = 0f;
    //for rotation of the watermelon when pass to idle state
    private float dtIdle = 0f;
    #endregion

    #region Public Variables
    [Header("Particles")]
    public ParticleSystem antiGravityParticles;
    public List<GameObject> movingPoints1;
    public Transform rotatorPoint;
    public Transform pointStop;
    public float maxSpeed;
    public float maxForce;
    public Vector3 velocity;
    public Transform playerPosition;
    //sounds
    [Header("sounds")]
    public AudioClip wakeupAC;
    public AudioClip flightAC;
    public AudioClip speakAC;

    public AudioSource AP;
    #endregion
    // Use this for initialization
    void Start () {
        
        proveStates = ProveStates.SPAWN;
        //animator = GetComponent<Animator>();
        audioSource = GetComponents<AudioSource>();
		canTalk = false;
    }
	
	// Update is called once per frame
	public void UpdateProve () {
        //print(proveStates);
        switch (proveStates)
        {
            case ProveStates.SPAWN:
                //animator.Play("WmSpawn");
                if(!canTalk)
                {
                    AP.clip = wakeupAC;
                    AP.Play();
                    canTalk = true;
				proveStates = ProveStates.UPFACE;
                }
                
                break;
            case ProveStates.UPFACE:
                antiGravityParticles.Play();
                //animator.Play("WmUp");
                if (!canTalk)
                {
                    AP.volume = 0.3f;
                    AP.pitch = 0.3f;
                    AP.clip = flightAC;
                    AP.Play();
                    canTalk = true;
                }
                break;
            case ProveStates.IDLETOMOVE:
                //animator.Play("Idle");
                Vector3 newPos = transform.position;
                float deltaHeight = (Mathf.Sin(runningTime + Time.deltaTime) - Mathf.Sin(runningTime));
                newPos.y += deltaHeight * 0.05f;
                runningTime += Time.deltaTime;
                transform.position = newPos;

                timeToStartMove += Time.deltaTime;

                if (timeToStartMove >= 3f)
                {
                    proveStates = ProveStates.MOVE;
                    //transform.LookAt(Quaternion.AngleAxis(-125, Vector3.up) * -playerPosition.position);
                }
                break;
            case ProveStates.IDLE:
                //animator.Play("Sandia_anim_welcome");
                //transform.eulerAngles += new Vector3(0f, 90f, 0f);
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, 69.41f, transform.eulerAngles.z),dtIdle);
                dtIdle += Time.deltaTime * 0.3f;
                Vector3 newPos3 = transform.position;
                float deltaHeight3 = (Mathf.Sin(runningTime + Time.deltaTime) - Mathf.Sin(runningTime));
                newPos3.y += deltaHeight3 * 0.05f;
                runningTime += Time.deltaTime;
                transform.position = newPos3;
                timerSpeach += Time.deltaTime;
                if(timerSpeach>=26)
                {
                    timerSpeach = 0f;
                    FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
                    FaderToScene.instance.active = true;
                }
                
                break;
            case ProveStates.MOVE:
                Vector3 newPos2 = transform.position;
                /*float deltaHeight2 = (Mathf.Sin(runningTime + Time.deltaTime) - Mathf.Sin(runningTime));
                newPos2.y += deltaHeight2 * 0.2f;
                runningTime += Time.deltaTime;

                transform.position = newPos2;

                transform.position += Seek(rotatorPoint.position) * Time.deltaTime;

                
                //transform.forward = -rotatorPoint.position;

                if(Vector3.Distance(transform.position,rotatorPoint.position)<=0.5f)
                {
                    TutorialController.instance.activeRotator = true;
                }

                if(Vector3.Distance(transform.position,pointStop.position)<=0.2f)
                {
                    proveStates = ProveStates.IDLE;
                    transform.localEulerAngles = new Vector3(-9.7f, -111.2f, transform.localEulerAngles.z);
                    audioSource[0].Play();
                }*/
                break;
            default:
                break;
        }
    }

    public Vector3 Seek(Vector3 target)
    {
        Vector3 desired = (target - this.transform.position).normalized * maxSpeed;
        Vector3 steer = Vector3.ClampMagnitude((desired - velocity),maxForce);
        return steer;
    }

    public void ChangeState(int state)
    {
        switch (state)
        {
            case 0:
                proveStates = ProveStates.SPAWN;
                break;
            case 1:
                proveStates = ProveStates.UPFACE;
                break;
            case 2:
                proveStates = ProveStates.IDLETOMOVE;
                break;
            default:
                break;
        }
        canTalk = false;
    }
}
