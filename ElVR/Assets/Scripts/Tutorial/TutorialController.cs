﻿using FI.TAGS;
using FI.TOOLS;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace FI
{

    public class TutorialController : MonoBehaviour
    {

        enum TutorialStates
        {
            WAITING,
            INTRO
        }

        #region PUBLIC_PROPERTIES

        public static TutorialController instance;
        public GameObject prove;
        public Transform rotator;
        public GameObject gazePanel;

        public bool activeRotator;

        public PlayableDirector TL;

        #endregion

        #region PRIVATE_PROPERTIES

        private TutorialStates tutorialStates;
        private float waitingTime = 0f;
        private bool isTriggerPress = false;
        private bool loading;

        #endregion

        void Awake()
        {
            instance = this;
        }
        // Use this for initialization
        void Start()
        {
            tutorialStates = TutorialStates.WAITING;
        }

        // Update is called once per frame
        void Update()
        {
            //For controller animation
            if ((OVRInput.GetActiveController() == OVRInput.Controller.LTrackedRemote || OVRInput.GetActiveController() == OVRInput.Controller.RTrackedRemote) &&
                (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad) && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)))
            {
                isTriggerPress = true;
            }
            else
            {
                isTriggerPress = false;
            }

            if (!loading && TL.time >= 55f)
            {
                loading = true;
                StartCoroutine(ChangeScene());
            }

            switch (tutorialStates)
            {
                case TutorialStates.WAITING:
                    waitingTime += Time.deltaTime;
                    if (waitingTime > 5f)
                    {
                        tutorialStates = TutorialStates.INTRO;
                    }
                    break;
                case TutorialStates.INTRO:
                    prove.GetComponent<ProveController>().UpdateProve();
                    if (activeRotator)
                    {
                        RotateRotator();
                    }
                    break;

                default:
                    break;
            }
        }

        // -----------------------------------------------------------------------------
        private IEnumerator ChangeScene()
        {
            yield return null;

            if (ServerData.Instance.GetIsVisualScaleActive())
            {
                yield return Utils.LoadNextScene(ScenesTags.visualScale);
            }
            else
            {
                yield return Utils.LoadNextScene(ServerData.Instance.GetSceneToLoad());
            }
        }

        // -----------------------------------------------------------------------------
        public void RotateRotator()
        {
            if (rotator.eulerAngles.y > -270)
                rotator.localEulerAngles += new Vector3(0f, -30f * Time.deltaTime, 0f);
        }
    }

}