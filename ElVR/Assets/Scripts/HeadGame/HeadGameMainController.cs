﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadGameMainController : MonoBehaviour {

    public enum GameStates
    {
        LOGIN,
        ONLINE,
        GETTINGDATA,
        GAME,
        UPDATA,
        GAMEOVER
    }
    #region Public variables
    public GameObject headControllerAnimatorStates;
    public GameStates gameStates;
    
    #endregion
    #region Private variables

    //flags to controll the conection states;
    private bool flagLogin = false;
    private bool flagOnline = false;
    private bool flagGettingExercise = false;
    #endregion
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (gameStates)
        {
            case GameStates.LOGIN:
                //LOGIN CONFIRMATION NEEDS TO BE IMPLEMENTED IN THE FIRST SCENE AS A DONTDESTROYONLOAD OBJECT
                if(!flagLogin)
                {
                    DosificationController.instance.PostLogin("paciente@paciente.com", "paciente");
                    flagLogin = true;
                }
                if(DosificationController.instance.keyLogin && DosificationController.instance.keyGps)
                {
                    gameStates = GameStates.ONLINE;
                }
                
                break;
            case GameStates.ONLINE:
                if(!flagOnline)
                {
                    DosificationController.instance.PostOnline(DosificationController.instance.dataPacient.id_paciente, "1", DosificationController.instance.latitud, DosificationController.instance.longitud);
                    flagOnline = true;
                }
                if(DosificationController.instance.keyOnline)
                {
                    gameStates = GameStates.GETTINGDATA;
                }
                break;
            case GameStates.GETTINGDATA:
                if(!flagGettingExercise)
                {
                    DosificationController.instance.PostGetExersice(DosificationController.instance.dataPacient.id_paciente, "8");
                }
                if(DosificationController.instance.keyExercise)
                {
                    gameStates = GameStates.GAME;
                }
                break;
            case GameStates.GAME:
                //headControllerAnimatorStates.SetActive(true);
                break;
            case GameStates.UPDATA:
                break;
            case GameStates.GAMEOVER:
                break;
            default:
                break;
        }
    }
}
