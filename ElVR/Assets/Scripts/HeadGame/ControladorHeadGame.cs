﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorHeadGame : MonoBehaviour {
	//Wrapper
	[System.Serializable]
	public class Repeticiones
	{
		public List<Repeticion> Repeticion;
	}

	[System.Serializable]
	public class Repeticion
	{
		public string TipoRepeticion;
		public string Tipo2Repeticion;		
		public string posRepeticion;		
		public string velocidadMovimiento;	
	}

	struct MoveDataStruct
	{
		private List<float> x;
		private List<float> y;
		private List<float> z;

		public List<float> X
		{
			get
			{
				return x;
			}

			set
			{
				x = value;
			}
		}

		public List<float> Y
		{
			get
			{
				return y;
			}

			set
			{
				y = value;
			}
		}

		public List<float> Z
		{
			get
			{
				return z;
			}

			set
			{
				z = value;
			}
		}

	}
	public enum HeadGameState
	{
		LOGIN,
		NOSESION,
		WELCOME,
		WELCOME2,
		TUTORIAL,
		TUTORIAL2,
		TUTORIAL3,
		TUTORIAL4,
		TUTORIAL5,
		TUTORIAL6,
		SPAWN1,
		SPAWN2,
		SPAWN3,
		COUNTER,
		ELVRMOVEMENT,
		PLAYING,
		GAMEOVER,
		ENDGAME
	}

	public static ControladorHeadGame instance;


	public int E_Duracion;
	public int E_TipoEjercicio;
	public int E_tiempoEspera;
	public string E_Repeticiones;

	public Transform[] posiciones;
	public Transform prefabMono;
	public Transform prefabArdilla;
	public Transform prefabSapo;
	public static bool isCreate;
	public HeadGameState headGameState;
	public AudioSource sonidoFeedBack;

	public List<Transform> posicionesObjetosList;
	public Transform spawnIzq;
	public Transform spawnDer;
	public Transform spawnMid;

	public Transform hand;	
	public Text timer;
	public Text objAparecieron;
	public Text objAtrapados;
	public static float vel;
	public static int cantidadObjAparecieron;
	public static int cantidadObjAtrapados;
	private float promExito;
	private int winner;

	public Image gearVrImageField;
	private bool isConeccting;
	private bool isDosificated;
	//Keys to connect
	public bool keyExercise;
	public bool keyGps;
	//Data from phone
	private float latitud=0;
	private float longitud=0;


	//MOVES
	private List<MoveDataStruct> movesData;
	private List<float> dataXpos;
	private List<float> dataYpos;
	private List<float> dataZpos;
	private float dataCounter;

	//TIMER
	private float secondsCount;
	private float secondsCount2;
	private int minuteCount;
	private int hourCount;

	//Textures for tutorial
	public Sprite connectingSprt;
	public Sprite tutorialFish1Sprt;
	public Sprite tutorialFish2Sprt;
	public Sprite tutorialFish3Sprt;
	public Sprite tutorialFish4Sprt;
	public Sprite tutorialFish5Sprt;
	public Sprite tutorialFish6Sprt;
	public Sprite tutorialFish7Sprt;
	public Sprite tutorialFish8Sprt;
	public Sprite gameFinishSprt;
	public Sprite noSesionSprt;

	private float auxPromExito;
	public Repeticiones repeticiones;
	private int numObjeto=0;

	void Awake()
	{
		instance = this;
	}
	// Use this for initialization
	void Start () {
		keyExercise = true;
		keyGps = true;

		//Initialize the list variables
		movesData = new List<MoveDataStruct>();
		dataXpos = new List<float>();
		dataYpos = new List<float>();
		dataZpos = new List<float>();
		dataCounter = 0.5f;


		headGameState = HeadGameState.LOGIN;
	}

	public void UpdateUI(){
		secondsCount += Time.deltaTime;
		secondsCount2 += Time.deltaTime;

		if (secondsCount2 < E_Duracion + 1) {
			timer.text = hourCount + "h:" + minuteCount + "m:" + (int)secondsCount + "s";
		} else {
			FaderToScene.instance.faderStates = FaderToScene.FaderStates.FADINGIN;
			FaderToScene.instance.active = true;
			headGameState = HeadGameState.GAMEOVER;
		}

	//	objAparecieron.text = "" + cantidadObjAparecieron;
	//	objAtrapados.text = "" + cantidadObjAtrapados;

		if(secondsCount >= 60){
			minuteCount++;
			secondsCount = 0;
		}else if(minuteCount >= 60){
			hourCount++;
			minuteCount = 0;
		}    
	}

	void Update () {
		switch (headGameState){
		case HeadGameState.LOGIN:
		/*	if (!isConeccting) {
			//	PostData ("paciente@paciente.com", "paciente");
				gearVrImageField.sprite = connectingSprt;
				isConeccting = true;

			}
			if(keyExercise && keyGps)
			{
				if (!isDosificated) {
					PostOnline ("3", "1", longitud, latitud);
					isDosificated = true;
				}
			}             
*/
			headGameState = HeadGameState.PLAYING;
			break;
		case HeadGameState.COUNTER:
		/*	
			E_Duracion = DosificationController.instance.exerciseOncologicoData.tiempo;
			E_TipoEjercicio = DosificationController.instance.exerciseOncologicoData.tipo_ejercicio;
			E_Repeticiones = DosificationController.instance.exerciseOncologicoData.repeticiones;
*/
			if (E_TipoEjercicio == 0) {
			} else {
				repeticiones = JsonUtility.FromJson<Repeticiones>(E_Repeticiones);
				Debug.Log(repeticiones.Repeticion.Count);
			}
			headGameState = HeadGameState.PLAYING;

			Debug.Log ("counter");
			gearVrImageField.sprite = tutorialFish1Sprt;
			break;	

		case HeadGameState.PLAYING:

			UpdateUI ();
			if (!isCreate) {
				cargarPunto();
			}
			/*
			if (ControladorHandFringe.isclosed) {

				dataCounter += Time.deltaTime;

				if (dataCounter >= 0.3f)
				{
					SaveMoveData(hand.transform.position.x, hand.transform.position.y, hand.transform.position.z);
					dataCounter = 0f;
				}
			}
			*/
			break;


		case HeadGameState.GAMEOVER:

			promExito = (cantidadObjAtrapados * 100) / cantidadObjAparecieron;
			promExito = Mathf.Round (promExito * 100f) / 100f;

			Debug.Log (promExito);

			if (promExito > 60) {
				winner = 1;
			} else {
				winner = 0;
			}
				
		//	PostMovesData ();
			DosificationController.instance.PostCompletaEjercicio3 ();
			headGameState = HeadGameState.ENDGAME;

			break;
		case HeadGameState.ENDGAME:

			break;
		default:
			break;
		}     
	} 

	IEnumerator CargarRepeticiones(){
		WWW www;
		www = new WWW ("http://www.creadoraproducciones.com/API/prueba.php");
		yield return www;

		string json = www.text;
		repeticiones = JsonUtility.FromJson<Repeticiones>(json);
		Debug.Log(repeticiones.Repeticion.Count);
		headGameState = HeadGameState.PLAYING;
	}

	public void cargarPunto(){
		if (E_TipoEjercicio == 0) {

			Repeticion rep =new Repeticion();
			rep.TipoRepeticion = Random.Range(1,3).ToString();
			rep.Tipo2Repeticion = Random.Range(1,3).ToString();
			rep.posRepeticion = Random.Range(1,4).ToString();
			rep.velocidadMovimiento = Random.Range(3,4).ToString();
		

			repeticiones.Repeticion.Add (rep);

		} else {
		
		}
		int cantidadRepeticiones = repeticiones.Repeticion.Count;
		string ejercicio;
		//	Debug.Log(cantidadRepeticiones);
		if (numObjeto < cantidadRepeticiones) {
			Transform objt;
			//TipoRepeticion
			if (repeticiones.Repeticion[numObjeto].TipoRepeticion == "1") {
				ejercicio = "horizontal";
				//Tipo2Repeticion
				if (repeticiones.Repeticion [numObjeto].Tipo2Repeticion == "1") {	// Izquierda_Derecha
					ejercicio = ejercicio + "/izquierda a derecha";
					//posRepeticion
					if (repeticiones.Repeticion [numObjeto].posRepeticion == "1") {	// Inferior
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabSapo) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[0];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[1];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[2];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 1;
						ejercicio = ejercicio + "/inferior";
					} else if (repeticiones.Repeticion [numObjeto].posRepeticion == "2") { // Centro
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabArdilla) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[3];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[4];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[5];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 2;
						ejercicio = ejercicio + "/Centro";
					} else { // Superior
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabArdilla) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[6];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[7];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[8];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 2;
						ejercicio = ejercicio + "/superior";
					}
				} else { // Derecha_Izquierda
					ejercicio = ejercicio + "/derecha a izquierda";
					//posRepeticion
					if (repeticiones.Repeticion [numObjeto].posRepeticion == "1") {	// Inferior
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabSapo) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[2];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[1];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[0];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 1;
						ejercicio = ejercicio + "/inferior";
					} else if (repeticiones.Repeticion [numObjeto].posRepeticion == "2") { // Centro
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabArdilla) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[5];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[4];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[3];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 2;
						ejercicio = ejercicio + "/Centro";
					} else { // Superior
						int ran = Random.Range(0,2);
						if (ran == 1) {
							objt = Instantiate (prefabMono) as Transform;
						} else {
							objt = Instantiate (prefabArdilla) as Transform;
						}

						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[8];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[7];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[6];
						objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 2;
						ejercicio = ejercicio + "/superior";
					}
				}
				//TipoRepeticion
			} else {
				ejercicio = "vertical";
				//Tipo2Repeticion
				int ran = Random.Range(0,2);
				if (ran == 1) {
					objt = Instantiate (prefabMono) as Transform;
				} else {
					objt = Instantiate (prefabArdilla) as Transform;
				}

				if (repeticiones.Repeticion [numObjeto].Tipo2Repeticion == "1") {	// Abajo_Arriba
					ejercicio = ejercicio + "/abajo a arriba";
					//posRepeticion
					if (repeticiones.Repeticion [numObjeto].posRepeticion == "1") {	// Izquierda
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[0];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[3];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[6];
						ejercicio = ejercicio + "/izquierda";
					} else if (repeticiones.Repeticion [numObjeto].posRepeticion == "2") { // Centro
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[1];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[4];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[7];
						ejercicio = ejercicio + "/Centro";
					} else { // Derecha
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[2];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[5];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[8];
						ejercicio = ejercicio + "/Derecha";
					}
					objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 3;
				} else { // Arriba_Abajo
					ejercicio = ejercicio + "/arriba a abajo";
					//posRepeticion
					if (repeticiones.Repeticion [numObjeto].posRepeticion == "1") {	// Izquierda
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[6];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[3];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[0];
						ejercicio = ejercicio + "/Izquierda";
					} else if (repeticiones.Repeticion [numObjeto].posRepeticion == "2") { // Centro
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[7];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[4];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[1];
						ejercicio = ejercicio + "/Centro";
					} else { // Derecha
						objt.GetComponent<ControladorAnimalPrefab> ().Spawn = posicionesObjetosList[8];
						objt.GetComponent<ControladorAnimalPrefab>().destine1 = posicionesObjetosList[5];
						objt.GetComponent<ControladorAnimalPrefab>().destine2 = posicionesObjetosList[2];
						ejercicio = ejercicio + "/Derecha";
					}
					objt.GetComponent<ControladorAnimalPrefab> ().tipo_mov = 4;
				}	
			}

			Debug.Log (ejercicio);
			objt.GetComponent<ControladorAnimalPrefab> ().velocity = float.Parse(repeticiones.Repeticion [numObjeto].velocidadMovimiento);
			objt.GetComponent<ControladorAnimalPrefab> ().timeIdle = E_tiempoEspera;
			objt.name = "POINT";
			cantidadObjAparecieron++;
			isCreate = true;
		}

			numObjeto++;

	}
	public void SetKeyGps(bool status)
	{
		keyGps = status;
	}

	public void SaveMoveData(float x, float y, float z)
	{
		dataXpos.Add(x);
		dataYpos.Add(y);
		dataZpos.Add(z);


	}

	public void SaveMoveToJson(List<float> x, List<float> y, List<float> z)
	{
		MoveDataStruct auxStruc = new MoveDataStruct();
		auxStruc.X = new List<float>();
		auxStruc.Y = new List<float>();
		auxStruc.Z = new List<float>();

		for (int i = 0; i < x.Count; i++)
		{
			auxStruc.X.Add(x[i]);
			auxStruc.Y.Add(y[i]);
			auxStruc.Z.Add(z[i]);

			Debug.Log ("x:"+x[i]+ "Y:"+y[i] + "Z:"+z[i]);
		}

		movesData.Add(auxStruc);

		dataXpos.Clear();
		dataYpos.Clear();
		dataZpos.Clear();
	}


	void PostMovesData()
	{
		string movimientos = "[{";
		for(int i = 0; i < movesData.Count; i ++)
		{

			movimientos += "\"movimiento\":[{";
			for (int j = 0; j < movesData[i].X.Count; j++)
			{
				movimientos += "\"x\":" + "\"" + movesData[i].X[j] + "\"" + ",";
				movimientos += "\"y\":" + "\"" + movesData[i].Y[j] + "\"" + ",";
				movimientos += "\"z\":" + "\"" + movesData [i].Z [j] + "\"";
				movimientos += "}";
				if (j != movesData[i].X.Count - 1)
					movimientos += ",{";
				else
					movimientos += "]}";
			}
			if (i != movesData.Count - 1)
				movimientos += ",{";
		}
		movimientos += "]";

		Debug.Log (movimientos);
		//string testTiros = "[{\"turno\": [{\"x\": \"-47.016\",\"y\": \"1.00015\",\"z\": \"-144.79\",\"nivel\": \"1\",\"acierto\": \"1\"}, {\"x\": \"-46.314\",\"y\": \"0.76515\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.296\",\"y\": \"0.96715\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.889\",\"y\": \"0.71315\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-47.068\",\"y\": \"1.60115\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}]}]";
		//gearVrTxtField2.text = auxProm.ToString();
		//DosificationController.instance.SaveEvolutionBallGame("33.2", "1", "12", "4", "4", "3",testTiros);
	//	DosificationController.instance.SaveEvolutionFishGame(auxPromExito.ToString(), winner.ToString(), peces.ToString(),"5", DosificationController.instance.exercise2Data.id_ejercicio, movimientos);
		//WWWForm phoneData = new WWWForm();
		//phoneData.AddField("tiros", tiros);
		//WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
		////StartCoroutine(PostGetEnumerator(www));
		//StartCoroutine("PostTurnDataEnumerator", www);
	}


	void PostGetExersice(string id, string ideE)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("id_ejercicio", ideE);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
		//StartCoroutine(PostGetEnumerator(www));

		www = new WWW ("http://www.creadoraproducciones.com/API/prueba.php");
		StartCoroutine("PostGetEnumerator", www);
	}
	void PostOnline(string id, string status, float lat, float lon)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("status", status);
		phoneData.AddField("latitud", lat.ToString());
		phoneData.AddField("longitud", lon.ToString());

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
		//StartCoroutine(PostOnlineEnumerator(www));

		StartCoroutine("PostOnlineEnumerator", www);
	}
	void PostData(string dni, string pass)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("email", dni);
		phoneData.AddField("pass", pass);

		//statusTxt.text += System.Text.Encoding.UTF8.GetString(phoneData.data);
		WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
		// StartCoroutine(PostdataEnumerator(www));
		StartCoroutine("PostdataEnumerator", www);

	}
	IEnumerator PostOnlineEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			PostGetExersice("3", "8");
			//print(www.text);
			StopCoroutine("PostOnlineEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}



	IEnumerator PostGetEnumerator(WWW www)
	{   
		yield return www; 
		if (www.error != "Null")
		{
			Debug.Log("Dosificacion");          
			print(www.text);
			DosificationController.instance.SaveDataExcersiseOncologico(www);
			//PostTurnsData();
			Debug.Log(www.text);
			if (DosificationController.instance.exerciseOncologicoData.status)
			{
				Debug.Log("aqui");
				if(headGameState == HeadGameState.LOGIN)
					headGameState = HeadGameState.COUNTER;
			}
			else
			{
				headGameState = HeadGameState.NOSESION;
			}
			StopCoroutine("PostGetEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostdataEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			//print(www.text);
			DosificationController.instance.SaveDataPacient(www);
			//PostOnline("3", "1","1231","-1231");
			keyExercise = true;
			StopCoroutine("PostdataEnumerator");
		}
		else
		{
			StopAllCoroutines();
			//ballGameState = BallGameState.WELCOME;
			Debug.Log(www.error);
		}
	}
}
