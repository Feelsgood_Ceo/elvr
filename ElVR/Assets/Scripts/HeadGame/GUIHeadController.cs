﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIHeadController : MonoBehaviour
{

    [SerializeField]
    [Range(0.01f, 0.05f)]
    private float amplitude = 0.01f;

    [SerializeField]
    [Range(7f, 15f)]
    private float rotationSpeed = 7f;

    float runningTime = 0f;

    // Update is called once per frame
    void Update()
    {
        //Floating coin
        Vector3 newPos = transform.position;
        float deltaHeight = (Mathf.Sin(runningTime + Time.deltaTime) 
            - Mathf.Sin(runningTime));
        newPos.y += deltaHeight * amplitude;
        runningTime += Time.deltaTime;
        transform.position = newPos;

        transform.eulerAngles += new Vector3(0f, 
            rotationSpeed * Time.deltaTime, 0f);
    }
}
