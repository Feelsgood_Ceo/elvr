﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorAnimalPrefab : MonoBehaviour {
	public enum AnimalState
	{
		APARICION,
		IDA,
		VUELTA,
		IDLE,
		DESAPARICION,
	}
	public AnimalState animalState;

	public bool isCaptured=false;
	public float velocity;
	public Transform Spawn;
	private Transform destineActual;
	public Transform destine1;
	public Transform destine2;
	public float timeIdle=6;
	public int tipo_mov;

	private Animator anim;
	private bool help=false;




	// Use this for initialization
	void Start () {
		animalState = AnimalState.APARICION;
		transform.position = Spawn.parent.GetChild(1).position;
		destineActual = Spawn;
		anim = this.transform.GetChild (1).GetComponent<Animator> ();
		Debug.Log (this.transform.GetChild (1).name);
	}
	
	// Update is called once per frame
	void Update () {
		switch (animalState) {
		case AnimalState.APARICION:
			if (help == false) {
				anim.SetTrigger ("correr");
				help = true;
			}
			transform.LookAt (destineActual);
			transform.position = Vector3.MoveTowards (transform.position, destineActual.position, 3 * Time.deltaTime);
			if (Vector3.Distance (transform.position, Spawn.position) <= 0.1f) {
				if (destine1 != null) {
					destineActual = destine1;
				} else {
					destineActual = destine2;
				}
				animalState = AnimalState.IDA;
			}
			break;
		case AnimalState.IDA:
			if (help == true) {
				if (tipo_mov == 1) {
					anim.SetTrigger ("correr");
				} else if (tipo_mov == 2) {
					anim.SetTrigger ("saltar");
				} else if (tipo_mov == 3) {
					anim.SetTrigger ("subir");
					transform.LookAt (ControladorHeadGame.instance.hand);
				} else {
					anim.SetTrigger ("bajar");
					transform.LookAt (ControladorHeadGame.instance.hand);
				}
				help = false;
			}
			transform.LookAt (destineActual);
			transform.position = Vector3.MoveTowards (transform.position, destineActual.position, velocity * Time.deltaTime);

			if (destine1 != null) {
				if (Vector3.Distance (transform.position, destine1.position) <= 0.5f) {
						destineActual = destine2;
				}
			}

			if(Vector3.Distance(transform.position, destine2.position) <= 0.2f){
				animalState = AnimalState.IDLE;
			}
			break;
		case AnimalState.IDLE:
			if (help == false) {
				if (tipo_mov == 1) {
					anim.SetTrigger ("idle");
				} else if (tipo_mov == 2) {
					if (destine2.parent.name.Contains ("4") || destine2.parent.name.Contains ("5") || destine2.parent.name.Contains ("6")) {
						anim.SetTrigger ("idletronco");
					} else {
						anim.SetTrigger ("idlecopa");
					}

				} else if (tipo_mov == 3) {
					anim.SetTrigger ("idlecopa");
				} else {
					anim.SetTrigger ("idle");
				}
				help = true;
			}
			transform.LookAt (ControladorHeadGame.instance.hand);
			timeIdle -= Time.deltaTime;
			if (timeIdle <= 0 || isCaptured) {
				animalState = AnimalState.VUELTA;
				if (destine1 != null) {
					destineActual = destine1;
				} else {
					destineActual = Spawn;
				}
			}
			break;
		case AnimalState.VUELTA:
			if (help == true) {
				if (tipo_mov == 1) {
					anim.SetTrigger ("correr");
					transform.LookAt (destineActual);
				} else if (tipo_mov == 2) {
					anim.SetTrigger ("saltar");
					transform.LookAt (destineActual);
				} else if (tipo_mov == 3) {
					anim.SetTrigger ("subir");
					transform.LookAt (ControladorHeadGame.instance.hand);
				} else {
					anim.SetTrigger ("bajar");
					transform.LookAt (ControladorHeadGame.instance.hand);
				}
				help = false;
			}
			transform.position = Vector3.MoveTowards (transform.position, destineActual.position, velocity * Time.deltaTime);
			if (destine1 != null) {
				if (Vector3.Distance (transform.position, destine1.position) <= 0.5f) {
					destineActual = Spawn;
				}
			}

			if(Vector3.Distance(transform.position, Spawn.position) <= 0.2f){
				animalState = AnimalState.DESAPARICION;
				destineActual = Spawn.parent.GetChild(1);
			}

			break;
		case AnimalState.DESAPARICION:
			if (help == false) {
				anim.SetTrigger ("correr");
				help = true;
			}
			transform.LookAt (destineActual);
			transform.position = Vector3.MoveTowards (transform.position, destineActual.position, 3 * Time.deltaTime);
			if (Vector3.Distance (transform.position, destineActual.position) <= 0.1f) {
				ControladorHeadGame.isCreate = false;
				Destroy (this.gameObject);
			}
			break;
		default:
			break;
		}
	}
}