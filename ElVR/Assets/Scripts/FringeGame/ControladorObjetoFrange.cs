﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorObjetoFrange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(ControladorFringeGame.instance.GetFrangeGameState()==ControladorFringeGame.FrangeGameState.PLAYING)
		transform.Translate(Vector3.left * Time.deltaTime * ControladorFringeGame.vel);
	}

	void OnTriggerExit(Collider col){
	}
}
