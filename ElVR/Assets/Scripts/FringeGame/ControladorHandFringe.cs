﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorHandFringe : MonoBehaviour {
	public static bool isclosed=false;
	// Use this for initialization
	void Start () {
		
	}
		
	void OnTriggerEnter(Collider col){
		if (ControladorFringeGame.instance.GetFrangeGameState () == ControladorFringeGame.FrangeGameState.PLAYING) {
			if (col.name == "Barrel") {
				if (isclosed) {
					ControladorFringeGame.cantidadObjAtrapados++;
					Destroy (this.transform.GetChild (2).gameObject);
					isclosed = false;
					ControladorFringeGame.isSaved = true;
				}
			} else {
				if (this.transform.childCount <= 2) {
					isclosed = true;
					col.transform.position = this.transform.position;
					col.GetComponent<Collider> ().isTrigger = true;
					col.GetComponent<Rigidbody> ().isKinematic = true;
					col.GetComponent<ControladorObjetoFrange> ().enabled = false;
					col.transform.SetParent (this.transform);
				}
			}
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
