﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorFringeGame : MonoBehaviour {
	struct MoveDataStruct

	{
		private List<float> x;
		private List<float> y;
		private List<float> z;

		public List<float> X
		{
			get
			{
				return x;
			}

			set
			{
				x = value;
			}
		}

		public List<float> Y
		{
			get
			{
				return y;
			}

			set
			{
				y = value;
			}
		}

		public List<float> Z
		{
			get
			{
				return z;
			}

			set
			{
				z = value;
			}
		}

	}
	public enum FrangeGameState
	{
		LOGIN,
		NOSESION,
		WELCOME,
		WELCOME2,
		TUTORIAL,
		TUTORIAL2,
		TUTORIAL3,
		TUTORIAL4,
		TUTORIAL5,
		TUTORIAL6,
		SPAWN1,
		SPAWN2,
		SPAWN3,
		COUNTER,
		ELVRMOVEMENT,
		PLAYING,
		GAMEOVER,
		ENDGAME
	}

	public static ControladorFringeGame instance;


	public int E_left;
	public float E_velocidad;
	public int E_isForTime;
	public int E_Duracion_Cantidad;



	public Transform[] objetos;
	public Transform franja;
	public Transform inicioFranja;
	public Transform barril;
	public Transform hand;	
	public Text timer;
	public Text objAparecieron;
	public Text objAtrapados;
	public static float vel;
	public static int cantidadObjAparecieron;
	public static int cantidadObjAtrapados;
	private float promExito;
	private int winner;

	public Image gearVrImageField;
	public FrangeGameState frangeGameState;
	private bool isConeccting;
	private bool isDosificated;
	//Keys to connect
	public bool keyExercise;
	public bool keyGps;
	//Data from phone
	private float latitud=0;
	private float longitud=0;


	private bool create;
	public static bool isSaved=false;

	//MOVES
	private List<MoveDataStruct> movesData;
	private List<float> dataXpos;
	private List<float> dataYpos;
	private List<float> dataZpos;
	private float dataCounter;

	//TIMER
	private float secondsCount;
	private float secondsCount2;
	private int minuteCount;
	private int hourCount;

	//Textures for tutorial
	public Sprite connectingSprt;
	public Sprite tutorialFish1Sprt;
	public Sprite tutorialFish2Sprt;
	public Sprite tutorialFish3Sprt;
	public Sprite tutorialFish4Sprt;
	public Sprite tutorialFish5Sprt;
	public Sprite tutorialFish6Sprt;
	public Sprite tutorialFish7Sprt;
	public Sprite tutorialFish8Sprt;
	public Sprite gameFinishSprt;
	public Sprite noSesionSprt;





	public FrangeGameState GetFrangeGameState()
	{
		return frangeGameState;
	}

	void Awake()
	{
		instance = this;
	}

	void Start () {
		create = false;
		keyGps = true;

		//Initialize the list variables
		movesData = new List<MoveDataStruct>();
		dataXpos = new List<float>();
		dataYpos = new List<float>();
		dataZpos = new List<float>();
		dataCounter = 0.3f;
	}

	IEnumerator crearObjeto(float time){
		Transform objt = Instantiate (objetos[Random.Range(0,objetos.Length)]) as Transform;
		cantidadObjAparecieron++;
		objt.position = inicioFranja.position;
		create = true;
		yield return new WaitForSeconds (time);
		create = false;
	}

	public void UpdateUI(){
		secondsCount += Time.deltaTime;
		secondsCount2 += Time.deltaTime;
		if (E_isForTime==1) {
			if (secondsCount2 < E_Duracion_Cantidad + 1) {
				timer.text = hourCount + "h:" + minuteCount + "m:" + (int)secondsCount + "s";
			} else {
				
				frangeGameState = FrangeGameState.GAMEOVER;
			}
		} else {
			if (cantidadObjAtrapados == E_Duracion_Cantidad) {
				Debug.Log ("CANTIDAD");
				frangeGameState = FrangeGameState.GAMEOVER;
			}
		}

		objAparecieron.text = "" + cantidadObjAparecieron;
		objAtrapados.text = "" + cantidadObjAtrapados;

		if(secondsCount >= 60){
			minuteCount++;
			secondsCount = 0;
		}else if(minuteCount >= 60){
			hourCount++;
			minuteCount = 0;
		}    
	}

	// Update is called once per frame
	void Update () {
		switch (frangeGameState){
		case FrangeGameState.LOGIN:
			if (!isConeccting) {
				PostData ("paciente@paciente.com", "paciente");
				gearVrImageField.sprite = connectingSprt;
				isConeccting = true;
			}

			if(keyExercise && keyGps)
			{
				if (!isDosificated) {
					PostOnline ("3", "1", longitud, latitud);
					isDosificated = true;
				}
			}               

			break;
		case FrangeGameState.COUNTER:
			
			E_left = DosificationController.instance.exercise3Data.posicion;
			E_velocidad = DosificationController.instance.exercise3Data.velocidad;
			E_isForTime = DosificationController.instance.exercise3Data.tipo_ejercicio;
			E_Duracion_Cantidad = DosificationController.instance.exercise3Data.tiempo_cantidad;


			Debug.Log ("counter");
			gearVrImageField.sprite = tutorialFish1Sprt;
			if (E_left==1) {
			} else {
				barril.position = new Vector3(barril.position.x*-1,barril.position.y,barril.position.z);
			}
			frangeGameState = FrangeGameState.PLAYING;
			break;	


		case FrangeGameState.PLAYING:
			
			UpdateUI ();

			if (!create) {
				StartCoroutine (crearObjeto (1/E_velocidad));
			}
			vel = E_velocidad;
			franja.GetComponent<Renderer> ().material.mainTextureOffset = new Vector2 (0,Time.time*E_velocidad/3*-1);	


			if (ControladorHandFringe.isclosed) {

				dataCounter += Time.deltaTime;

				if (dataCounter >= 0.3f)
				{
					SaveMoveData(hand.transform.position.x, hand.transform.position.y, hand.transform.position.z);
					dataCounter = 0f;
				}
			}
			if (isSaved) {
				SaveMoveToJson(dataXpos,dataYpos,dataZpos);
				isSaved = false;
			}



			break;


		case FrangeGameState.GAMEOVER:

			promExito = (cantidadObjAtrapados * 100) / cantidadObjAparecieron;
			promExito = Mathf.Round (promExito * 100f) / 100f;

			Debug.Log (promExito);

			if (promExito > 60) {
				winner = 1;
			} else {
				winner = 0;
			}



			PostMovesData ();
			DosificationController.instance.PostCompletaEjercicio3 ();
			frangeGameState = FrangeGameState.ENDGAME;

			break;
		case FrangeGameState.ENDGAME:

			break;
		default:
			break;
		}     
	}

	public void SetKeyGps(bool status)
	{
		keyGps = status;
	}

	public void SaveMoveData(float x, float y, float z)
	{
		dataXpos.Add(x);
		dataYpos.Add(y);
		dataZpos.Add(z);


	}

	public void SaveMoveToJson(List<float> x, List<float> y, List<float> z)
	{
		MoveDataStruct auxStruc = new MoveDataStruct();
		auxStruc.X = new List<float>();
		auxStruc.Y = new List<float>();
		auxStruc.Z = new List<float>();

		for (int i = 0; i < x.Count; i++)
		{
			auxStruc.X.Add(x[i]);
			auxStruc.Y.Add(y[i]);
			auxStruc.Z.Add(z[i]);

			Debug.Log ("x:"+x[i]+ "Y:"+y[i] + "Z:"+z[i]);
		}

		movesData.Add(auxStruc);

		dataXpos.Clear();
		dataYpos.Clear();
		dataZpos.Clear();
	}

	void PostMovesData()
	{
		string movimientos = "[{";
		for(int i = 0; i < movesData.Count; i ++)
		{

			movimientos += "\"movimiento\":[{";
			for (int j = 0; j < movesData[i].X.Count; j++)
			{
				movimientos += "\"x\":" + "\"" + movesData[i].X[j] + "\"" + ",";
				movimientos += "\"y\":" + "\"" + movesData[i].Y[j] + "\"" + ",";
				movimientos += "\"z\":" + "\"" + movesData [i].Z [j] + "\"";
				movimientos += "}";
				if (j != movesData[i].X.Count - 1)
					movimientos += ",{";
				else
					movimientos += "]}";
			}
			if (i != movesData.Count - 1)
				movimientos += ",{";
		}
		movimientos += "]";

		Debug.Log (movimientos);
		//string testTiros = "[{\"turno\": [{\"x\": \"-47.016\",\"y\": \"1.00015\",\"z\": \"-144.79\",\"nivel\": \"1\",\"acierto\": \"1\"}, {\"x\": \"-46.314\",\"y\": \"0.76515\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.296\",\"y\": \"0.96715\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-45.889\",\"y\": \"0.71315\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}, {\"x\": \"-47.068\",\"y\": \"1.60115\",\"z\": \"-144.61\",\"nivel\": \"2\",\"acierto\": \"1\"}]}]";
		//gearVrTxtField2.text = auxProm.ToString();
		//DosificationController.instance.SaveEvolutionBallGame("33.2", "1", "12", "4", "4", "3",testTiros);
		DosificationController.instance.SaveEvolutionFrangeGame(promExito.ToString(), winner.ToString(), cantidadObjAparecieron.ToString(),"6", DosificationController.instance.exercise3Data.id_ejercicio, movimientos);
		//WWWForm phoneData = new WWWForm();
		//phoneData.AddField("tiros", tiros);
		//WWW www = new WWW("http://feelsgood.care/neuralvr/api/guardaEvolucion", phoneData);
		////StartCoroutine(PostGetEnumerator(www));
		//StartCoroutine("PostTurnDataEnumerator", www);
	}
	void PostGetExersice(string id, string ideE)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("id_ejercicio", ideE);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/getEjercicio", phoneData);
		StartCoroutine("PostGetEnumerator", www);
	}
	void PostOnline(string id, string status, float lat, float lon)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("id_paciente", id);
		phoneData.AddField("status", status);
		phoneData.AddField("latitud", lat.ToString());
		phoneData.AddField("longitud", lon.ToString());

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/onlineDevice", phoneData);
		StartCoroutine("PostOnlineEnumerator", www);
	}
	void PostData(string dni, string pass)
	{
		WWWForm phoneData = new WWWForm();
		phoneData.AddField("email", dni);
		phoneData.AddField("pass", pass);

		WWW www = new WWW("http://feelsgood.care/neuralvr/api/login", phoneData);
		StartCoroutine("PostdataEnumerator", www);

	}
	IEnumerator PostOnlineEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			PostGetExersice("3", "6");
/////////////////////////////////////////////////////////////////////
		//	if (frangeGameState == FrangeGameState.LOGIN) {
		//		frangeGameState = FrangeGameState.COUNTER;
		//	}
////////////////////////////////////////////////////////////////////			
			StopCoroutine("PostOnlineEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostGetEnumerator(WWW www)
	{   
		yield return www;
		if (www.error != "Null")
		{
			Debug.Log("Dosificacion");          
			print(www.text);

			DosificationController.instance.SaveDataExcersise3(www);
			Debug.Log(www.text);
			if (DosificationController.instance.exercise3Data.status)
			{
				if(frangeGameState == FrangeGameState.LOGIN)
					frangeGameState = FrangeGameState.COUNTER;
			}
			else
			{
			}
			StopCoroutine("PostGetEnumerator");
		}
		else
		{
			print("Error");
			Debug.Log(www.error);
		}
	}
	IEnumerator PostdataEnumerator(WWW www)
	{
		yield return www;
		if (www.error != "Null")
		{
			DosificationController.instance.SaveDataPacient(www);
			keyExercise = true;
			StopCoroutine("PostdataEnumerator");
		}
		else
		{
			StopAllCoroutines();
			Debug.Log(www.error);
		}
	}
}
