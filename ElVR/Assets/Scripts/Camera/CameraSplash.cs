﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSplash : MonoBehaviour {

    public Vector3 cameraPosition;

    public void SetCameraPosition() {

        Transform player = GameObject.Find("Player").transform;

        player
            .transform
            .position = new Vector3(cameraPosition.x, cameraPosition.y, cameraPosition.z);
    }
}
