﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class Recenter : MonoBehaviour {

    private void Awake()
    {
        DontDestroyOnLoad(this);
        SetPositionalTracking();

        SceneManager.sceneLoaded += UpdateCameras;
    }
    
    /// <summary>
    /// Updates camera positions
    /// </summary>
    private void UpdateCameras(Scene scene, LoadSceneMode mode) {
        InputTracking.Recenter();

        switch (scene.buildIndex) {
            case 0://Splash
                RecenterCamera();
                SetSplashCameraPosition();
                break;

            default:
                RecenterCamera();
                break;
        }
    }

    /// <summary>
    /// Recenters camera every time a scene is loaded
    /// </summary>
    private void RecenterCamera() {
        InputTracking.Recenter();
    }

    /// <summary>
    /// Sets positional tracking
    /// </summary>
    private void SetPositionalTracking() {
        InputTracking.disablePositionalTracking = true;
    }

    /// <summary>
    /// Set Splash scene camera position
    /// </summary>
    private void SetSplashCameraPosition() {
        this.GetComponent<CameraSplash>().SetCameraPosition();
    }
}