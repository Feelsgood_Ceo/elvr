﻿// UI Mockup
// Version: 1.0.5
// Compatible: Unity 5.5.1 or higher, see more info in Readme.txt file.
//
// Developer:			Gold Experience Team (https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:4162)
// Unity Asset Store:	https://www.assetstore.unity3d.com/#!/content/76213
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com.

#region Namespaces

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

#endregion // Namespaces

// ######################################################################
// BGItem descripes buttons, far and near background in the scene. This class will be used in Demo
// ######################################################################

[System.Serializable]
public class BGItem
{
	public Button m_Button;
	public GameObject m_FarBG;
	public GameObject m_MidBG;
}

// ######################################################################
// UIP_Demo recieves user clicks on bottom icons. It switch the Canvas and control the UI animations.
// ######################################################################

public class UIP_Demo : MonoBehaviour
{

	// ########################################
	// Variables
	// ########################################

	#region Variables

	public List<BGItem> m_Backgrounds;			// Store background canvas and button in the list.
	public GameObject m_BackgroundSelection;	// GameObject of Border sprite that moves between background's icons.

	public List<GameObject> m_Mockups;			// Store GameObject of Mockup's Canvases.

	int m_MockupIndex = 0;						// Current Mockup index.
	int m_MockupIndexOld = 0;                   // Previous Mockup index.

	public GameObject m_HelpCanvas;				// GameObject of Help Canvas.

	public Button m_HomeButton;					// Home button.
	public Button m_HelpButton;                 // Help button.
	public Button m_HelpButtonClose;            // Close Help button.
	public Button m_PreviousButton;             // Left Arrow button.
	public Button m_NextButton;                 // Right Arrow button.

	public Text m_IndexAndTotal;				// Text to display current Canvas's index.
	public Text m_Subject;						// Text to display current Canvas's name.

	#endregion // Variables

	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html
	void Start()
	{
		// Make sure that m_HelpCanvas and all items in m_Mockups are inactived
		if (m_HelpCanvas.activeSelf)
			m_HelpCanvas.SetActive(false);		
		foreach(GameObject item in m_Mockups)
		{
			if (item.activeSelf)
				item.SetActive(false);
		}

		// Show Home Canvas
		ChangMockup(0);

		// Show "Background - Sunny" Canvas
		ChangeBackground(0);
	}

	// Update is called every frame, if the MonoBehaviour is enabled.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html
	void Update()
	{
		// User press Left Arrow Key on the Keyboard
		if(Input.GetKeyUp(KeyCode.LeftArrow))
		{
			PreviousButton();
		}
		// User press Right Arrow Key on the Keyboard
		else if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			NextButton();
		}
	}

	// ########################################
	// Respond functions for user's clicks.
	// ########################################

	// User clicks on "Button_Home" button
	public void HomeButton()
	{
		//Debug.Log("HomeButton()");

		// Change Mockup to first item in m_Mockups list
		ChangMockup(0);
	}

	// User clicks on "Button_Help" button
	public void HelpButton()
	{
		//Debug.Log("HelpButton()");

		// Check if Help Canvas is active then disable Help button
		if (m_HelpCanvas.activeSelf)
		{
			m_HelpButton.interactable = false;
		}
		// Other, show Help Canvas
		else
		{
			m_HelpCanvas.SetActive(true);
		}

		// Play Help Canvas In-Animation
		GSui.Instance.MoveIn(m_HelpCanvas.transform, true);
	}

	// User clicks on Close-Help button
	public void HelpButtonClose()
	{
		//Debug.Log("HelpButtonClose()");

		// Close Help Canvas
		StartCoroutine(HideCanvas(m_HelpCanvas, 0.3f));
	}

	// User clicks on "Button_Previous" button
	public void PreviousButton()
	{
		//Debug.Log("PreviousButton()");

		// Change Mockup to previous index
		ChangMockup(m_MockupIndex-1);
	}

	// User clicks on "Button_Next" button
	public void NextButton()
	{
		//Debug.Log("NextButton()");

		// Change Mockup to next index
		ChangMockup(m_MockupIndex+1);
	}

	// User clicks on "Button_Sunny" button
	public void BGSunnyButton()
	{
		//Debug.Log("BGSunnyButton()");

		// Change background to first item in m_Backgrounds list
		ChangeBackground(0);
	}

	// User clicks on "Button_Sunset" button
	public void BGSunsetButton()
	{
		//Debug.Log("BGSunsetButton()");

		// Change background to second item in m_Backgrounds list
		ChangeBackground(1);
	}

	// User clicks on "Button_Night" button
	public void BGNightButton()
	{
		//Debug.Log("BGNightButton()");

		// Change background to third item in m_Backgrounds list
		ChangeBackground(2);
	}

	// ########################################
	// Change, hide the Canvas functions
	// ########################################

	// Delay before hide OldCanvas and show NewCanvas
	IEnumerator ChangeCanvas(GameObject NewCanvas, GameObject OldCanvas, float Delay)
	{
		// Change Canvas
		if(OldCanvas!=NewCanvas)
		{
			// Play OldCanvas's Out-animations
			GSui.Instance.MoveOut(OldCanvas.transform, true);
			//GSui.Instance.SetInteracableAllButtons(false);

			// Wait for Delay time
			yield return new WaitForSeconds(Delay);

			// Deactive OldCanvas
			OldCanvas.SetActive(false);
		}

		// Wait for a frame
		yield return new WaitForEndOfFrame();

		// Active the NewCanvas
		NewCanvas.SetActive(true);

		// Reset NewCanvas's animations
		GSui.Instance.Reset(NewCanvas.transform);

		// Play NewCanvas's In-animations
		GSui.Instance.MoveIn(NewCanvas.transform, true);
		//GSui.Instance.SetInteracableAllButtons(true);
	}

	// Play CanvasToHide's Out-animations, Delay and Deactive it
	IEnumerator HideCanvas(GameObject CanvasToHide, float Delay)
	{
		// Play Out-animations
		GSui.Instance.MoveOut(CanvasToHide.transform, true);
		//GSui.Instance.SetInteracableAllButtons(false);

		// Wait for Delay time
		yield return new WaitForSeconds(Delay);

		CanvasToHide.SetActive(false);
		//GSui.Instance.SetInteracableAllButtons(true);

	}

	// Change Mockup to the given index
	void ChangMockup(int Index)
	{
		// Round MockupIndex between 0 and m_Mockups.Count - 1
		if (Index<0)
			Index = m_Mockups.Count - 1;
		else if (Index>=m_Mockups.Count)
			Index = 0;

		// Shift m_MockupIndex to m_MockupIndexOld
		m_MockupIndexOld = m_MockupIndex;

		// Set m_MockupIndex
		m_MockupIndex = Index;

		// Update "Text_IndexAndTotal" Text
		m_IndexAndTotal.text = (Index+1).ToString() + " / " + m_Mockups.Count.ToString();

		// Update "Text_Subject" Text
		m_Subject.text = m_Mockups[m_MockupIndex].name;

		// Active new Mockup then Play it's In-animations
		StartCoroutine(ChangeCanvas(m_Mockups[m_MockupIndex], m_Mockups[m_MockupIndexOld], 0.65f));

		// Toggle Home button depends on m_MockupIndex
		if (m_MockupIndex==0)
		{
			m_HomeButton.interactable = false;
		}
		else
		{
			m_HomeButton.interactable = true;
		}
	}

	// ########################################
	// Background functions
	// ########################################

	// Change Background to IndexToEnable index
	void ChangeBackground(int IndexToEnable)
	{
		for (int Index = 0; Index<m_Backgrounds.Count; Index++)
		{
			// Deactive Background Canvas
			m_Backgrounds[Index].m_Button.interactable = true;
			m_Backgrounds[Index].m_FarBG.SetActive(false);
			m_Backgrounds[Index].m_MidBG.SetActive(false);
			GSui.Instance.MoveIn(m_Backgrounds[Index].m_FarBG.transform, false);
			GSui.Instance.MoveIn(m_Backgrounds[Index].m_MidBG.transform, false);

			// Active Background Canvas which is at IndexToEnable index
			if (Index == IndexToEnable)
			{
				// Active Background Canvas
				m_BackgroundSelection.transform.position = m_Backgrounds[Index].m_Button.transform.position;
				m_Backgrounds[Index].m_Button.interactable = false;
				m_Backgrounds[Index].m_FarBG.SetActive(true);
				m_Backgrounds[Index].m_MidBG.SetActive(true);

				// Play In-animations
				GSui.Instance.Reset(m_Backgrounds[Index].m_FarBG.transform);
				GSui.Instance.Reset(m_Backgrounds[Index].m_MidBG.transform);
				GSui.Instance.MoveIn(m_Backgrounds[Index].m_FarBG.transform, true);
				GSui.Instance.MoveIn(m_Backgrounds[Index].m_MidBG.transform, true);
			}
		}
	}
}
