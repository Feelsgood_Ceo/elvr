﻿using FI.TAGS;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FI
{

    public class PersistentGO : MonoBehaviour
    {

        [SerializeField]
        private Texture2D[] lightMapsTextures;

        // ------------------------------------------
        void Start()
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += SceneLoaded;
        }

        // -----------------------------------------------------------------------
        private void SceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            LightmapData[] lightmaps = new LightmapData[lightMapsTextures.Length];

            for (int i = 0; i < lightmaps.Length; i++)
            {
                lightmaps[i] = new LightmapData();
                lightmaps[i].lightmapColor = lightMapsTextures[i];
            }

            if (!arg0.name.Equals(ScenesTags.splashScreen))
                transform.GetChild(0).gameObject.SetActive(true);
        }

        // ------------------------------------------
        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= SceneLoaded;
        }

    }

}