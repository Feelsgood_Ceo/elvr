﻿using UnityEngine;

namespace FI.AI.TRIS.INTRO
{

    public static class TrisTagsAnim
    {
        /// <summary>
        /// Trigger
        /// </summary>
        public static int happy = Animator.StringToHash("Happy");

        /// <summary>
        /// Trigger
        /// </summary>
        public static int sad = Animator.StringToHash("Sad");
    }

}