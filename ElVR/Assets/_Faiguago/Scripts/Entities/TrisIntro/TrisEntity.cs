﻿using FI.DS;
using FI.TAGS;
using UnityEngine;
using UnityEngine.Playables;

namespace FI.AI.TRIS.INTRO
{
    public class TrisEntity : LivingEntity
    {
        /// <summary>
        /// Tris state
        /// </summary>
        public enum State
        {
            Idle,
            Talking,
            Empty
        }

        #region FIRST_DIALOGUE

        /// <summary>
        /// Introductory dialogue
        /// </summary>
        private static string d00_1 = "Hola bonita, " +
            "soy Tris la hermanita de Juan estoy algo " +
            "cansada por que por mas que lo intento no " +
            "puedo alcanzar a tomarle una buena foto al " +
            "animalito ya que se mueven mucho. Me dices " +
            "que si me muevo mas despacio ya no se asustarian de mi?";

        /// <summary>
        /// Question!
        /// </summary>
        private static string d01_1 = "Tu crees que me podrias ayudar?";

        /// <summary>
        /// Positive answer
        /// </summary>
        private static string d10_1 = "Eres la mejor del mundo! " +
            "Te voy a ensenar que movimientos debes hacer.";

        /// <summary>
        /// Negative answer
        /// </summary>
        private static string d20_1 = "Oh, no te preocupes " +
            "Cuando tengas un tiempito me ayudas!";

        #endregion

        #region SECOND_DIALOGUE

        private static string d00_2 = "Hola de nuevo! " +
            "Me quieres ayudar a tomar fotos para llenar " +
            "mi super album de animales? Los quiero a todos!";

        private static string d10_2 = ""; // Empty

        private static string d20_2 = "Oh, no te preocupes " +
            "Cuando tengas un tiempito me ayudas!";

        #endregion

        #region AUDIOS

        // ------------------------------------------
        public AudioClip holaBonitaClip,
            tuCreesClip, eresMejorMundoClip,
            noTePreocupesClip, holaDeNuevoClip;
        
        #endregion

        #region VARIABLES

        /// <summary>
        /// Reference to the director
        /// </summary>
        private PlayableDirector director;
        public PlayableDirector _Director
        {
            get
            {
                return director;
            }
        }

        // ------------------------------------------
        private FiniteStateMachine emptyState, idleState,
            talkingState, initialState;

        // ------------------------------------------
        [SerializeField]
        private GameObject cameraGO;
        public GameObject _CameraGO
        {
            get
            {
                return cameraGO;
            }
        }

        #endregion

        #region UNITY_API

        // ----------------------------
        protected override void Start()
        {
            base.Start();
        }

        // ------------------------------
        protected override void Update()
        {
            base.Update();
        }

        #endregion

        // ------------------------------------------
        protected override void InitiliazeStates()
        {
            // Instantiate the states
            initialState = new InitialState(this);
            idleState = new IdleState(this);
            talkingState = new TalkingState(this, false, true);
            emptyState = new EmptyState(this);

            // Setting the current state
            currentState = initialState;
            currentState.OnStateEnter();
        }

        // ------------------------------------------------
        protected override void InitiliazeComponents()
        {
            base.InitiliazeComponents();

            director = FindObjectOfType<PlayableDirector>();
        }

        // -------------------------------------------------------------------------------------------------------------
        protected override void InitializeNodes()
        {
            dialogueNodes = new Node[2];

            dialogueNodes[0] = new Node(
                                    new Dialogue(d00_1, null, holaBonitaClip),
                                    new Node(new Dialogue(d01_1, null, tuCreesClip),
                                        new Node(new Dialogue(d10_1, () => { StartGame(true); }, eresMejorMundoClip)),
                                        new Node(new Dialogue(d20_1, EndInteraction, noTePreocupesClip))));

            dialogueNodes[1] = new Node(
                                    new Dialogue(d00_2, null, holaDeNuevoClip),
                                    new Node(new Dialogue(d10_1, () => { StartGame(false); }, eresMejorMundoClip)),
                                    new Node(new Dialogue(d20_2, EndInteraction, noTePreocupesClip)));
        }

        /// <summary>
        /// Change the current state of Tris
        /// </summary>
        public void ChangeState(State state)
        {
            currentState.OnStateExit();

            switch (state)
            {
                case State.Idle:
                    currentState = idleState;
                    break;

                case State.Talking:
                    currentState = talkingState;
                    break;

                case State.Empty:
                    currentState = emptyState;
                    break;
            }

            currentState.OnStateEnter();
        }

        #region ACTIONS

        // -------------------------------------------------------------------
        private void EndInteraction()
        {
            ChangeState(State.Idle);
        }

        // -------------------------------------------------------------------
        private void StartGame(bool isTutorial)
        {
            int val = isTutorial ? 0 : 1;

            PlayerPrefs.SetInt(
                   PlayerPrefsTags.monkeyGameMode, val);

            ChangeState(State.Empty);
            StartCoroutine(Utils.LoadNextScene(ScenesTags.monkeyGame));
        }

        #endregion

    }
}