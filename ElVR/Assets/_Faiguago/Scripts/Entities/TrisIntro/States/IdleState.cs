﻿using UnityEngine;

namespace FI.AI.TRIS.INTRO
{

    public class IdleState : FiniteStateMachine
    {
        /// <summary>
        /// Reference to the main controller
        /// </summary>
        private TrisEntity controller;

        // ------------------------------------------------
        public IdleState(TrisEntity controller)
        {
            this.controller = controller;
        }

        // ------------------------------------------------
        public override void OnStateEnter()
        {
            ChangeHeaderTipVisibility(true);
            controller._IDialogInt.ShowAnimatedText();
        }

        // ------------------------------------------------
        public override void OnStateExit()
        {
            ChangeHeaderTipVisibility(false);
            controller._CurrentDialogIndex = 0;
            controller._IDialogInt.HideAnimatedText();
        }

        // ------------------------------------------------------
        public override void OnStateUpdate()
        {
            if (Input.GetButtonDown("Fire1"))
                controller.ChangeState(TrisEntity.State.Talking);
        }

        /// <summary>
        /// Change the visibility of the header tip
        /// </summary>
        private void ChangeHeaderTipVisibility(bool visibility)
        {
            if (visibility)
                controller._HeaderTipAnimator.SetTrigger("Show");
            else
                controller._HeaderTipAnimator.SetTrigger("Hide");
        }

    }

}