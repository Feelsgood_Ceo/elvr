﻿namespace FI.AI.TRIS.INTRO
{

    public class EmptyState : FiniteStateMachine
    {
        /// <summary>
        /// Reference to the main controller
        /// </summary>
        private TrisEntity controller;

        // ------------------------------------------------
        public EmptyState(TrisEntity controller)
        {
            this.controller = controller;
        }

        // ------------------------------------------------
        public override void OnStateEnter() { }

        // ------------------------------------------------
        public override void OnStateExit() { }

        // ------------------------------------------------
        public override void OnStateUpdate() { }
        
    }

}