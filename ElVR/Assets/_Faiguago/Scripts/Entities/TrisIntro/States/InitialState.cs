﻿namespace FI.AI.TRIS.INTRO
{

    public class InitialState : FiniteStateMachine
    {
        /// <summary>
        /// Reference to the main controller
        /// </summary>
        private TrisEntity controller;

        // ------------------------------------------------
        public InitialState(TrisEntity controller)
        {
            this.controller = controller;
        }

        // ------------------------------------------------
        public override void OnStateEnter() { }

        // ------------------------------------------------
        public override void OnStateExit() { }

        // ------------------------------------------------
        public override void OnStateUpdate()
        {
            if (controller._Director.duration - controller._Director.time <= 0.1f)
            {
                controller._Director.enabled = false;
                controller.ChangeState(TrisEntity.State.Talking);
            }
        }
    }

}
