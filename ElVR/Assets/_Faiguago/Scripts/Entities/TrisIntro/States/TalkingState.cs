﻿using FI.DS;
using FI.TAGS;
using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FI.AI
{

    public class TalkingState : FiniteStateMachine
    {
        /// <summary>
        /// Reference to the main controller
        /// </summary>
        private LivingEntity controller;

        /// <summary>
        /// Current Node
        /// </summary>
        private Node currentNode;

        /// <summary>
        /// Current Dialogue String
        /// </summary>
        private string currentDialogueString;

        /// <summary>
        /// The dialogue is being animated
        /// </summary>
        private bool isAnimating;

        /// <summary>
        /// Allow Controller Inputs
        /// </summary>
        private bool allowControllerInputs;

        /// <summary>
        /// Continue without interaction
        /// </summary>
        private bool autoContinue;

        // ------------------------------------------------
        public TalkingState(LivingEntity controller,
            bool allowButtonInteraction = false,
            bool allowControllerInputs = true,
            bool autoContinue = true)
        {
            this.controller = controller;
            this.autoContinue = autoContinue;
            this.allowControllerInputs = allowControllerInputs;

            // Register the events
            if (allowButtonInteraction)
            {
                controller._IDialogInt.continueButtonClicked
                    += ContinueButtonClicked;
                controller._IDialogInt.noButtonClicked
                    += NoButtonClicked;
                controller._IDialogInt.yesButtonClicked
                    += YesButtonClicked;
            }
        }

        // ---------------------------------------------------------------
        private void YesButtonClicked(object sender, EventArgs e)
        {
            currentNode = currentNode.GetChild(0);

            currentDialogueString = currentNode._Dialogue.dialogue;

            controller.StartCoroutine(AnimateDialogue());
        }

        // ---------------------------------------------------------------
        private void ContinueButtonClicked(object sender, EventArgs e)
        {
            if (currentNode.ChildrenAmount() == -1)
            {
                currentNode._Dialogue.dialogueAction();
            }
            else
            {
                currentNode = currentNode.GetChild(0);
                currentDialogueString = currentNode._Dialogue.dialogue;

                controller.StartCoroutine(AnimateDialogue());
            }
        }

        // ---------------------------------------------------------------
        private void NoButtonClicked(object sender, EventArgs e)
        {
            currentNode = currentNode.GetChild(1);

            currentDialogueString = currentNode._Dialogue.dialogue;

            controller.StartCoroutine(AnimateDialogue());
        }

        // -----------------------------------------------------------------
        public override void OnStateEnter()
        {
            //if (SceneManager.GetActiveScene().name.Equals(ScenesTags.intro))
            //    currentNode = controller.DialogueNode(1);
            //else

            currentNode = controller._CurrentNode;

            currentDialogueString = currentNode._Dialogue.dialogue;

            controller._IDialogInt.ShowPanel();
            controller._IDialogInt.SetNameText(controller._EntityName);

            controller.StartCoroutine(AnimateDialogue());
        }

        // ------------------------------------------------
        public override void OnStateExit()
        {
            controller._CurrentDialogIndex++;
            controller._IDialogInt.HidePanel();
        }

        // ------------------------------------------------
        public override void OnStateUpdate()
        {
            if (!isAnimating && allowControllerInputs)
            {
                CheckNextSentence();
            }
        }

        // ------------------------------------------------
        private void CheckNextSentence()
        {
            bool option1 = Input.GetButtonDown("Fire1");

            // Originally the button was Fire2
            bool option2 = Input.GetButtonDown("Fire1");

            if (option1 || option2 || autoContinue)
            {
                if (currentNode.ChildrenAmount() == 1)
                {
                    currentNode = currentNode.GetChild(0);
                    currentDialogueString = currentNode._Dialogue.dialogue;

                    if (currentDialogueString.Contains("la mejor del"))
                        controller._Animator.SetTrigger(TRIS.INTRO.TrisTagsAnim.happy);

                    controller.StartCoroutine(AnimateDialogue());
                }
                else if (currentNode.ChildrenAmount() >= 2)
                {
                    if (option1 || autoContinue)
                    {
                        currentNode = currentNode.GetChild(0);
                    }
                    else if (option2)
                    {
                        currentNode = currentNode.GetChild(1);
                    }

                    currentDialogueString = currentNode._Dialogue.dialogue;

                    if (currentDialogueString.Contains("la mejor del"))
                        controller._Animator.SetTrigger(TRIS.INTRO.TrisTagsAnim.happy);

                    controller.StartCoroutine(AnimateDialogue());
                }
                else
                {
                    currentNode._Dialogue.dialogueAction();
                }
            }
        }

        // ------------------------------------------------
        private IEnumerator AnimateDialogue()
        {
            HideButtons();
            isAnimating = true;

            controller._IDialogInt.CleanText();
            controller._IDialogInt.SetNameText(controller._EntityName);

            StringBuilder builder = new StringBuilder();

            yield return new WaitForSeconds(0.3f);

            controller._IDialogInt.PlayAudioClip(currentNode._Dialogue.audioClip);

            for (int i = 0; i < currentDialogueString.Length; i++)
            {
                builder.Append(currentDialogueString[i]);
                controller._IDialogInt.SetDialogText(builder);

                yield return new WaitForSeconds(controller._TalkSpeed);
            }

            CheckButtons();

            yield return new WaitForSeconds(1f);

            isAnimating = false;
        }

        // ------------------------------------------------
        private void CheckButtons()
        {
            bool yesNoButtons = currentNode.ChildrenAmount() >= 2;

            if (yesNoButtons)
            {
                controller._IDialogInt.ShowYesNoButtons(true);
            }
            else
            {
                controller._IDialogInt.ShowContinueButtons(true);
            }
        }

        // ------------------------------------------------
        private void HideButtons()
        {
            controller._IDialogInt.ShowContinueButtons(false);
            controller._IDialogInt.ShowYesNoButtons(false);
        }

    }

}