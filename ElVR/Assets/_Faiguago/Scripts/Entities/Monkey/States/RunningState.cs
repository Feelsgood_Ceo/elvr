﻿using FI.TAGS;

namespace FI.AI.MONKEY
{

    public class RunningState : FiniteStateMachine
    {
        private Monkey controller;

        private MonkeyMotionComponent motionComponent;
        
        // ----------------------------------------------
        public RunningState(Monkey controller)
        {
            this.controller = controller;
            motionComponent = controller._MonkeyMotionComponent;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            motionComponent.OnStateEnter(MonkeyMotionComponent.MovementType.Run);

            controller._Animator.SetFloat(MonkeyTagsAnim.speed, 1f);
        }
        
        // ----------------------------------------------
        public override void OnStateExit()
        {
            controller._Animator.SetFloat(MonkeyTagsAnim.speed, 0f);
        }

        // ----------------------------------------------
        public override void OnStateUpdate()
        {
            if (motionComponent.OnStateUpdate())
            {
                controller._TargetPos = controller
                    .PathFollower.GetPointInCircle(0.5f);
                controller.ChangeState(Monkey.States.Jumping);
            }
        }
        
    }

}