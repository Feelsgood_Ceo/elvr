﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class InitialState : FiniteStateMachine
    {
        private Monkey controller;

        public static event Action EntityWasSeen;

        // ----------------------------------------------
        public InitialState(Monkey controller)
        {
            this.controller = controller;
        }

        // ----------------------------------------------
        public override void OnStateEnter() { }

        // ----------------------------------------------
        public override void OnStateExit() { }

        // -----------------------------------------------------
        public override void OnStateUpdate()
        {
            if (controller._IMonkeyGameMode.CanSeeTheEntity
                && controller._IMonkeyTracker._EntityWasSeen)
            {
                controller._TargetPos = controller._WaypointPos;
                controller.ChangeState(Monkey.States.Running);

                if (EntityWasSeen != null)
                    EntityWasSeen();
            }
        }
    }

}