﻿using FI.TAGS;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class JumpingBetweenTreesState : FiniteStateMachine
    {
        private Monkey controller;

        private MonkeyMotionComponent motionComponent;

        // ----------------------------------------------
        public JumpingBetweenTreesState(Monkey controller)
        {
            this.controller = controller;
            motionComponent = controller._MonkeyMotionComponent;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            controller._TargetPos = GetRandomPosition();
            
            controller._Animator.SetTrigger(MonkeyTagsAnim.setJumping);
            motionComponent.OnStateEnter(MonkeyMotionComponent.MovementType.JumpBetweenTrees);
        }

        // ----------------------------------------------
        public override void OnStateExit() { }

        // ----------------------------------------------
        public override void OnStateUpdate()
        {
            if (motionComponent.OnStateUpdate())
            {
                controller.ChangeState(Monkey.States.Idle);
            }
        }

        /// <summary>
        /// Get Random Position 
        /// </summary>
        /// <returns></returns>
        private Vector3 GetRandomPosition()
        {
            return Vector3.zero;
        }

    }

}