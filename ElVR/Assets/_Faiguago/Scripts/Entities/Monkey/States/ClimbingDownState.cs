﻿using FI.TAGS;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class ClimbingDownState : FiniteStateMachine
    {
        private Monkey controller;

        private MonkeyMotionComponent motionComponent;

        // ----------------------------------------------
        public ClimbingDownState(Monkey controller)
        {
            this.controller = controller;
            motionComponent = controller._MonkeyMotionComponent;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            controller._TargetPos = controller._WaypointPos;
            motionComponent.OnStateEnter(MonkeyMotionComponent.MovementType.Jump);

            controller._Animator.SetTrigger(MonkeyTagsAnim.setJumping);
        }

        // ----------------------------------------------
        public override void OnStateExit() { }

        // ----------------------------------------------
        public override void OnStateUpdate()
        {
            if (motionComponent.OnStateUpdate())
                controller.ChangeState(Monkey.States.Posing);
        }
        
    }

}