﻿using FI.TAGS;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class IdleState : FiniteStateMachine
    {
        private Monkey controller;

        private Vector3 dir;
        private float interpolator;
        private Quaternion initialRot;

        // ----------------------------------------------
        public IdleState(Monkey controller)
        {
            this.controller = controller;
        }

        // ---------------------------------------------------------------
        public override void OnStateEnter()
        {
            interpolator = 0f;
            initialRot = controller.transform.rotation;

            dir = controller._PlayerPos - controller.transform.position;
            dir.y = 0f;
            dir.Normalize();

            controller._Animator.SetTrigger(MonkeyTagsAnim.setIdle);
        }

        // ---------------------------------------------------------
        public override void OnStateExit() { }

        // ---------------------------------------------------------
        public override void OnStateUpdate()
        {
            UpdateRotation();

            if (controller._IMonkeyGameMode._IstheGameActive)
            {
                if (controller._WasTheGameCompleted)
                {
                    controller.ChangeState(Monkey.States.ClimbingDown);
                }
                else if (controller._IMonkeyTracker._EntityWasSeen)
                {
                    controller.ChangeState(Monkey.States.Climbing);
                }
            }
        }

        /// <summary>
        /// Update the rotation to see the player
        /// </summary>
        private void UpdateRotation()
        {
            interpolator += Time.deltaTime;

            if (interpolator <= 1)
            {
                controller.transform.rotation =
                    Quaternion.Lerp(initialRot, Quaternion.LookRotation(dir), interpolator * 5f);
            }
        }
    }

}