﻿using FI.TAGS;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class JumpingState : FiniteStateMachine
    {
        private Monkey controller;

        private MonkeyMotionComponent motionComponent;
        
        // ----------------------------------------------
        public JumpingState(Monkey controller)
        {
            this.controller = controller;
            motionComponent = controller._MonkeyMotionComponent;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            controller._Animator.SetTrigger(MonkeyTagsAnim.setHighJump);

            motionComponent.OnStateEnter(MonkeyMotionComponent.MovementType.Jump);
        }

        // ----------------------------------------------
        public override void OnStateExit() { }

        // ----------------------------------------------
        public override void OnStateUpdate()
        {
            if (motionComponent.OnStateUpdate())
            {
                controller.ChangeState(Monkey.States.Idle);
            }
        }
        
    }

}