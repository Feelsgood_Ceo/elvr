﻿using FI.TAGS;
using FI.TOOLS;
using System;
using System.Collections;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class ClimbingState : FiniteStateMachine
    {
        private Monkey controller;

        private bool isClimbingDown;

        private float interpolator;
        private Vector3 oldPos;

        private bool reverseDir;

        private bool numberOfLapsSetted, angleSetted;

        private int numberOfLaps, endReached;

        private ExerciseType currentExercise;

        public static event Action TurnCompletedEvent;

        // ------------------------------------
        public ClimbingState(Monkey controller)
        {
            endReached = 0;
            numberOfLaps = 0;
            reverseDir = false;
            interpolator = 0.5f;

            this.controller = controller;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            if (interpolator >= 1f)
                reverseDir = true;
            else
                reverseDir = false;

            angleSetted = false;
            numberOfLapsSetted = false;

            currentExercise = controller._ExerciseType[numberOfLaps];

            if (currentExercise == ExerciseType.Vertical)
            {
                if (interpolator >= 1f)
                {
                    isClimbingDown = true;
                    controller.StartCoroutine(
                        MoveToDownPosition(controller.PathFollower.LeftPoint));
                }
                else if (interpolator <= 0f)
                {
                    isClimbingDown = true;
                    controller.StartCoroutine(
                        MoveToDownPosition(controller.PathFollower.RightPoint));
                }
            }

            oldPos = controller.transform.position;
            controller._Animator.SetTrigger(MonkeyTagsAnim.setClimbing);
        }

        // -------------------------------
        public override void OnStateExit()
        {
            isClimbingDown = false;
        }

        // ------------------------------------------------------------------
        public override void OnStateUpdate()
        {
            if (!isClimbingDown)
            {
                if (!reverseDir)
                    interpolator += Time.deltaTime / (controller._ExerciseTime / 2f);
                else
                    interpolator -= Time.deltaTime / (controller._ExerciseTime / 2f);

                controller.transform.position = PathCreator
                    .Instance.GetPointInCircle(interpolator);

                if (interpolator >= 1f)
                {
                    endReached++;
                    interpolator = 1f;
                    controller.ChangeState(Monkey.States.Idle);
                }
                else if (interpolator <= 0f)
                {
                    endReached++;
                    interpolator = 0f;
                    controller.ChangeState(Monkey.States.Idle);
                }

                if (interpolator <= 0.1f && !numberOfLapsSetted && !reverseDir)
                {
                    numberOfLaps++;
                    numberOfLapsSetted = true;
                }

                if (interpolator >= 0.5f &&
                    numberOfLaps >= controller._ExerciseType.Length)
                {
                    controller._WasTheGameCompleted = true;
                    controller.ChangeState(Monkey.States.Idle);
                }

                if (!angleSetted && endReached != 0 && endReached % 2 == 0 && interpolator >= 0.5f)
                {
                    angleSetted = true;

                    if (numberOfLaps < controller._Grades.Length)
                    {
                        if (TurnCompletedEvent != null)
                            TurnCompletedEvent();

                        SetDesiredAngle(controller._Grades[numberOfLaps].GradesToInt());
                    }
                }
            }

            UpdateRotation();
        }

        // ---------------------------------------------------------------
        private void UpdateRotation()
        {
            if (!isClimbingDown)
            {
                Vector3 dir = controller.transform.position - oldPos;
                dir.y = 0;
                dir.Normalize();

                oldPos = controller.transform.position;
                controller.transform.rotation = Quaternion.LookRotation(dir);
            }
            else
            {
                Vector3 dir = controller._PlayerPos - controller.transform.position;
                dir.y = 0;
                dir.Normalize();

                controller.transform.rotation = Quaternion.LookRotation(dir);
            }
        }

        // ---------------------------------------------------------------------------------
        private IEnumerator MoveToDownPosition(Vector3 targetPos)
        {
            float timer = 0f;

            Vector3 initialPos = controller.transform.position;

            while (timer <= 1f)
            {
                timer += Time.deltaTime / (controller._ExerciseTime / 8f);
                controller.transform.position = Vector3.Lerp(initialPos, targetPos, timer);

                yield return null;
            }

            timer = 0f;

            while (timer <= 1f)
            {
                timer += Time.deltaTime / (controller._ExerciseTime / 8f);
                controller.transform.position = Vector3.Lerp(targetPos, initialPos, timer);

                yield return null;
            }

            isClimbingDown = false;
        }

        // -----------------------------------------------
        private void SetDesiredAngle(int angle)
        {
            controller.PathFollower.DesiredAngle = angle;
        }
    }

}