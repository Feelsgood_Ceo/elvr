﻿using FI.TAGS;
using UnityEngine;

namespace FI.AI.MONKEY
{

    public class PosingState : FiniteStateMachine
    {
        private Monkey controller;

        private Vector3 dir;
        private float interpolator;
        private Quaternion initialRot;

        // ----------------------------------------------
        public PosingState(Monkey controller)
        {
            this.controller = controller;
        }

        // ----------------------------------------------
        public override void OnStateEnter()
        {
            controller.SendEventInitiatePhotoMode();

            dir = controller._PlayerPos - controller.transform.position;
            dir.y = 0;
            dir.Normalize();

            initialRot = controller.transform.rotation;

            controller._Animator.SetInteger(MonkeyTagsAnim.pose, controller._RandomPose);
        }

        // ----------------------------------------------
        public override void OnStateExit()
        {
            controller._Animator.SetInteger(MonkeyTagsAnim.pose, -1);
        }

        // ----------------------------------------------------------
        public override void OnStateUpdate()
        {
            interpolator += Time.deltaTime;

            if (interpolator <= 1f)
            {
                controller.transform.rotation =
                    Quaternion.Lerp(initialRot, 
                    Quaternion.LookRotation(dir), interpolator * 5f);
            }

            if (!controller._IMonkeyGameMode._IstheGameActive)
            {
                controller._TargetPos = controller
                    .PathFollower.GetPointInCircle(0.5f);
                controller.ChangeState(Monkey.States.Jumping);
            }
        }
    }

}