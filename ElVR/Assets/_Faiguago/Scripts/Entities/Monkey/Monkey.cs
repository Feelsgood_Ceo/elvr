﻿using FI.GM.MONKEY;
using FI.TOOLS;
using System;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;
using Newtonsoft.Json;

namespace FI.AI.MONKEY
{

    public class Monkey : LivingEntity, IEntity
    {

        #region VARIABLES

        /// <summary>
        /// Laps Text
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI lapsText;
        public TextMeshProUGUI _LapsText
        {
            get
            {
                return lapsText;
            }
        }

        /// <summary>
        /// Reference to the game mode
        /// </summary>
        [SerializeField]
        private MonkeyGameMode gameMode;
        public IEntityGameMode _IMonkeyGameMode;

        /// <summary>
        /// Exercise Type
        /// </summary>
        public ExerciseType[] _ExerciseType { get; set; }

        /// <summary>
        /// Reference to the monkey tracker
        /// </summary>
        [SerializeField]
        private EntityTracker monkeyTracker;
        public IEntityTracker _IMonkeyTracker { get; private set; }

        /// <summary>
        /// Speed of the monkey
        /// </summary>
        [Header("Monkey characteristics")]
        [SerializeField]
        private float speed;
        public float _Speed
        {
            get
            {
                return speed;
            }
        }

        /// <summary>
        /// Exercise Time
        /// </summary>
        public float _ExerciseTime { get; set; }

        /// <summary>
        /// Height of the jump
        /// </summary>
        [SerializeField]
        private float jumpHeight;
        public float _JumpHeight
        {
            get
            {
                return jumpHeight;
            }
        }

        /// <summary>
        /// Current Index Exercise Type
        /// </summary>
        public int _CurrentIndexExerciseType
        {
            get; set;
        }

        /// <summary>
        /// Current Spline Index
        /// </summary>
        public int _CurrentSplineIndex
        {
            get; set;
        }

        /// <summary>
        /// Exercise grades
        /// </summary>
        public Grades[] _Grades { get; set; }

        /// <summary>
        /// States of the monkey
        /// </summary>
        private FiniteStateMachine initialState, climbingState,
            runningState, idleState, posingState, jumpingState,
            climbingDown;

        /// <summary>
        /// Waypoints
        /// </summary>
        [SerializeField]
        private Transform waypoint;
        public Vector3 _WaypointPos
        {
            get
            {
                return waypoint.position;
            }
        }

        /// <summary>
        /// Target position to run
        /// </summary>
        public Vector3 _TargetPos { get; set; }

        /// <summary>
        /// Reference to the motion component
        /// </summary>
        private MonkeyMotionComponent monkeyMotionComponent;
        public MonkeyMotionComponent _MonkeyMotionComponent
        {
            get
            {
                return monkeyMotionComponent;
            }
        }

        /// <summary>
        /// Save the states
        /// </summary>
        public States _CurrentState { get; private set; }
        public States _PreviousState { get; private set; }

        /// <summary>
        /// Choose a random pose
        /// </summary>
        private int randomPose = -1;
        public int _RandomPose
        {
            get
            {
                if (randomPose != -1)
                    return randomPose;
                else
                {
                    randomPose = Random.Range(0, 6);
                    return randomPose;
                }
            }
        }

        /// <summary>
        /// Was The Game Completed
        /// </summary>
        public bool _WasTheGameCompleted { get; set; }

        [SerializeField]
        private PathCreator pathFollower;
        public PathCreator PathFollower { get { return pathFollower; } }

        #endregion

        #region EVENTS

        /// <summary>
        /// Initiate Photo Mode Event
        /// </summary>
        public event EventHandler InitiatePhotoModeEvent;

        #endregion

        #region UNITY_API

        /// <summary>
        /// Default start method
        /// </summary>
        protected override void Start()
        {
            GetReferences();

            base.Start();
        }

        /// <summary>
        /// Default update method
        /// </summary>
        protected override void Update()
        {
            base.Update();
        }

        #endregion

        #region GENERAL_METHODS

        /// <summary>
        /// Get References
        /// </summary>
        private void GetReferences()
        {
            _IMonkeyGameMode = gameMode;
            _IMonkeyTracker = monkeyTracker;
        }

        /// <summary>
        /// Initialize states
        /// </summary>
        protected override void InitiliazeStates()
        {
            // Components
            monkeyMotionComponent = new MonkeyMotionComponent(this);

            // States
            initialState = new InitialState(this);
            idleState = new IdleState(this);
            runningState = new RunningState(this);
            climbingState = new ClimbingState(this);
            posingState = new PosingState(this);
            jumpingState = new JumpingState(this);
            climbingDown = new ClimbingDownState(this);

            // Set the current state and enter on it
            currentState = initialState;
            currentState.OnStateEnter();

            // Save the current state
            _CurrentState = States.Initial;
        }

        /// <summary>
        /// Change the current monkey's state
        /// </summary>
        public void ChangeState(States state)
        {
            // Exit of the current state
            currentState.OnStateExit();

            // Save the previous state
            _PreviousState = _CurrentState;

            switch (state)
            {
                case States.Idle:
                    currentState = idleState;
                    break;
                case States.Running:
                    currentState = runningState;
                    break;
                case States.Climbing:
                    currentState = climbingState;
                    break;
                case States.Posing:
                    currentState = posingState;
                    break;
                case States.Initial:
                    currentState = initialState;
                    break;
                case States.Jumping:
                    currentState = jumpingState;
                    break;
                case States.ClimbingDown:
                    currentState = climbingDown;
                    break;
            }

            // Save the current state
            _CurrentState = state;

            // Enter to the current state
            currentState.OnStateEnter();
        }
        
        /// <summary>
        /// Send Event Initiate Photo Mode
        /// </summary>
        public void SendEventInitiatePhotoMode()
        {
            if (InitiatePhotoModeEvent != null)
                InitiatePhotoModeEvent(this, null);
        }

        // ---------------------------------------
        public void SetExerciseDatas()
        {
            switch (_Grades[0])
            {
                case Grades.G80:
                    pathFollower.DesiredAngle = 80;
                    break;

                case Grades.G100:
                    pathFollower.DesiredAngle = 100;
                    break;

                case Grades.G120:
                    pathFollower.DesiredAngle = 120;
                    break;

                case Grades.G140:
                    pathFollower.DesiredAngle = 140;
                    break;

                case Grades.G160:
                    pathFollower.DesiredAngle = 160;
                    break;

                case Grades.G180:
                    pathFollower.DesiredAngle = 180;
                    break;

                default:
                    pathFollower.DesiredAngle = 120;
                    break;
            }
        }

        #endregion
    }

    /// <summary>
    /// Interface to the monkey
    /// </summary>
    public interface IEntity
    {
        event EventHandler InitiatePhotoModeEvent;

        int _RandomPose { get; }

        ExerciseType[] _ExerciseType { get; set; }

        Grades[] _Grades { get; set; }

        float _ExerciseTime { get; set; }

        void SetExerciseDatas();
    }

    /// <summary>
    /// Bezier Struct
    /// </summary>
    [Serializable]
    public struct BezierStruct
    {
        [HideInInspector]
        public float duration;
        public BezierSpline spline;
        public bool isClimbingDown;
    }

}