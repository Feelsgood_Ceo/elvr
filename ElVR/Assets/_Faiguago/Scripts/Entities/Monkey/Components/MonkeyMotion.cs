﻿using UnityEngine;

namespace FI.AI.MONKEY
{

    public class MonkeyMotionComponent
    {
        private Monkey controller;

        private float interpolator;

        private Vector3 dir;
        private Vector3 initialPos;
        private Vector3 targetPos;

        private Quaternion initialRot;

        /// <summary>
        /// Time to run
        /// </summary>
        private float timeToRun;

        public enum MovementType
        {
            Run,
            Jump,
            JumpBetweenTrees
        }

        private MovementType movementType;

        // ----------------------------------------------
        public MonkeyMotionComponent(Monkey controller)
        {
            this.controller = controller;
        }

        // ----------------------------------------------
        public void OnStateEnter(MovementType movementType)
        {
            interpolator = 0f;

            initialPos = controller.transform.position;
            targetPos = controller._TargetPos;

            initialRot = controller.transform.rotation;

            float distance = Mathf.Sqrt(Vector3.SqrMagnitude(
                initialPos - targetPos));
            timeToRun = distance / controller._Speed;

            dir = targetPos - initialPos;
            dir.y = 0f;
            dir.Normalize();

            this.movementType = movementType;
        }
        
        // ----------------------------------------------
        public bool OnStateUpdate()
        {
            switch (movementType)
            {
                case MovementType.Run:
                    UpdatePosAndRot();
                    break;
                case MovementType.Jump:
                    Jump();
                    break;
                case MovementType.JumpBetweenTrees:
                    JumpBetweenTrees();
                    break;
            }

            if (interpolator >= 1f)
                return true;
            else
                return false;
        }

        // ----------------------------------------------
        private void UpdatePosAndRot()
        {
            interpolator += Time.deltaTime / timeToRun;

            controller.transform.position =
                Vector3.Lerp(initialPos, targetPos, interpolator);

            controller.transform.rotation =
                Quaternion.Lerp(initialRot,
                Quaternion.LookRotation(dir), interpolator * 5f);
        }

        // ----------------------------------------------
        private void JumpBetweenTrees()
        {
            interpolator += Time.deltaTime;

            float x = (1 - 2 * interpolator);
            float height = 1 - x * x;
            height *= controller._JumpHeight;

            controller.transform.position = Vector3.Lerp(
                initialPos, targetPos, interpolator);

            Vector3 newPos = controller.transform.position;
            newPos.y += height;

            controller.transform.rotation =
                Quaternion.Lerp(initialRot,
                Quaternion.LookRotation(dir), interpolator * 5f);

            controller.transform.position = newPos;
        }

        // ----------------------------------------------
        private void Jump()
        {
            interpolator += Time.deltaTime / 0.5f;

            controller.transform.position = Vector3.Lerp(
                initialPos, targetPos, interpolator);

            controller.transform.rotation =
                Quaternion.Lerp(initialRot,
                Quaternion.LookRotation(dir), interpolator * 5f);
        }

    }

}
