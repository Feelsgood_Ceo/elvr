﻿using UnityEngine;

namespace FI.TAGS
{

    public static class MonkeyTagsAnim
    {

        /// <summary>
        /// Float; value to set the speed
        /// </summary>
        public static int speed = Animator.StringToHash("Speed");
        
        /// <summary>
        /// Int; value to set the pose
        /// </summary>
        public static int pose = Animator.StringToHash("Pose");

        /// <summary>
        /// Trigger; Jumping
        /// </summary>
        public static int setJumping = Animator.StringToHash("SetJumping");

        /// <summary>
        /// Trigger; High Jump
        /// </summary>
        public static int setHighJump = Animator.StringToHash("SetHighJump");

        /// <summary>
        /// Trigger; Climbing
        /// </summary>
        public static int setClimbing = Animator.StringToHash("SetClimbing");

        /// <summary>
        /// Trigger; Idle
        /// </summary>
        public static int setIdle = Animator.StringToHash("SetIdle");

    }

}