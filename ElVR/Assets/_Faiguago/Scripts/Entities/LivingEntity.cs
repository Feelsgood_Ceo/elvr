﻿using FI.AI;
using FI.DS;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FI
{
    /// <summary>
    /// Grades
    /// </summary>
    public enum Grades
    {
        G80,
        G100,
        G120,
        G140,
        G160,
        G180
    }

    /// <summary>
    /// Exercise type
    /// </summary>
    public enum ExerciseType
    {
        Horizontal,
        Vertical,
    }

    [RequireComponent(typeof(NavMeshAgent),
        typeof(Animator))]
    public abstract class LivingEntity : MonoBehaviour
    {
        #region ENUMS

        /// <summary>
        /// Enum states
        /// </summary>
        public enum States
        {
            Initial,
            Idle,
            Climbing,
            Running,
            Posing,
            Jumping,
            ClimbingDown,
            JumpingBetweenTrees
        }

        #endregion

        #region VARIABLES

        /// <summary>
        /// Entity name
        /// </summary>
        [SerializeField]
        private string entityName;
        public string _EntityName
        {
            get
            {
                return entityName;
            }
        }

        /// <summary>
        /// Dialogues
        /// </summary>
        protected Node[] dialogueNodes;
        public Node DialogueNode(int node)
        {
            if (node < dialogueNodes.Length)
                return dialogueNodes[node];
            else
                return null;
        }

        /// <summary>
        /// Index of the current dialog
        /// </summary>
        private int currentDialogIndex;
        public int _CurrentDialogIndex
        {
            get
            {
                return currentDialogIndex;
            }
            set
            {
                if (value < dialogueNodes.Length)
                    currentDialogIndex = value;
            }
        }

        /// <summary>
        /// Current node active
        /// </summary>
        private Node currentNode;
        public Node _CurrentNode
        {
            get
            {
                return dialogueNodes[_CurrentDialogIndex];
            }
        }

        /// <summary>
        /// Speed of the conversation
        /// </summary>
        [SerializeField]
        [Range(0, 10)]
        private float talkSpeed = 5f;
        public float _TalkSpeed
        {
            get
            {
                return talkSpeed / 100f;
            }
        }

        /// <summary>
        /// The current player location
        /// </summary>
        public Vector3 _PlayerPos
        {
            get
            {
                return player.transform.position;
            }
        }

        /// <summary>
        /// Reference to the animator of the header tip
        /// </summary>
        private Animator headerTipAnimator;
        public Animator _HeaderTipAnimator
        {
            get
            {
                return headerTipAnimator;
            }
        }

        /// <summary>
        /// Reference to the agent
        /// </summary>
        private NavMeshAgent agent;
        public NavMeshAgent _Agent
        {
            get
            {
                return agent;
            }
        }

        /// <summary>
        /// Reference to the animator
        /// </summary>
        private Animator animator;
        public Animator _Animator
        {
            get
            {
                return animator;
            }
        }

        /// <summary>
        /// Current state of the entity
        /// </summary>
        protected FiniteStateMachine currentState;

        /// <summary>
        /// Reference to the dialog interface
        /// </summary>
        private IDialogInterface iDialogInt;
        public IDialogInterface _IDialogInt
        {
            get
            {
                return iDialogInt;
            }
        }


        /// <summary>
        /// Reference to the player
        /// </summary>
        [SerializeField]
        [Header("References")]
        protected GameObject player;
        public GameObject _PlayerGO
        {
            get
            {
                return player;
            }
        }

        /// <summary>
        /// The tip when the player is near to the entity
        /// </summary>
        [SerializeField]
        [Tooltip("Can be null")]
        protected GameObject headerTip;

        /// <summary>
        /// Old frame
        /// </summary>
        private int oldFrame;

        /// <summary>
        /// Position of the previous frame
        /// </summary>
        private Vector3 oldPos;

        /// <summary>
        /// Velocity based transform
        /// </summary>
        private float velocity;
        public float _Velocity
        {
            get
            {
                if (oldFrame != Time.frameCount)
                {
                    oldFrame = Time.frameCount;
                    velocity = Mathf.Sqrt(Vector3.SqrMagnitude(
                        transform.position - oldPos));
                    oldPos = transform.position;

                    return velocity;
                }
                else
                    return velocity;
            }
        }

        #endregion

        /// <summary>
        /// Default start method
        /// </summary>
        protected virtual void Start()
        {
            InitiliazeComponents();
            InitializeNodes();
            InitiliazeStates();
        }

        /// <summary>
        /// Default update method
        /// </summary>
        protected virtual void Update()
        {
            currentState.OnStateUpdate();
        }

        /// <summary>
        /// Initialize states
        /// </summary>
        protected abstract void InitiliazeStates();

        /// <summary>
        /// Initialize nodes
        /// </summary>
        protected virtual void InitializeNodes() { }

        /// <summary>
        /// Get references to the components
        /// </summary>
        protected virtual void InitiliazeComponents()
        {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            iDialogInt = player.GetComponent<IDialogInterface>();

            if (headerTip)
                headerTipAnimator = headerTip.GetComponent<Animator>();
        }

    }

}