﻿using FI.DS;
using FI.GM.MONKEY;
using UnityEngine;

namespace FI.AI.TRIS.MG
{

    public class TrisMonkeyGame : LivingEntity
    {
        public enum State
        {
            Talking,
            Empty
        }

        #region DIALOGUE_TUTORIAL

        private const string dt00 = "Puedes ver al mono?";
        private const string dt01 = "Primero solo debes seguirlo con la mirada. Facil no?";
        private const string dt02 = "Luego cuando el mono pose para ti deberas presionar el boton grande para tomarle una foto";

        #endregion

        #region AUDIO_CLIP

        [SerializeField]
        private AudioClip ac00, ac01, ac02;

#endregion

        #region VARIABLES

        [SerializeField]
        private float delay = 3f;

        /// <summary>
        /// State machines
        /// </summary>
        private FiniteStateMachine
            talkingState, emptyState;

        /// <summary>
        /// Reference to the game mode
        /// </summary>
        [SerializeField]
        private MonkeyGameMode gameMode;
        private IEntityGameMode _IMonkeyGameMode;
        
        /// <summary>
        /// Can start the tutorial
        /// </summary>
        private bool canStart;
        
        #endregion

        #region UNITY_API

        // ---------------------------------------------
        protected override void Start()
        {
            _IMonkeyGameMode = gameMode;

            base.Start();

            Invoke("CanStart", delay);
        }

        // ---------------------------------------------
        protected override void Update()
        {
            if (canStart)
                base.Update();
        }

        #endregion

        // ---------------------------------------------
        private void CanStart()
        {
            canStart = true;
        }

        /// <summary>
        /// Initialize the components
        /// </summary>
        protected override void InitiliazeComponents()
        {
            base.InitiliazeComponents();
        }

        // ---------------------------------------------
        protected override void InitializeNodes()
        {
            dialogueNodes = new Node[1];

            dialogueNodes[0] = new Node(
                                    new Dialogue(dt00, null, ac00),
                                    new Node(
                                        new Dialogue(dt01, null, ac01),
                                        new Node(
                                            new Dialogue(dt02, 
                                            delegate {
                                                ChangeState(State.Empty);
                                                StartCoroutine(_IMonkeyGameMode.SetToCanSeeTheEntity(0f));
                                            }, ac02))));
        }

        // ---------------------------------------------
        protected override void InitiliazeStates()
        {
            talkingState = new TalkingState(this);
            emptyState = new EmptyStateMG(this);

            // Setting the current state
            currentState = talkingState;
            currentState.OnStateEnter();
        }

        /// <summary>
        /// Change the current state of Tris
        /// </summary>
        public void ChangeState(State state)
        {
            currentState.OnStateExit();

            switch (state)
            {
                case State.Empty:
                    currentState = emptyState;
                    break;

                case State.Talking:
                    currentState = talkingState;
                    break;
            }

            currentState.OnStateEnter();
        }
    }

}