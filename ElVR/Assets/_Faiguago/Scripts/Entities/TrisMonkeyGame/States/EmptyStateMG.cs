﻿using System.Collections;
using UnityEngine;

namespace FI.AI.TRIS.MG
{

    public class EmptyStateMG : FiniteStateMachine
    {
        private float timer;
        private TrisMonkeyGame controller;

        // ---------------------------------------------
        public EmptyStateMG(TrisMonkeyGame controller)
        {
            this.controller = controller;
        }

        // ---------------------------------------------
        public override void OnStateEnter()
        {
            controller.StartCoroutine(Hide());
        }

        // ---------------------------------------------
        public override void OnStateExit()
        {
        }

        // ---------------------------------------------
        public override void OnStateUpdate() { }

        /// <summary>
        /// Hide Tris
        /// </summary>
        private IEnumerator Hide()
        {
            Vector3 initialPos = controller.transform.localPosition;

            while (timer <= 1f)
            {
                timer += Time.deltaTime;

                controller.transform.localPosition =
                    Vector3.Lerp(initialPos, initialPos +
                    Vector3.left * 2.5f, timer);

                yield return null;
            }
            
            // Disable tris's GO
            controller.gameObject.SetActive(false);
        }
    }

}