﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;

namespace FI.SS
{

    public class TakePhoto : MonoBehaviour, ITakePhoto
    {

        #region VARIABLES

        /// <summary>
        /// Instantiate the photo
        /// </summary>
        [SerializeField]
        private GameObject photoGO;

        /// <summary>
        /// Reference to the photo's animator
        /// </summary>
        private Animator photoAnimator;

        /// <summary>
        /// Reference to the photo's animator
        /// </summary>
        private Material photoMaterial;

        /// <summary>
        /// Command Buffer
        /// </summary>
        private CommandBuffer commandBuffer;

        /// <summary>
        /// Photo's RenderTexture
        /// </summary>
        private RenderTexture photoRT;

        #endregion

        #region UNITY_API

        // ---------------------------------------
        private void Start()
        {
            GetReferences();
            CreateCommandBuffer();
        }

        /// <summary>
        /// Default OnDestroy method
        /// </summary>
        private void OnDestroy()
        {
            photoRT.Release();
        }

        #endregion

        #region GENERAL_METHODS

        /// <summary>
        /// Getting the references to use
        /// </summary>
        private void GetReferences()
        {
            photoAnimator = photoGO.GetComponent<Animator>();
            photoMaterial = photoGO.transform.Find("Photo")
                .GetComponent<Renderer>().sharedMaterial;
        }

        /// <summary>
        /// Create Command Buffer
        /// </summary>
        private void CreateCommandBuffer()
        {
            commandBuffer = new CommandBuffer();

            photoRT = new RenderTexture(
                Camera.main.pixelWidth,
                Camera.main.pixelHeight, 0);
            photoRT.antiAliasing = 4;
            photoRT.filterMode = FilterMode.Trilinear;

            commandBuffer.Blit(
                BuiltinRenderTextureType.CurrentActive,
                new RenderTargetIdentifier(photoRT));
        }

        // -----------------------------------------------
        private void SetTexture(Texture2D texture)
        {
            photoMaterial.SetTexture("_MainTex", texture);
        }

        // --------------------------------------------------------------------------------
        private Texture2D CaptureScreenshot(RenderTexture rt, string fileName, bool save)
        {
            // Set the target to the 
            // current render texture
            Graphics.SetRenderTarget(rt);

            // Read the pixels of the current render texture active
            Texture2D t = new Texture2D(rt.width / 2, rt.height / 2, TextureFormat.RGB24, false);
            t.ReadPixels(new Rect(rt.width / 4, rt.height / 4, t.width, t.height), 0, 0);
            t.Apply();

            // Set to null the current target
            Graphics.SetRenderTarget(null);

            if (save)
                SaveScreenshot(t, fileName);

            return t;
        }

        /// <summary>
        /// Save the screenshot
        /// </summary>
        private void SaveScreenshot(Texture2D t, string fileName)
        {
            // Encode texture into PNG
            byte[] bytes = t.EncodeToPNG();

#if UNITY_ANDROID && !UNITY_EDITOR
            // Save file to the gallery
            NativeGallery.SaveToGallery(bytes, "FeelsGood", fileName + ".png", true);
#elif UNITY_STANDALONE || UNITY_EDITOR
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + @"\Feelsgood";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            FileStream fileSaver;
            fileSaver = new FileStream(path + "\\" + fileName + ".png", FileMode.Create);

            BinaryWriter binary = new BinaryWriter(fileSaver);
            binary.Write(bytes);
            fileSaver.Close();
#endif
        }

        /// <summary>
        /// Show the photo
        /// </summary>
        private void ShowPhoto()
        {
            photoAnimator.SetTrigger("Show");
        }

#endregion

#region INTERFACE

        /// <summary>
        /// Take the photo, save and show it!
        /// </summary>
        public void Take(string photoName, bool save)
        {
            isTakingPhoto = true;

            StartCoroutine(AddCommandBuffer(photoName, save));
        }

#endregion

#region COROUTINES

        /// <summary>
        /// Add Command Buffer
        /// </summary>
        private IEnumerator AddCommandBuffer(string photoName, bool save)
        {
            Camera cam = Camera.main;

            cam.AddCommandBuffer(CameraEvent.AfterSkybox, commandBuffer);

            yield return null;

            ShowPhoto();

            Texture2D temp = CaptureScreenshot(photoRT, photoName, save);

            cam.RemoveAllCommandBuffers();

            SetTexture(temp);

            yield return StartCoroutine(DisableTakingPhoto(temp));
        }

        /// <summary>
        /// Set to false the isTakingPhoto
        /// </summary>
        private IEnumerator DisableTakingPhoto(Texture2D photo)
        {
            yield return new WaitForSeconds(4f);

            isTakingPhoto = false;
            Destroy(photo);
        }

        /// <summary>
        /// Is taking a photo
        /// </summary>
        private bool isTakingPhoto;
        public bool _IsTakingPhoto
        {
            get
            {
                return isTakingPhoto;
            }
        }

#endregion

    }

    /// <summary>
    /// Interface to the ToSaveScreenshot
    /// </summary>
    public interface ITakePhoto
    {
        void Take(string photoName, bool save);

        bool _IsTakingPhoto
        {
            get;
        }
    }

}