﻿using FI.TAGS;
using UnityEngine;
using UnityEngine.Playables;

namespace FI
{

    public class TimelineDelay : MonoBehaviour
    {

        [SerializeField]
        private float delay;
        private PlayableDirector director;

        private bool isPlaying, isLoading;

        // Use this for initialization
        private void Start()
        {
            director = GetComponent<PlayableDirector>();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Time.timeSinceLevelLoad >= delay
                && !isPlaying)
            {
                isPlaying = true;
                director.Play();
            }

            if (!isLoading && director.time >= 42.5f)
            {
                isLoading = true;
                StartCoroutine(Utils.LoadNextScene(ScenesTags.monkeyGame));
            }
        }
    }

}