﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class KeywordsEnabler
{

    private static string debugKey = "DEBUG_LOG_ENABLED";

    static KeywordsEnabler()
    {

    }

    [MenuItem("Keywords/Debug/Enable")]
    private static void EnableDebug()
    {
#if UNITY_STANDALONE
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, debugKey);
#else
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WSA, debugKey);
#endif
    }

    [MenuItem("Keywords/Debug/Enable", true)]
    private static bool EnableDebugValidate()
    {
#if UNITY_STANDALONE
        string temp = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
#else
        string temp = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WSA);
#endif
        return !temp.Contains(debugKey);
    }

    [MenuItem("Keywords/Debug/Disable")]
    private static void DisableDebug()
    {
#if UNITY_STANDALONE
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "");
#else
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WSA, "");
#endif
    }

    [MenuItem("Keywords/Debug/Disable", true)]
    private static bool DisableDebugValidate()
    {
#if UNITY_STANDALONE
        string temp = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
#else
        string temp = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.WSA);
#endif

        return temp.Contains(debugKey);
    }
}
