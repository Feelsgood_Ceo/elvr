﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FI
{

    public class InputDetector : MonoBehaviour, IInputHandler
    {

        public static bool triggerButtonPressed;

        // Use this for initialization
        void Start()
        {
            triggerButtonPressed = false;
            InputManager.Instance.AddGlobalListener(gameObject);
        }

        public void OnInputDown(InputEventData eventData)
        {
            triggerButtonPressed = true;
        }

        public void OnInputUp(InputEventData eventData)
        {
            triggerButtonPressed = false;
        }

        // -------------------------------
        private void OnDestroy()
        {
            triggerButtonPressed = false;
        }
    }

}