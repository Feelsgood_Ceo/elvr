﻿namespace FI.AI
{

    public abstract class FiniteStateMachine
    {
        
        public abstract void OnStateEnter();
        public abstract void OnStateUpdate();
        public abstract void OnStateExit();

    }

}