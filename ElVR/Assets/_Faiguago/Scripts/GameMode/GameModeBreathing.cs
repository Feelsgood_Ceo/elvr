﻿using FI.TAGS;
using FI.TOOLS;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FI.GM
{

    public class GameModeBreathing : MonoBehaviour
    {
        #region PUBLIC_PROPERTIES

        public Transform player;

        public string[] bienvenidaText;

        public Transform[] posiciones;

        public TextMeshProUGUI textUnity, countdownTextUnity;

        public GameObject circle;

        public AudioSource firstMessage,
            secondMessage, soundtrack;

        //Ambient sounds
        public AudioSource 
            audioLago, 
            audioFogata,
            audioFogataP7,
            audioAves,
            audioParque;

        public AudioSource audioInhalaExhala;
        public AudioClip clipInhala, clipExhala;

        public float bellVolume = 0.5f;
        public AudioClip bellClip;

        public float showCircleDuration = 3f;

        /// <summary>
        /// GameObject with all the playlists to reproduce
        /// </summary>
        public Transform playlists;

        /// <summary>
        /// Audiobell playing source
        /// </summary>
        public AudioSource audioBell;

        /// <summary>
        /// Black raw image. Used to fade in effects
        /// </summary>
        public RawImage fadePlane;

        #endregion

        #region PRIVATE_FIELDS

        private Vector3 initialSize;

        private float timeLeft;

        private int actualWelcomeText, alreadyDidItCounter;

        private int actualPos, repeticiones, inhalarTime,
            mantenerTime, exhalarTime, descansarTime;

        private bool gameOver;

        private bool isCountdownActive, inhalaActive,
            esperaActive, exhalaActive, descansaActive;

        private AudioSource audioSource;

        /// <summary>
        /// Playlist containing all the music files to play
        /// </summary>
        private InPlaylist[] breathPlaylist;



        #endregion

        #region UNITY_API

        // Use this for initialization
        private void Start()
        {
            initialSize = circle.transform.localScale;

#if !UNITY_EDITOR
            InitializeExercise();
#else
            ServerData.ReadDataEvent += InitializeExercise;
#endif

            //Initialize Game
            InitializeFadeInPlane();
            breathPlaylist = LoadPlaylists(breathPlaylist, playlists);

            StartCoroutine(StartFirstMessage());
        }

        // ------------------------------------------
        private IEnumerator InitCoroutine()
        {
            yield return new WaitForSeconds(33f);

            StartCoroutine(AnimateShowCircle());

            yield return new WaitForSeconds(11f);

            firstMessage.gameObject.SetActive(false);
            secondMessage.gameObject.SetActive(true);

            yield return new WaitForSeconds(14f);

            yield return StartCoroutine(
                DelayText(3, actualWelcomeText));
        }

        /// <summary>
        /// Animate Show Circle
        /// </summary>
        private IEnumerator AnimateShowCircle()
        {
            float timer = 0f;
            circle.gameObject.SetActive(true);
            circle.transform.localScale = Vector3.zero;

            while (timer <= 1f)
            {
                timer += Time.deltaTime / showCircleDuration;
                circle.transform.localScale = Vector3.Lerp(
                    Vector3.zero, initialSize, timer);

                yield return null;
            }
        }

        // -------------------------------------------------
        private void OnDestroy()
        {
#if UNITY_EDITOR
            ServerData.ReadDataEvent -= InitializeExercise;
#endif
        }

        /// <summary>
        /// Only for testing in editor
        /// </summary>
        private void InitializeExercise()
        {
            SetValuesFromServer();

            timeLeft = inhalarTime;
            
            // Select differents ambient sounds
            ActivateAmbientSound(actualPos);
            
            // Create another audio source for the bell
            InitializeAudioBell();            
        }

        /// <summary>
        /// Set Values From Server
        /// </summary>
        private void SetValuesFromServer()
        {
            actualPos = ServerData.Instance._BreathingExerciseData.Scenario;
            inhalarTime = ServerData.Instance._BreathingExerciseData.Inhalacion;
            mantenerTime = ServerData.Instance._BreathingExerciseData.Mantener;
            exhalarTime = ServerData.Instance._BreathingExerciseData.Exhalacion;
            descansarTime = ServerData.Instance._BreathingExerciseData.Descanso;
            repeticiones = ServerData.Instance._BreathingExerciseData.Repeticiones;

            //Change actual position
            //actualPos = actualPos - 1;

            // Set the current position
            //player.position = posiciones[actualPos].position;
            //player.rotation = posiciones[actualPos].rotation;

            //Setting current position
            SetPlayerInitialPosition(actualPos);
        }

        // Update is called once per frame
        void Update()
        {
            if (isCountdownActive)
            {
                countdownTextUnity.gameObject.SetActive(true);
                Countdown();
            }
        }

        #endregion

        #region ENUMS
        /// <summary>
        /// Enum with the type of sounds that can be played
        /// </summary>
        private enum SoundType
        {
            INHALE,
            EXHALE
        }
        #endregion

        #region AUDIO_RELATED
        /// <summary>
        /// Loads all playlists in object
        /// </summary>
        /// <param name="playlist">Playlist to be filled with music</param>
        /// <param name="playlistGO">Object containing playlist files</param>
        /// <returns>Playlist filled with music files</returns>
        private InPlaylist[] LoadPlaylists(InPlaylist[] playlist, Transform playlistGO)
        {
            int index = playlistGO.childCount;
            playlist = new InPlaylist[index];

            for (int i = 0; i < index; i++)
            {
                playlist[i] = playlists
                    .GetChild(i).GetComponent<InPlaylist>();
            }

            return playlist;
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Initializes audio bell
        /// </summary>
        private void InitializeAudioBell() {
            audioBell.volume = 1.0f;
            audioBell.clip = bellClip;
            audioBell.loop = false;
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays Bell sound
        /// </summary>
        private void PlayBellSound()
        {
            audioBell.Play();
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays the first message 
        /// </summary>
        private IEnumerator StartFirstMessage()
        {
            yield return new WaitForSeconds(20.0f);//4
            breathPlaylist[0].Play();

            yield return new WaitForSeconds(9.0f);//5 y 6
            breathPlaylist[0].Stop();
            breathPlaylist[1].Play();

            yield return new WaitForSeconds(9.0f);
            breathPlaylist[1].Stop();

            yield return new WaitForSeconds(3.0f);//7
            breathPlaylist[2].Play();

            yield return new WaitForSeconds(9.0f);//8 y 9
            breathPlaylist[2].Stop();
            breathPlaylist[3].Play();

            yield return new WaitForSeconds(9.0f);
            breathPlaylist[3].Stop();

            yield return new WaitForSeconds(3.0f);//10
            breathPlaylist[4].Play();

            yield return new WaitForSeconds(7.0f);//11
            breathPlaylist[4].Stop();
            breathPlaylist[5].Play();

            yield return new WaitForSeconds(12.0f);//12
            ShowBubble();
        }
        // ------------------------------------------------------------
   
        private IEnumerator StartBubbleMessage()
        {

            breathPlaylist[6].Play();

            yield return new WaitForSeconds(11.0f);//13 al 17
            breathPlaylist[6].Stop();
            breathPlaylist[7].Play();

            yield return new WaitForSeconds(30.0f);
            StartCoroutine(DelayText(2, actualWelcomeText));

            yield return new WaitForSeconds(8.0f);
            breathPlaylist[7].Stop();
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays an audioclip listed in the playlist
        /// </summary>
        /// <param name="playlist">Origin playlist</param>
        /// <param name="PlaylistIndex">Playlist index</param>
        /// <param name="play">True to play, false to stop</param>
        private void PlayAudioClip(InPlaylist[] playlist, int PlaylistIndex, bool play)
        {
            if (play) playlist[PlaylistIndex].Play();
            else playlist[PlaylistIndex].Stop();
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays an audioclip (inhale motion)
        /// </summary>
        private void PlayInhaleClip()
        {
            switch (alreadyDidItCounter)
            {
                case 1: PlayAudioClip(breathPlaylist, 10, true); break;
                case 2: PlayAudioClip(breathPlaylist, 32, true); break;
                case 3: PlayAudioClip(breathPlaylist, 16, true); break;
                default: RandomizeSound(SoundType.INHALE); break;
            }
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays an audioclip (exhale motion)
        /// </summary>
        private void PlayExhaleClip()
        {
            switch (alreadyDidItCounter)
            {
                case 0: PlayAudioClip(breathPlaylist, 8, true); break;
                case 1: PlayAudioClip(breathPlaylist, 12, true); break;
                case 2: PlayAudioClip(breathPlaylist, 33, true); break;
                case 3: PlayAudioClip(breathPlaylist, 17, true); break;
                default: RandomizeSound(SoundType.EXHALE); break;
            }
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Randomizes a chosen type of sound
        /// </summary>
        /// <param name="type">Typr of sound to be used</param>
        private void RandomizeSound(SoundType type)
        {
            int[] inhaleSounds = new int[7] { 14, 19, 22, 24, 26, 28, 30 };
            int[] exhaleSounds = new int[7] { 15, 18, 20, 25, 27, 29, 31 };

            int result = 0;

            switch (type)
            {
                case SoundType.INHALE:
                    if (alreadyDidItCounter == repeticiones - 2 && alreadyDidItCounter > 0)
                    {
                        PlayAudioClip(breathPlaylist, 36, true);
                    }
                    else if (alreadyDidItCounter < repeticiones - 2 && alreadyDidItCounter > 6)
                    {
                        result = Random.Range(0, inhaleSounds.Length);
                        PlayAudioClip(breathPlaylist, inhaleSounds[result], true);
                    }
                    break;

                case SoundType.EXHALE:
                    if (alreadyDidItCounter == repeticiones - 3)
                    {
                        PlayAudioClip(breathPlaylist, 37, true);
                    }
                    else if (alreadyDidItCounter == repeticiones - 2)
                    {
                        PlayAudioClip(breathPlaylist, 38, true);
                    }
                    else if (alreadyDidItCounter == repeticiones - 1)
                    {
                        PlayAudioClip(breathPlaylist, 39, true);
                    }
                    else if (alreadyDidItCounter < repeticiones - 3 && alreadyDidItCounter > 6)
                    {
                        result = Random.Range(0, exhaleSounds.Length);
                        PlayAudioClip(breathPlaylist, exhaleSounds[result], true);
                    }
                    break;
            }
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Plays a audioclip (rest motion)
        /// </summary>
        private void PlayRestClip()
        {
            if (alreadyDidItCounter == repeticiones - 2)
            {
                PlayAudioClip(breathPlaylist, 35, true);
            }
            else if (alreadyDidItCounter == repeticiones - 1)
            {
                PlayAudioClip(breathPlaylist, 40, true);
            }
        }
        // ------------------------------------------------------------
        #endregion

        #region AMBIENT_SOUNDS

        /// <summary>
        /// Activates AudioSource with ambient sound
        /// </summary>
        /// <param name="actualPosition">Current player position</param>
        private void ActivateAmbientSound(int actualPosition) {

            Debug.Log("My position is: " + actualPosition);

            switch (actualPosition) {
                case 0:
                    audioAves.gameObject.SetActive(true);
                    break;

                case 1:
                    audioFogata.gameObject.SetActive(true);
                    break;

                case 2:
                    audioAves.gameObject.SetActive(true);
                    audioLago.gameObject.SetActive(true);
                    break;

                case 3:
                    audioLago.gameObject.SetActive(true);
                    break;

                case 4:
                    audioParque.gameObject.SetActive(true);
                    audioLago.gameObject.SetActive(true);
                    break;

                case 5:
                    audioAves.gameObject.SetActive(true);
                    audioLago.gameObject.SetActive(true);
                    break;

                case 6:
                    audioFogataP7.gameObject.SetActive(true);
                    break;

                case 7:
                    audioParque.gameObject.SetActive(true);
                    break;

                case 8:
                    audioLago.gameObject.SetActive(true);
                    break;

                case 9:
                    audioLago.gameObject.SetActive(true);
                    break;

                case 10:
                    audioAves.gameObject.SetActive(true);
                    break;

                case 11:
                    audioLago.gameObject.SetActive(true);
                    audioAves.gameObject.SetActive(true);
                    break;
            }

            /*if (actualPos == 1 || actualPos == 2 || actualPos == 3 || actualPos == 5)
            {
                audioLago.gameObject.SetActive(true);
            }
            if (actualPos == 2)
            {
                audioFogata.gameObject.SetActive(true);
            }*/
        }
        #endregion

        #region TESTING
        private void ForceActualPosition(int position) {
            actualPos = position;
            ActivateAmbientSound(actualPos);
        }
        #endregion

        #region VISUAL_EFFECTS
        /// <summary>
        /// Fades in a plane inside the camera at the end of the exercise
        /// </summary>
        private void FadeInPlane() {
            fadePlane.CrossFadeAlpha(1.0f, 5.0f, true);
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Shows bubble
        /// </summary>
        private void ShowBubble()
        {
            StartCoroutine(AnimateShowCircle());
            StartCoroutine(StartBubbleMessage());
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Activates the plane to fade in at the camera
        /// </summary>
        private void ActivateFadeInPlane() {
            fadePlane.canvasRenderer.SetAlpha(0.0f);
            fadePlane.gameObject.SetActive(true);

            FadeInPlane();
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Starts FadeIn animation
        /// </summary>
        private void InitializeFadeInPlane() {
            fadePlane.canvasRenderer.SetAlpha(1.0f);
            fadePlane.gameObject.SetActive(false);
        }
        // ------------------------------------------------------------

        /// <summary>
        /// Deactivates breathing bubble
        /// </summary>
        private void DectivateBreathingBubble() {
            circle.SetActive(false);
        }
        #endregion

        /// <summary>
        /// Sets player initial position
        /// </summary>
        /// <param name="serverPosition">Scenario ID from server</param>
        void SetPlayerInitialPosition(int serverPosition) {

            int newPosition = 0; //If value is not defined, position will always be 0
            bool isValueDefined = System.Enum.IsDefined(typeof(ScenarioPoints), serverPosition);

            if (isValueDefined)
            {
                //Getting Enum Name value
                string scenarioName = System.Enum.GetName(typeof(ScenarioPoints), serverPosition);

                //Creating a enum array
                var scenarioPoints = System.Enum.GetValues(typeof(ScenarioPoints));

                //Checking index value
                for (int i = 0; i < scenarioPoints.Length; i++)
                {
                    if (scenarioPoints.GetValue(i).ToString() == scenarioName)
                    {
                        newPosition = i;
                        break;
                    }
                }
            }

            //Setting final positions
            player.position = posiciones[newPosition].position;
            player.rotation = posiciones[newPosition].rotation;
        }

        // ------------------------------------------------------------
        private IEnumerator DelayText(int timeForDelay, int indexText)
        {
            yield return new WaitForSeconds(timeForDelay);

            textUnity.text = bienvenidaText[indexText];
            yield return new WaitForSeconds(timeForDelay);

            textUnity.text = bienvenidaText[indexText + 1];
            yield return new WaitForSeconds(timeForDelay);

            textUnity.text = bienvenidaText[indexText + 2];
            yield return new WaitForSeconds(timeForDelay);

            //soundtrack.gameObject.SetActive(false);
            StartBreathing();
        }

        // ----------------------------------------------------------------------
        private void Countdown()
        {
            timeLeft -= Time.deltaTime;
            countdownTextUnity.text = timeLeft.ToString("N0");

            if (inhalaActive)
            {
                circle.transform.localScale = Vector3.Lerp(initialSize,
                    initialSize * 1.5f, 1 - timeLeft / inhalarTime);
            }
            if (esperaActive)
            {

            }
            if (exhalaActive)
            {
                circle.transform.localScale = Vector3.Lerp(initialSize * 1.5f,
                    initialSize, 1 - timeLeft / exhalarTime);
            }
            if (descansaActive)
            {

            }
            if (timeLeft <= 0)
            {
                if (inhalaActive)
                {
                    ClearText(mantenerTime, "SOSTEN LA\nRESPIRACION");

                    inhalaActive = false;
                    esperaActive = true;

                }
                else if (esperaActive)
                {
                    ClearText(exhalarTime, "EXHALA");
                    esperaActive = false;
                    exhalaActive = true;
                    audioInhalaExhala.Stop();
                    audioInhalaExhala.clip = clipExhala;
                    audioInhalaExhala.volume = 0.5f;
                    audioInhalaExhala.Play();

                    //Play SFX
                    PlayExhaleClip();
                }
                else if (exhalaActive)
                {
                    ClearText(descansarTime, "DESCANSA");
                    exhalaActive = false;
                    descansaActive = true;

                    PlayRestClip();

                }
                else if (descansaActive)
                {
                    ClearText(1, "");
                    descansaActive = false;
                    isCountdownActive = false;

                    Reset();
                }
            }
        }

        // --------------------------------------------------------
        private void ClearText(float nextCountdown, string message)
        {
            timeLeft = nextCountdown;
            textUnity.text = message;
        }

        // --------------------------------------------------
        private void Reset()
        {
            alreadyDidItCounter++;
            countdownTextUnity.text = "";

            if (alreadyDidItCounter < repeticiones)
            {
                StartBreathing();
            }
            else
            {
                //circle.SetActive(false);
                ActivateFadeInPlane();

                ServerData.Instance.SendExerciseComplete();

                Invoke("DectivateBreathingBubble", 5f);
                Invoke("BreathingFinished", 6.0f);
            }
        }

        /// <summary>
        /// Simulation Finished
        /// </summary>
        private void BreathingFinished()
        {
            gameOver = true;

            GameMode_VisualScale.finalScene = true;

            if (ServerData.Instance.GetIsVisualScaleActive())
                StartCoroutine(Utils.LoadNextScene(ScenesTags.visualScale));
            else {

            #if !UNITY_EDITOR
                Application.Quit();
            #else
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
            }
        }

        /// <summary>
        /// Start Breathing
        /// </summary>
        private void StartBreathing()
        {
            inhalaActive = true;
            isCountdownActive = true;
            textUnity.text = "INHALA";

            timeLeft = inhalarTime;

            audioInhalaExhala.Stop();
            audioInhalaExhala.clip = clipInhala;
            audioInhalaExhala.volume = 1.0f;
            audioInhalaExhala.Play();

            PlayBellSound();
            PlayInhaleClip();
        }
    }

}