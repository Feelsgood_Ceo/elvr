﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FI.GM
{

    public abstract class GameMode : MonoBehaviour
    {
        /// <summary>
        /// Default Awake
        /// </summary>
        protected virtual void Awake()
        {
            GetReferences();
            //SceneManager.sceneLoaded += GameSceneLoaded;
        }

        /// <summary>
        /// Load the exercise datas
        /// </summary>
        protected abstract void GameSceneLoaded(
            Scene arg0, LoadSceneMode arg1);

        /// <summary>
        /// Default OnDestroy
        /// </summary>
        protected virtual void OnDestroy()
        {
            //SceneManager.sceneLoaded -= GameSceneLoaded;
        }

        /// <summary>
        /// Get References
        /// </summary>
        protected abstract void GetReferences();

        /// <summary>
        /// Get References
        /// </summary>
        protected abstract void FinishTheGame(string status);
    }

}