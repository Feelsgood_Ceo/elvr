﻿#define DEBUG_TEMP

using System.Collections;
using UnityEngine;
using FI.AI.TRIS.MG;
using FI.TAGS;
using System;
using FI.DS;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using FI.AI.MONKEY;
using FI.TOOLS;
using TMPro;

namespace FI.GM.MONKEY
{

    [RequireComponent(typeof(EntityTracker))]
    public class MonkeyGameMode : GameMode, IEntityGameMode
    {

        #region VARIABLES

        /// <summary>
        /// Developer Tools
        /// </summary>
        [Header("Developer Tools")]
        [SerializeField]
        private bool developerMode;
        [SerializeField]
        private bool forceGame;

        /// <summary>
        /// Reference to the monkey
        /// </summary>
        [SerializeField]
        private Monkey monkey;
        private IEntity _IMonkey { get; set; }

        /// <summary>
        /// Reference to the coin's transform
        /// displayed in the screen
        /// </summary>
        [SerializeField]
        private Transform coinInSreenT;
        private Vector3 _CoinInScreenPos
        {
            get
            {
                return coinInSreenT.position;
            }
        }

        /// <summary>
        /// Coin's animator
        /// </summary>
        private Animator coinAnimator;

        /// <summary>
        /// Reference to Tris
        /// </summary>
        [Header("Game mode references")]
        [SerializeField]
        private TrisMonkeyGame trisMG;

        /// <summary>
        /// World Space Canvas
        /// </summary>
        [SerializeField]
        private GameObject worldSpaceCanvas;

        /// <summary>
        /// Coin prefab GO
        /// </summary>
        [SerializeField]
        private GameObject coinPrefabGO;

        /// <summary>
        /// Indicator
        /// </summary>
        [SerializeField]
        private GameObject arrowGO;

        /// <summary>
        /// Coin prefab GO
        /// </summary>
        [SerializeField]
        private GameObject uiEffectGameCompletedGO;

        /// <summary>
        /// Monkey's position
        /// </summary>
        private Vector3 _MonkeyPos
        {
            get
            {
                return monkey.transform.position;
            }
        }

        /// <summary>
        /// Player transform
        /// </summary>
        [SerializeField]
        private Transform playerT;
        private Vector3 _PlayerPos
        {
            get
            {
                return playerT.position;
            }
        }

        /// <summary>
        /// Coin's amount
        /// </summary>
        private int coins;

        /// <summary>
        /// Reference to the AudioSource created
        /// </summary>
        private AudioSource audioSource, soundEffectSource;

        /// <summary>
        /// Reference to the dialog interface
        /// </summary>
        private IDialogInterface iDialogInt;

        /// <summary>
        /// Is the tutorial?
        /// </summary>
        private bool isTutorial;
        public bool _IsTutorial
        {
            get
            {
                return isTutorial;
            }
        }

        /// <summary>
        /// The Game Was Played Previously?
        /// </summary>
        private bool gameWasPlayedPreviously;
        public bool _GameWasPlayedPreviously
        {
            get
            {
                return gameWasPlayedPreviously;
            }
        }

        /// <summary>
        /// Reference to the monkey tracker component
        /// </summary>
        private IEntityTracker iMonkeyTracker;
        private IEntityTracker _IMonkeyTracker
        {
            get
            {
                if (iMonkeyTracker != null)
                    return iMonkeyTracker;
                else
                {
                    iMonkeyTracker = GetComponent<IEntityTracker>();
                    return iMonkeyTracker;
                }
            }
        }

        /// <summary>
        /// Active Take Photo Messages
        /// </summary>
        private string[] activeTakePhotoMessages;

        /// <summary>
        /// Active Take Photo Audios
        /// </summary>
        private AudioClip[] activeTakePhotoAudios;

        /// <summary>
        /// Reference to the coroutine show and animate text
        /// </summary>
        private Coroutine coroutineShowAndAnimateText;

        #endregion

        #region AUDIO_CLIPS

        /// <summary>
        /// Estas lista, a comenzar!
        /// </summary>
        [Header("Estas lista?")]
        [SerializeField]
        private AudioClip estasListaClip;

        /// <summary>
        /// Muy Bien
        /// </summary>
        [Header("Muy bien!")]
        [SerializeField]
        private AudioClip muyBienClip;

        /// <summary>
        /// Sigue así, perfecto, lo estas
        /// haciendo muy bien! clips
        /// </summary>
        [Header("Mensajes cada 4 monedas")]
        [SerializeField]
        private AudioClip[] fourCoinsClips;

        /// <summary>
        /// Tienes que seguir al mono!
        /// Debes mirar al mono!
        /// </summary>
        [Header("Mensajes de Advertencia")]
        [SerializeField]
        private AudioClip[] warningMessages;

        /// <summary>
        /// Ahora! Presiona el boton grande
        /// del mando! Asi le tomaras
        /// </summary>
        [Header("Tomar foto por primera vez")]
        [SerializeField]
        private AudioClip[] firstTimePhotoAudios;

        /// <summary>
        /// Esta posando, tomale la foto
        /// </summary>
        [Header("Tomar foto por segunda vez")]
        [SerializeField]
        private AudioClip[] secondTimePhotoAudios;

        /// <summary>
        /// Uy ya se movio!
        /// Ese animalito no se queda mucho
        /// </summary>
        [Header("La foto no fue tomada")]
        [SerializeField]
        private AudioClip photoWasNotTakenAudio;

        /// <summary>
        /// Mejor descansemos
        /// </summary>
        [Header("Perdio al mono de vista")]
        [SerializeField]
        private AudioClip twelveSecondsAudio;

        /// <summary>
        /// Muy bien!
        /// Lo conseguiste!
        /// Muchas gracias por todo eres .........
        /// </summary>
        [Header("Primera vez que complete el juego")]
        [SerializeField]
        private AudioClip firtTimeFinalAudio;

        /// <summary>
        /// Te pasaste!
        /// Muchas gracias, te quedaron super .......
        /// </summary>
        [Header("Segunda vez que complete el juego")]
        [SerializeField]
        private AudioClip secondTimeFinalAudio;

        /// <summary>
        /// Tienes que primero mirar al monito
        /// </summary>
        [Header("La foto fue tomada incorrectamente")]
        [SerializeField]
        private AudioClip photoWasTakenIncorrectlyAudio;

        [SerializeField]
        private AudioClip coinSound;

        [SerializeField]
        private ParticleSystem coinParticles;

        #endregion

        #region MESSAGES

        /// <summary>
        /// Twelve seconds message
        /// </summary>
        private const string twelveSecondsMessage = "Mejor descansemos un poco, " +
            "no te preocupes en otro momento me ayudas! " +
            "Puedes venir cuando quieras, aqui te estare esperando";

        /// <summary>
        /// First Take Photo Messages
        /// </summary>
        private static string[] firstTakePhotoMessages = {
            "Ahora!\nPresiona el boton grande del mando! Asi le tomaras una foto.",
            "Esta posando, tomale la foto!",
            "Vamos presiona el boton!"
        };

        /// <summary>
        /// Photo Not Taken
        /// </summary>
        private static string photoWasNotTakenMessage = "Uy! Ya se movio.\n" +
            "Ese animalito no se queda mucho tiempo quieto.";

        /// <summary>
        /// Photo Was Taken INCORRECTLY
        /// </summary>
        private static string photoWasTakenIncorrectlyMessage = "Tienes que primero mirar al" +
            " monito y luego tomar la foto.";

        /// <summary>
        /// Second Take Photo Messages
        /// </summary>
        private static string[] secondTakePhotoMessages = {
            "Esta posando, tomale la foto!",
            "Vamos presiona el boton!",
            "Tomale ahora!",
            "Fotografialo!",
            "Rapido tomale!"
        };

        /// <summary>
        /// The first time that the game was completed
        /// </summary>
        private static string firstTimeGameCompletedMessage = "Muy bien!\nLo conseguiste!\n" +
            "Muchas gracias por todo eres la mas linda del mundo!\n" +
            "Podras ver las fotos que has coleccionado en el album de " +
            "fotos que se encuentra en tu cabana.";

        /// <summary>
        /// The second time that the game was completed
        /// </summary>
        private static string secondTimeGameCompletedMessage = "Te pasaste!\nMuchas gracias, " +
            "te quedaron super lindas las fotos! Regresa pronto!";

        #endregion

        #region CONDITIONS

        /// <summary>
        /// Is the game active?
        /// </summary>
        private bool isTheGameActive;
        public bool _IstheGameActive { get { return isTheGameActive; } }

        private bool canSeeTheEntity;
        public bool CanSeeTheEntity { get { return canSeeTheEntity; } }

        /// <summary>
        /// The game is completed?
        /// </summary>
        private bool gameCompleted;

        /// <summary>
        /// The game was successfully completed?
        /// </summary>
        private bool gameWasSuccessfullyCompleted;

        #endregion

        #region UNITY_API

        #region EVENTS

        public static event Action<string> MonkeyGameFinishedEvent;

        #endregion

        /// <summary>
        /// Default start method
        /// </summary>
        private void Start()
        {
            InitializeGame();
            InitializeCallbacks();
            HideSceneElements();

            InitialState.EntityWasSeen += EntityWasSeen;
        }

        // -----------------------------------------------
        protected override void OnDestroy()
        {
            base.OnDestroy();

            InitialState.EntityWasSeen -= EntityWasSeen;
        }

        // -------------------------
        private void EntityWasSeen()
        {
            isTheGameActive = true;
            arrowGO.SetActive(false);
        }

        /// <summary>
        /// Default update method
        /// </summary>
        private void Update()
        {
            // Load again if an input is detected
            if (gameCompleted && (Input.GetButtonDown("Fire1")
                || InputDetector.triggerButtonPressed))
            {
                Destroy(FindObjectOfType<InputDetector>().gameObject);
            }
        }

        #endregion

        #region CALLBACKS

        /// <summary>
        /// Callback if the photo was not taken
        /// </summary>
        private void PhotoWasNotTaken(object sender, EventArgs args)
        {
            StopAudioAndTextAnimations();

            coroutineShowAndAnimateText = StartCoroutine(
                AnimateTextAndPlayClip(photoWasNotTakenMessage, photoWasNotTakenAudio));

            FinishTheGame("Incomplete");
        }

        /// <summary>
        /// Notify if the photo was taken correctly
        /// </summary>
        private void PhotoWasTakenCorrectly(object sender, EventArgs args)
        {
            StopAudioAndTextAnimations();

            if (gameWasPlayedPreviously)
            {
                coroutineShowAndAnimateText = StartCoroutine(
                    AnimateTextAndPlayClip(firstTimeGameCompletedMessage, firtTimeFinalAudio));
            }
            else
            {
                coroutineShowAndAnimateText = StartCoroutine(
                    AnimateTextAndPlayClip(secondTimeGameCompletedMessage, secondTimeFinalAudio));
            }

            gameWasSuccessfullyCompleted = true;

            FinishTheGame("Complete");
        }

        /// <summary>
        /// Notify if the photo was taken INCORRECTLY
        /// </summary>
        private void PhotoWasTakenIncorrectly(object sender, EventArgs e)
        {
            StopAudioAndTextAnimations();

            coroutineShowAndAnimateText = StartCoroutine(
                AnimateTextAndPlayClip(photoWasTakenIncorrectlyMessage, photoWasTakenIncorrectlyAudio));

            Invoke("DelayToCallInitiatePhotoMode", 5f);
        }

        /// <summary>
        /// Delay To Call Photo Mode Initiated
        /// </summary>
        private void DelayToCallInitiatePhotoMode()
        {
            InitiatePhotoMode();
        }

        /// <summary>
        /// Initiate The Photo Mode
        /// </summary>
        private void InitiatePhotoMode(object sender = null, EventArgs args = null)
        {
            InvokeRepeating("MessagesToTakeThePhoto", 0f, 3f);
        }

        #endregion

        #region GENERAL_METHODS

        /// <summary>
        /// Stop Text Animations
        /// </summary>
        private void StopAudioAndTextAnimations()
        {
            CancelInvoke();

            if (coroutineShowAndAnimateText != null)
                StopCoroutine(coroutineShowAndAnimateText);

            // Clean audio 
            // source and text
            StopAudio();
            iDialogInt.CleanText();
            iDialogInt.HidePanel();
        }

        /// <summary>
        /// Initialize Callbacks
        /// </summary>
        private void InitializeCallbacks()
        {
            // Add the methods to trigger the events
            _IMonkeyTracker.FourSecondsEvent +=
                delegate { FourSecondsTrigger(); };
            _IMonkeyTracker.TwelveSecondsEvent +=
                delegate { TwelveSecondsTrigger(); };
            _IMonkeyTracker.CoinWonEvent += CoinWon;

            // Photo's events
            _IMonkeyTracker.PhotoWasNotTaken += PhotoWasNotTaken;
            _IMonkeyTracker.PhotoWasTakenCorrectly += PhotoWasTakenCorrectly;
            _IMonkeyTracker.PhotoWasTakenIncorrectly += PhotoWasTakenIncorrectly;

            // Monkey's callbacks
            _IMonkey.InitiatePhotoModeEvent += InitiatePhotoMode;
        }

        /// <summary>
        /// Initialize the game or the tutorial
        /// </summary>
        private void InitializeGame()
        {
#if !UNITY_EDITOR
            developerMode = false;
#endif

            // Load the player prefs
            isTutorial = PlayerPrefs.GetInt(
                PlayerPrefsTags.monkeyGameMode, 0) == 0;
            gameWasPlayedPreviously = PlayerPrefs.GetInt(
                PlayerPrefsTags.monkeyGameWasPlayed, 0) == 1;

            // Set the current messages at the moment to take the photos
            if (gameWasPlayedPreviously)
            {
                activeTakePhotoMessages = secondTakePhotoMessages;
                activeTakePhotoAudios = secondTimePhotoAudios;
            }
            else
            {
                activeTakePhotoMessages = firstTakePhotoMessages;
                activeTakePhotoAudios = firstTimePhotoAudios;
            }

            // Developer mode
            if (developerMode)
            {
                if (forceGame)
                    StartGame();
                else
                    StartTutorial();

                return;
            }

            // Start the game or the tutorial
            if (isTutorial)
                StartTutorial();
            else
                StartGame();
        }

        /// <summary>
        /// Get the references
        /// </summary>
        protected override void GetReferences()
        {
            // Getting the dialog interface
            iDialogInt = playerT.GetComponent<IDialogInterface>();

            // Create the audio source
            audioSource = gameObject.AddComponent<AudioSource>();
            soundEffectSource = gameObject.AddComponent<AudioSource>();

            // Get the animator of the coin
            coinAnimator = coinInSreenT.GetComponent<Animator>();

            // Get the monkey's interface reference
            _IMonkey = monkey;
        }

        /// <summary>
        /// Start the tutorial
        /// </summary>
        private void StartTutorial()
        {
            trisMG.gameObject.SetActive(true);
        }

        /// <summary>
        /// Directly start the game
        /// </summary>
        private void StartGame()
        {
            // ?????????????????????????
            StopAudioAndTextAnimations();

            // Active the monkey's behavior
            coroutineShowAndAnimateText = StartCoroutine(
                AnimateTextAndPlayClip("Estas lista?\nComienza!", estasListaClip));

            // Delay to activate the monkey behavior
            StartCoroutine(SetToCanSeeTheEntity(4f));
        }

        // -------------------------------------------------------------
        protected override void FinishTheGame(string status)
        {
            isTheGameActive = false;
            worldSpaceCanvas.SetActive(false);

            PlayerPrefs.SetInt(PlayerPrefsTags.monkeyGameMode, 1);
            PlayerPrefs.SetInt(PlayerPrefsTags.monkeyGameWasPlayed, 1);

            if (MonkeyGameFinishedEvent != null)
                MonkeyGameFinishedEvent(status);

            Invoke("ShowFinalUIAnimation", 5f);
            Invoke("DelayToSetGameIsOver", 10f);
        }

        /// <summary>
        /// Show Final UI Animation
        /// </summary>
        private void ShowFinalUIAnimation()
        {
            if (gameWasSuccessfullyCompleted)
                uiEffectGameCompletedGO.SetActive(true);
        }

        /// <summary>
        /// Delay To Set The Game Is Over
        /// </summary>
        private void DelayToSetGameIsOver()
        {
            gameCompleted = true;
            StopAudioAndTextAnimations();

            GameMode_VisualScale.finalScene = true;

            if (ServerData.Instance.GetIsVisualScaleActive())
                StartCoroutine(Utils.LoadNextScene(ScenesTags.visualScale));
            else
            {
            #if !UNITY_EDITOR
                Application.Quit();
            #else
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
            }
        }

        // -----------------------------------------------
        private void FourSecondsTrigger()
        {
            // ?????????????????????????
            StopAudioAndTextAnimations();

            int index = Random.Range(0, warningMessages.Length);

            coroutineShowAndAnimateText = StartCoroutine(AnimateTextAndPlayClip(
                warningMessages[index].name, warningMessages[index]));
        }

        // -----------------------------------------------
        private void TwelveSecondsTrigger()
        {
            // ?????????????????????????
            StopAudioAndTextAnimations();

            coroutineShowAndAnimateText = StartCoroutine(
                AnimateTextAndPlayClip(twelveSecondsMessage, twelveSecondsAudio));

            FinishTheGame("Incomplete");
        }

        /// <summary>
        /// Instantiate the coin
        /// </summary>
        private void CoinWon(TextMeshProUGUI scoreText, string scoreString)
        {
            coins++;
            CheckCoinsAmount();

            GameObject temp = Instantiate(coinPrefabGO,
                _MonkeyPos, Quaternion.identity);

            StartCoroutine(AnimateCoin(temp.transform, scoreText, scoreString));
        }

        /// <summary>
        /// Check the coin's amount
        /// and play the respective clip
        /// </summary>
        private void CheckCoinsAmount()
        {
            // ?????????????????????????
            StopAudioAndTextAnimations();

            if (coins == 1)
            {
                coroutineShowAndAnimateText = StartCoroutine(AnimateTextAndPlayClip(
                    muyBienClip.name, muyBienClip));
            }
            if (coins % 4 == 0)
            {
                int index = Random.Range(0, fourCoinsClips.Length);

                coroutineShowAndAnimateText = StartCoroutine(AnimateTextAndPlayClip(
                    fourCoinsClips[index].name, fourCoinsClips[index]));
            }
        }

        #region COROUTINES

        [SerializeField]
        private AudioClip miraAlMonitoClip;

        // ------------------------------------------------------------
        public IEnumerator SetToCanSeeTheEntity(float delay)
        {
            yield return new WaitForSeconds(delay);

            GameSceneLoaded(new Scene(), 0);

            canSeeTheEntity = true;
            worldSpaceCanvas.SetActive(true);

            if (miraAlMonitoClip)
            {
                AudioSource temp = gameObject.AddComponent<AudioSource>();
                temp.clip = miraAlMonitoClip;
                temp.loop = false;
                temp.Play();
            }
        }

        /// <summary>
        /// Show and animate the message
        /// </summary>
        private IEnumerator AnimateTextAndPlayClip(string text, AudioClip clip)
        {
            // Play the current clip 
            // before the text animation
            PlayClip(clip);

            yield return null;

            //// Clean text and show the panel
            //iDialogInt.CleanText();
            //iDialogInt.ShowPanel();

            //StringBuilder builder = new StringBuilder();

            //// Wait until the message shows
            //yield return new WaitForSeconds(1f);

            //for (int i = 0; i < text.Length; i++)
            //{
            //    builder.Append(text[i]);

            //    if (!text[i].Equals('\\'))
            //        iDialogInt.SetDialogText(builder);

            //    yield return new WaitForSeconds(0.05f);
            //}

            //yield return new WaitForSeconds(1f);

            //iDialogInt.HidePanel();
        }

        /// <summary>
        /// Animate the coin instantiated
        /// </summary>
        private IEnumerator AnimateCoin(Transform coinT, TextMeshProUGUI scoreText, string scoreString)
        {
            float percentage = 0f;
            Vector3 initialPos = coinT.position;

            while (percentage <= 1)
            {
                percentage += Time.deltaTime / 1f;

                coinT.position = Vector3.Lerp(initialPos,
                    _CoinInScreenPos, percentage);
                coinT.Rotate(0f, Time.deltaTime * 250f, 0f);

                yield return null;
            }

            coinParticles.Play();
            PlaySoundEffect(coinSound);
            coinAnimator.SetTrigger("Resize");

            // Set the new coin score
            scoreText.text = scoreString;

            Destroy(coinT.gameObject);
        }

        /// <summary>
        /// Play effects sounds
        /// </summary>
        private void PlaySoundEffect(AudioClip clip)
        {
            soundEffectSource.clip = clip;
            soundEffectSource.Play();
        }

        /// <summary>
        /// First Messages To Take The Photo
        /// </summary>
        private void MessagesToTakeThePhoto()
        {
            // ?????????????????????????
            StopAudioAndTextAnimations();

            int index = Random.Range(0, activeTakePhotoMessages.Length);

            coroutineShowAndAnimateText = StartCoroutine(AnimateTextAndPlayClip(
                activeTakePhotoMessages[index], activeTakePhotoAudios[index]));
        }

        #endregion

        /// <summary>
        /// Play a clip
        /// </summary>
        private void PlayClip(AudioClip clip)
        {
            if (clip)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        /// <summary>
        /// Stop audio source
        /// </summary>
        private void StopAudio()
        {
            audioSource.Stop();
        }

        /// <summary>
        /// Load the datas
        /// </summary>
        protected override void GameSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            if (ServerData.Instance._AreValid)
            {
                if (ServerData.Instance._HeadExerciseData.IsProcedurallyGenerated)
                    SetExercisesRandomly();
                else
                    SetExercises(ServerData.Instance._HeadExerciseData.TipoMovimiento);

                _IMonkey._ExerciseTime = ServerData.Instance._HeadExerciseData.TiempoEjercicio;
                _IMonkey._Grades = ServerData.Instance._HeadExerciseData.Grados;

                Debug.Log("Data correctly setted!");
            }
            else
            {
                SetExercises(new int[] { 1 });

                _IMonkey._Grades = new Grades[] { Grades.G120 };
                _IMonkey._ExerciseTime = 5f;

                Debug.LogError("Data not availables!");
            }

            _IMonkey.SetExerciseDatas();
        }

        /// <summary>
        /// Set Exercises
        /// </summary>
        private void SetExercises(int[] movType)
        {
            ExerciseType[] exType = new ExerciseType[movType.Length];

            for (int i = 0; i < movType.Length; i++)
            {
                exType[i] = (ExerciseType)movType[i];
            }

            _IMonkey._ExerciseType = exType;
        }

        /// <summary>
        /// Set Exercises Randomly
        /// </summary>
        private void SetExercisesRandomly()
        {
            int amount = 5;
            ExerciseType[] exType = new ExerciseType[amount];

            for (int i = 0; i < amount; i++)
            {
                exType[i] = (ExerciseType)Random.Range(0, 2);
            }

            _IMonkey._ExerciseType = exType;
        }

        #endregion

        /// <summary>
        /// Hide scene elements
        /// </summary>
        private void HideSceneElements() {
            GameObject[] sceneElements;
            sceneElements = GameObject.FindGameObjectsWithTag("monkeyScene");

            foreach (GameObject element in sceneElements) {
                element.SetActive(false);
            }
        }

    }

    /// <summary>
    /// Interface to communicate 
    /// any entity with the game mode
    /// </summary>
    public interface IEntityGameMode
    {
        bool _IsTutorial { get; }

        bool _GameWasPlayedPreviously { get; }

        bool _IstheGameActive { get; }

        bool CanSeeTheEntity { get; }

        IEnumerator SetToCanSeeTheEntity(float delay);
    }

}