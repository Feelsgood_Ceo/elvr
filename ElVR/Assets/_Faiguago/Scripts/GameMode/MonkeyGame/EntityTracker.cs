﻿using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;
using FI.AI.MONKEY;
using FI.SS;
using FI.TAGS;

namespace FI.GM.MONKEY
{

    public class EntityTracker : MonoBehaviour, IEntityTracker
    {

        #region VARIABLES

        /// <summary>
        /// Reference to the crosshair
        /// </summary>
        [SerializeField]
        private Image crosshair;

        /// <summary>
        /// Reference to the timer
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI
            timerText, scoreText;

        /// <summary>
        /// Timer In Photo Mode
        /// </summary>
        private float timerInPhotoMode;

        /// <summary>
        /// Reference to the main camera
        /// </summary>
        [SerializeField]
        private Transform playerT;

        /// <summary>
        /// Time without seeing the monkey
        /// </summary>
        private float invisibleMonkeyTimer;

        /// <summary>
        /// Reference to the monkey
        /// </summary>
        [SerializeField]
        private Monkey monkey;
        private IEntity iMonkey;
        private IEntity _IMonkey
        {
            get
            {
                if (iMonkey != null)
                    return iMonkey;
                else
                {
                    iMonkey = monkey;
                    return iMonkey;
                }
            }
        }

        /// <summary>
        /// Reference to the photo saver
        /// </summary>
        [Header("WS_UI")]
        [SerializeField]
        private TakePhoto takePhoto;
        private ITakePhoto _ITakePhoto;

        /// <summary>
        /// Reference to the IMonkeyGameMode interface
        /// </summary>
        private IEntityGameMode _IMonkeyGameMode;

        /// <summary>
        /// Score of the game
        /// </summary>
        private float score;

        /// <summary>
        /// The photo was taken
        /// </summary>
        private bool thePhotoWasTaken;

        #endregion

        #region EVENTS

        /// <summary>
        /// Lockers
        /// </summary>
        private bool fourSecondsEventTriggered,
            eightSecondsEventTriggered,
            twelveSecondsEventTriggered;

        /// <summary>
        /// Event to repeat every four seconds
        /// </summary>
        public event EventHandler FourSecondsEvent;

        /// <summary>
        /// Twelve seconds event
        /// </summary>
        public event EventHandler TwelveSecondsEvent;

        /// <summary>
        /// Coin Won Event
        /// </summary>
        public event Action<TextMeshProUGUI, string> CoinWonEvent;

        /// <summary>
        /// Photo Was Not Taken
        /// </summary>
        public event EventHandler PhotoWasNotTaken;

        /// <summary>
        /// Photo Was Taken Correctly
        /// </summary>
        public event EventHandler PhotoWasTakenCorrectly;

        /// <summary>
        /// Photo Was Taken Incorrectly
        /// </summary>
        public event EventHandler PhotoWasTakenIncorrectly;

        /// <summary>
        /// Notify if the monkey was seen
        /// </summary>
        private bool monkeyWasSeen;
        public bool _EntityWasSeen
        {
            get
            {
                return monkeyWasSeen;
            }
        }

        /// <summary>
        /// Is In Photo Mode?
        /// </summary>
        private bool isInPhotoMode;

        /// <summary>
        /// Was The Max Time In Photo Mode Reached?
        /// </summary>
        private bool maxTimeInPhotoModeReached;

        #endregion

        #region UNITY_API

        /// <summary>
        /// Default start method
        /// </summary>
        private void Start()
        {
            GetReferences();
            SetCallbacks();
        }

        // Update is called once per frame
        void Update()
        {
            // Always update the state of the monkey!
            UpdateMonkeyWasSeen();

            if (_IMonkeyGameMode._IstheGameActive
                && !isInPhotoMode)
            {
                UpdateTimer();
                UpdateEvents();
                UpdateScore();
            }

            // Update the timer if the photo hasn't been taken yet
            if (isInPhotoMode && !maxTimeInPhotoModeReached
                && !thePhotoWasTaken && !_ITakePhoto._IsTakingPhoto
                && monkeyWasSeen)
            {
                TakePhoto();
            }
        }

        #endregion

        #region CALLBACKS

        /// <summary>
        /// Reset the monkey timer
        /// </summary>
        private void PhotoModeInitiated(object sender, EventArgs e)
        {
            CleanTimerText();
            isInPhotoMode = true;
        }

        #endregion

        #region GENERAL_METHODS

        /// <summary>
        /// Get References
        /// </summary>
        private void GetReferences()
        {
            _ITakePhoto = takePhoto;
            _IMonkeyGameMode = GetComponent<IEntityGameMode>();
        }

        /// <summary>
        /// Set Callbacks
        /// </summary>
        private void SetCallbacks()
        {
            _IMonkey.InitiatePhotoModeEvent += PhotoModeInitiated;
        }

        /// <summary>
        /// Reset the monkey timer
        /// </summary>
        private void ResetMonkeyTimer()
        {
            invisibleMonkeyTimer = 0f;

            // Reset the lockers
            fourSecondsEventTriggered = false;
            eightSecondsEventTriggered = false;
            twelveSecondsEventTriggered = false;
        }

        // ---------------------------------------------------------------------------
        private void TakePhoto()
        {
            // Send the notifications
            thePhotoWasTaken = true;

            if (PhotoWasTakenCorrectly != null)
                PhotoWasTakenCorrectly(this, null);

            _ITakePhoto.Take(TagsPhotos.monkeysPose[_IMonkey._RandomPose], true);

            ResetReticle();  
        }

        /// <summary>
        /// Raycast to the monkey
        /// </summary>
        private void UpdateMonkeyWasSeen()
        {
            RaycastHit hit;
            Ray ray = new Ray(playerT.position, playerT.forward);
            Debug.DrawRay(ray.origin, ray.direction * 100f);

            if (Physics.Raycast(ray, out hit, 100f,
                LayerMask.GetMask("Monkey")))
            {
                // If the monkey was seen
                monkeyWasSeen = true;

                // Change the crosshair color to green
                //crosshair.color = new Color(0f, 1f, 0f, 1f);
                ActivateReticleAnimation(true);
            }
            else
            {
                // If the monkey 
                // was not seen
                monkeyWasSeen = false;

                // Change the crosshair color to red
                //crosshair.color = new Color(1f, 0f, 0f, 1f);
                ActivateReticleAnimation(false);
            }
        }

        /// <summary>
        /// Update score and send 
        /// the notification
        /// </summary>
        private void UpdateScore()
        {
            if (monkeyWasSeen)
            {
                score += 20f;

                if (score % 1000 == 0)
                {
                    if (CoinWonEvent != null)
                        CoinWonEvent(scoreText, (score / 1000f).ToString("N0"));
                }
            }
        }

        /// <summary>
        /// Update the timer
        /// </summary>
        private void UpdateTimer()
        {
            if (monkeyWasSeen)
                ResetMonkeyTimer();
            else
                invisibleMonkeyTimer += Time.deltaTime;

            // Update the timer text
            timerText.SetText(
                (12f - invisibleMonkeyTimer).ToString("N1") + " s");
        }

        /// <summary>
        /// Update the events to send
        /// </summary>
        private void UpdateEvents()
        {
            // Check the four seconds event
            if (invisibleMonkeyTimer >= 4f
                && !fourSecondsEventTriggered)
            {
                FourSecondsTrigger();
            }

            // Check the eight seconds event
            if (invisibleMonkeyTimer >= 8f
                && !eightSecondsEventTriggered)
            {
                EightSecondsTrigger();
            }

            // Check the twelve seconds event
            if (invisibleMonkeyTimer >= 12f
                && !twelveSecondsEventTriggered)
            {
                TwelveSecondsTrigger();
            }
        }

        // ----------------------------------------
        private void FourSecondsTrigger()
        {
            fourSecondsEventTriggered = true;

            if (FourSecondsEvent != null)
                FourSecondsEvent(this, null);
        }

        // ----------------------------------------
        private void EightSecondsTrigger()
        {
            eightSecondsEventTriggered = true;

            if (FourSecondsEvent != null)
                FourSecondsEvent(this, null);
        }

        // ----------------------------------------
        private void TwelveSecondsTrigger()
        {
            twelveSecondsEventTriggered = true;

            if (TwelveSecondsEvent != null)
                TwelveSecondsEvent(this, null);

            CleanTimerText();
        }

        /// <summary>
        /// Clean Timer Text
        /// </summary>
        private void CleanTimerText()
        {
            // Clean the timer text
            timerText.SetText("");
        }

        #endregion

        /// <summary>
        /// Sends a message to activate an animation
        /// </summary>
        /// <param name="increase">True to increase, false otherwise</param>
        private void ActivateReticleAnimation(bool increase)
        {
            this.GetComponent<ReticleAnimations>().IncreaseReticleSize(increase);
        }

        /// <summary>
        /// Resets reticle
        /// </summary>
        private void ResetReticle() {
            this.GetComponent<ReticleAnimations>().ResetReticle();
        }
    }

    /// <summary>
    /// Interface to the MonkeyTracker
    /// </summary>
    public interface IEntityTracker
    {
        bool _EntityWasSeen { get; }

        event EventHandler FourSecondsEvent;

        event EventHandler TwelveSecondsEvent;

        event Action<TextMeshProUGUI, string> CoinWonEvent;

        event EventHandler PhotoWasNotTaken;

        event EventHandler PhotoWasTakenCorrectly;

        event EventHandler PhotoWasTakenIncorrectly;
    }

}