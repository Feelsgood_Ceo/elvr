﻿using FI.TOOLS;
using FI.TAGS;
using UnityEngine;

namespace FI.GM
{

    public class GameMode_VisualScale : MonoBehaviour
    {
        [SerializeField]
        private GameObject emotionsGO, painGO,
            painQuestionGO, painScaleGO, finalMessageGO;

        private int pain, state, painScale;

        public static bool finalScene;

        // Use this for initialization
        private void Start()
        {
            Emotions.EmotionSelectedEvent += EmotionSelectedEvent;
            YesNoSelection.YesNoSelectedEvent += YesNoSelectedEvent;
            PainScale.PainSelectedEvent += PainSelectedEvent;
        }

        // ---------------------------------------
        private void YesNoSelectedEvent(int id)
        {
            if (id == 1)
            {
                pain = 1;
                painScaleGO.SetActive(true);
                painQuestionGO.SetActive(false);
            }
            else
            {
                painGO.SetActive(false);
                finalMessageGO.SetActive(true);

                Invoke("LoadNextScene", 5f);
            }
        }

        // --------------------------------------
        private void PainSelectedEvent(int id)
        {
            painScale = id;
            painGO.SetActive(false);
            finalMessageGO.SetActive(true);

            Invoke("LoadNextScene", 5f);
        }

        // --------------------------------------
        private void EmotionSelectedEvent(int id)
        {
            state = id;
            emotionsGO.SetActive(false);
            painGO.SetActive(true);
        }

        // ---------------------------------------------------------
        private void OnDestroy()
        {
            finalScene = true;

            Emotions.EmotionSelectedEvent -= EmotionSelectedEvent;
            YesNoSelection.YesNoSelectedEvent -= YesNoSelectedEvent;
            PainScale.PainSelectedEvent -= PainSelectedEvent;
        }

        // -------------------------------------------------------------------------------
        private void LoadNextScene()
        {
            if (!finalScene)
            {
                ServerData.Instance.SetInitialStateAndPain(pain, state, painScale);
                StartCoroutine(Utils.LoadNextScene(ServerData.Instance.GetSceneToLoad()));
            }
            else
            {
                ServerData.Instance.SetFinalStateAndPain(pain, state, painScale);
                StartCoroutine(Utils.LoadNextScene(ScenesTags.splashScreen));
            }
        }
    }

}