﻿using FI.TAGS;
using FI.TOOLS;
using HoloToolkit.UI.Keyboard;
using HoloToolkit.Unity.InputModule;
using SimpleJSON;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FI
{

    public class Authentication : MonoBehaviour
    {

        [SerializeField, Range(3, 30)]
        private int secondsToWait = 3;

        [SerializeField]
        private KeyboardInputField inputField;

        private const string dni = "06574215";

        [SerializeField]
        private GameObject loadingPanel, wrongDNI;

        [SerializeField]
        private Text loadingText;

        private static string[] loadingString = new string[] { "Cargando datos del paciente",
        "Cargando datos del paciente.", "Cargando datos del paciente..", "Cargando datos del paciente..." };

        private bool isRunning;

        [SerializeField]
        private GameObject[] toDestroy;

        private int textIndex;

        private AsyncOperation ao;

        [SerializeField]
        private VRButton enterButton, clearButton;

        // ----------------------------------------------------
        void Start()
        {
            isRunning = false;

            enterButton.ButtonClicked += OnEnterButtonClicked;
            clearButton.ButtonClicked += OnClearButtonClicked;
        }

        // --------------------------------
        private void OnClearButtonClicked()
        {
            inputField.text = "";
        }

        // --------------------------------
        private void OnEnterButtonClicked()
        {
            if (!isRunning)
            {
                isRunning = true;

                StartCoroutine(LoginPatient(inputField.text));
            }
        }

        // --------------------------------------------------
        private IEnumerator LoadNextScene(string sceneToLoad)
        {
            ao = SceneManager.LoadSceneAsync(sceneToLoad);
            ao.allowSceneActivation = false;

            yield return ao;
        }

        // ------------------------------------------------------------------------
        private IEnumerator LoginPatient(string dni)
        {
            WWWForm form = new WWWForm();

            form.AddField("dni", dni);

            WWW www = new WWW("http://feelsgood.care/neuralvr/api/loginDNI", form);

            yield return www;

            if (www.error == null)
            {
                if (!www.text.Contains("Datos incorrectos"))
                {
                    JSONNode json = JSON.Parse(www.text);
                    StartCoroutine(HandleAuthentication(json));
                }
                else
                {
                    wrongDNI.SetActive(true);
                    Invoke("DisableWrongDNIText", 3);
                }
            }
            else
            {
                isRunning = false;
                Debug.LogError("Service error at login");
            }
        }

        // -------------------------------
        private void DisableWrongDNIText()
        {
            isRunning = false;
            wrongDNI.SetActive(false);
        }

        // ----------------------------------------------------------
        private IEnumerator HandleAuthentication(JSONNode json)
        {
            loadingPanel.SetActive(true);

            string patientId = json["id_paciente"];
            ServerData.Instance.FastQuery(patientId);

            yield return null;

            while (ServerData.Instance.RunningFastQuery)
            {
                ShowLoadingText();
                yield return new WaitForSecondsRealtime(0.5f);
            }

            if (ServerData.Instance.ValidExercise != 0)
            {
                ServerData.Instance.ReadData(patientId,
                    ServerData.Instance.ValidExercise.ToString());

                while (!ServerData.Instance.ReadDataCompleted)
                {
                    ShowLoadingText();
                    yield return new WaitForSecondsRealtime(0.5f);
                }

                loadingText.text = string.Format("Ejercicio de {0} {1} cargado correctamente!",
                    json["nombre"].ToString().ToUpper(), json["apellido"].ToString().ToUpper());

                yield return new WaitForSeconds(3f);

                // Start simulation
                StartCoroutine(Countdown());

                // Load the next scene
                string sceneToLoad = string.Empty;

                if (ServerData.Instance.GetIsTutorialActive())
                    sceneToLoad = ScenesTags.tutorial;
                else
                    sceneToLoad = ServerData.Instance.GetSceneToLoad();

                StartCoroutine(LoadNextScene(sceneToLoad));
            }
            else
            {
                loadingText.text = string.Format("El paciente {0} {1} no dispone de ejercicios asignados, " +
                    "dosifique un ejercicio desde la plataforma web.",
                    json["nombre"].ToString().ToUpper(), json["apellido"].ToString().ToUpper());

                yield return new WaitForSeconds(3f);

                loadingPanel.SetActive(false);
                isRunning = false;
            }
        }

        /// <summary>
        /// Show Loading Text
        /// </summary>
        private void ShowLoadingText()
        {
            loadingText.text = loadingString[textIndex];
            textIndex = (textIndex + 1) % loadingString.Length;
        }

        // ----------------------------------------------------------------------
        private IEnumerator Countdown()
        {
            int remainingTime = secondsToWait;

            while (remainingTime > 0)
            {
                loadingText.text = "Cargando en " + remainingTime + " segundos";
                remainingTime--;

                yield return new WaitForSeconds(1);
            }

            for (int i = 0; i < toDestroy.Length; i++)
            {
                Destroy(toDestroy[i]);
            }

            ao.allowSceneActivation = true;
        }
    }

}