﻿using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FI
{

    public class VRButton : MonoBehaviour, IInputClickHandler
    {
        public event Action ButtonClicked;

        // -------------
        void Start() { }

        public virtual void OnInputClicked(InputClickedEventData eventData)
        {
            if (ButtonClicked != null)
                ButtonClicked();
        }

    }

}