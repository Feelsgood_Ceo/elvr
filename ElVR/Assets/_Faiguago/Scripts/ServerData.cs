﻿//#define TEST_MODE

using System.Collections;
using UnityEngine;
using SimpleJSON;
using System;
using UnityEngine.SceneManagement;
using FI.TAGS;
using FI.Rep;
using TMPro;
using File = System.IO.File;

namespace FI.TOOLS
{

    public class ServerData : MonoBehaviour
    {
        #region CONSTANTS

        private const string breathingExerciseId = "1";
        private const string headExerciseId = "8";

        #endregion

        #region PUBLIC_PROPERTIES

        /// <summary>
        /// Only for testing purposes
        /// </summary>
        public string patientInEditor;

        /// <summary>
        /// Only for testing purposes
        /// </summary>
        public string exerciseInEditor;

        /// <summary>
        /// Are the datas valids?
        /// </summary>
        public bool _AreValid { get; private set; }

        #endregion

        #region PRIVATE_VARIABLES

        private string currentPatientId, currentExerciseId;

        private string urlGetExercise = "http://feelsgood.care/neuralvr/api/getEjercicio";
        private string urlSaveEvolution = "http://feelsgood.care/neuralvr/api/guardaEvolucion";

        #endregion

        #region EXERCISES_DATAS

        public HeadExerciseDataRaw _HeadExerciseDataRaw { get; private set; }
        public HeadExerciseData _HeadExerciseData { get; private set; }

        public BreathingExerciseDataRaw _BreathingExerciseDataRaw { get; private set; }
        public BreathingExerciseData _BreathingExerciseData { get; private set; }

        #endregion

        #region PUBLIC_VARIABLES

        /// <summary>
        /// Singleton
        /// </summary>
        private static ServerData instance;
        public static ServerData Instance
        {
            get
            {
                return instance;
            }
        }

        public int ValidExercise { get; private set; }

        public bool RunningFastQuery { get; private set; }
        public bool ReadDataCompleted { get; private set; }

        #endregion

        public static event Action ReadDataEvent;

        #region UNITY_API

        /// <summary>
        /// Default Awake
        /// </summary>
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Intro Audio Message
        /// </summary>
        public AudioSource introMessage;

        // Use this for initialization
        private IEnumerator Start()
        {
#if !UNITY_EDITOR || TEST_MODE
            string path = Application.persistentDataPath;
            int index = path.IndexOf("/AppData");
            path = path.Substring(0, index)
                + "/Pictures/Feelsgood/session.txt";

            string text = File.ReadAllText(path);
            File.Delete(path);

            string[] values = text.Split(',');

            switch (values[1])
            {
                case headExerciseId:
                    break;

                case breathingExerciseId:
                    yield return new WaitForSeconds(5f);
                    introMessage.gameObject.SetActive(true);
                    yield return new WaitForSeconds(7f);
                    break;

                default:
                    break;
            }

            ReadData(values[0], values[1]);
            yield return null;
#else
            ReadData(patientInEditor, exerciseInEditor);
            yield return null;
#endif
        }

        #endregion

        #region QUERIES

        /// <summary>
        /// Fast query to get the first active exercise
        /// </summary>
        public void FastQuery(string patientId)
        {
            ValidExercise = 0;
            RunningFastQuery = true;
            StartCoroutine(FastQueryCoroutine(patientId));
        }

        /// <summary>
        /// Fast query coroutine
        /// </summary>
        private IEnumerator FastQueryCoroutine(string patientId)
        {
            for (int i = 1; i <= 8; i++)
            {
                WWWForm phoneData = new WWWForm();

                // Fill the form
                phoneData.AddField("id_paciente", patientId);
                phoneData.AddField("id_ejercicio", i.ToString());

                WWW www = new WWW(urlGetExercise, phoneData);

                yield return www;

                if (www.error == null && !www.text.Equals("{\"status\":false}"))
                {
                    ValidExercise = i;
                    i = 100;
                }
                else if (www.error != null)
                {
                    ValidExercise = -1;
                    i = 100;
                }
            }

            RunningFastQuery = false;

            // Print in console
            switch (ValidExercise)
            {
                case 0:
                    Debug.LogError("WITHOUT EXERCISE!");
                    break;

                case 1:
                    Debug.Log("Breathing exercise!");
                    break;

                case 2:
                    Debug.Log("");
                    break;

                case 3:
                    Debug.Log("");
                    break;

                case 4:
                    Debug.Log("");
                    break;

                case 5:
                    Debug.Log("");
                    break;

                case 6:
                    Debug.Log("");
                    break;

                case 7:
                    Debug.Log("");
                    break;

                case 8:
                    Debug.Log("Neck exercise!");
                    break;

                default:
                    Debug.LogError("NO INTERNET CONNECTION!");
                    break;
            }
        }

        /// <summary>
        /// Read Data
        /// </summary>
        public void ReadData(string patientId, string exerciseId)
        {
            _AreValid = false;
            ReadDataCompleted = false;

            WWWForm phoneData = new WWWForm();

            // Set the current ids
            currentPatientId = patientId;
            currentExerciseId = exerciseId;

            // Fill the form
            phoneData.AddField("id_paciente", currentPatientId);
            phoneData.AddField("id_ejercicio", currentExerciseId);

            WWW www = new WWW(urlGetExercise, phoneData);

            StartCoroutine(ReadData(www, currentExerciseId));
        }

        /// <summary>
        /// Coroutine to read the data
        /// </summary>
        private IEnumerator ReadData(WWW www, string id)
        {
            yield return www;

            if (www.error == null)
            {
                Debug.Log(www.text);

                try
                {
                    JSONNode jsonParsed = JSON.Parse(www.text);

                    ReadExercise(id, jsonParsed);

                    _AreValid = true;

                    ReadDataCompleted = true;

#if !UNITY_EDITOR || TEST_MODE
                    Invoke("LoadNextScene", 3f);
#else
                    if (ReadDataEvent != null)
                    {
                        ReadDataEvent();
                    }
#endif
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);

                    ReadDataCompleted = true;
                }
            }
            else
            {
                Debug.LogWarning(www.error);

                ReadDataCompleted = true;
            }
        }

        #endregion

        #region DATAS

        [Serializable]
        private struct StateAndPain
        {
            public int pain;
            public int state;
            public int painScale;

            public StateAndPain(int pain, int state, int painScale)
            {
                this.pain = pain;
                this.state = state;
                this.painScale = painScale;
            }
        }

        private StateAndPain initialState, finalState;

        // -------------------------------------
        public void SetInitialStateAndPain(
            int pain, int state, int painScale)
        {
            initialState = new StateAndPain(
                pain, state, painScale);
        }

        // -------------------------------------
        public void SetFinalStateAndPain(
            int pain, int state, int painScale)
        {
            finalState = new StateAndPain(
                pain, state, painScale);
        }

        /// <summary>
        /// Get Scene To Load
        /// </summary>
        public string GetSceneToLoad()
        {
            string sceneToLoad = string.Empty;
           
            switch (currentExerciseId)
            {
                case headExerciseId:
                    sceneToLoad = ScenesTags.intro;
                    break;

                case breathingExerciseId:
                    sceneToLoad = ScenesTags.breathingScene;
                    break;

                default:
                    sceneToLoad = ScenesTags.intro;
                    break;
            }

            return sceneToLoad;
        }

        /// <summary>
        /// Get Is Tutorial Active
        /// </summary>
        public bool GetIsTutorialActive()
        {
            bool isTutorialActive = false;

            switch (currentExerciseId)
            {
                case headExerciseId:
                    isTutorialActive = _HeadExerciseData.IsTutorial;
                    break;

                case breathingExerciseId:
                    isTutorialActive = _BreathingExerciseData.IsTutorial;
                    break;

                default:
                    isTutorialActive = false;
                    break;
            }

            return isTutorialActive;
        }

        /// <summary>
        /// Get Is Tutorial Active
        /// </summary>
        public bool GetIsVisualScaleActive()
        {
            bool isVisualScaleActive = false;

            switch (currentExerciseId)
            {
                case headExerciseId:
                    isVisualScaleActive = _HeadExerciseData.DogActive;
                    break;

                case breathingExerciseId:
                    isVisualScaleActive = _BreathingExerciseData.DogActive;
                    break;

                default:
                    isVisualScaleActive = false;
                    break;
            }

            return isVisualScaleActive;
        }

        // ----------------------------------------------
        private string GetExerciseId()
        {
            string id;

            switch (SceneManager.GetActiveScene().name)
            {
                case ScenesTags.monkeyGame:
                    id = headExerciseId;
                    break;
                case ScenesTags.breathingScene:
                    id = breathingExerciseId;
                    break;
                default:
                    id = headExerciseId;
                    break;
            }

            return id;
        }

        /// <summary>
        /// Load Next Scene
        /// </summary>
        private void LoadNextScene()
        {
            // Load the next scene
            string sceneToLoad = string.Empty;

            if (GetIsTutorialActive())
                sceneToLoad = ScenesTags.tutorial;
            else if (GetIsVisualScaleActive())
                sceneToLoad = ScenesTags.visualScale;
            else
                sceneToLoad = GetSceneToLoad();

            StartCoroutine(Utils.LoadNextScene(sceneToLoad));
        }

        #endregion

        // ------------------------------------------------------
        private void ReadExercise(string id, JSONNode jsonParsed)
        {
            switch (id)
            {
                case breathingExerciseId:
                    ReadBreathingExercise(jsonParsed);
                    break;

                case headExerciseId:
                    ReadHeadExercise(jsonParsed);
                    break;

                default:
                    ReadHeadExercise(jsonParsed);
                    break;
            }
        }

        // -----------------------------------------------------------------------------------
        private void ReadBreathingExercise(JSONNode jsonParsed)
        {
            _BreathingExerciseDataRaw = new BreathingExerciseDataRaw();

            _BreathingExerciseDataRaw.id_ejercicio = jsonParsed["id_ejercicio"];
            _BreathingExerciseDataRaw.id_escenario = jsonParsed["id_escenario"];
            _BreathingExerciseDataRaw.id_paciente = jsonParsed["id_paciente"];
            _BreathingExerciseDataRaw.momento = jsonParsed["momento"];
            _BreathingExerciseDataRaw.repeticiones = jsonParsed["repeticiones"];
            _BreathingExerciseDataRaw.inhalacion = jsonParsed["inhalacion"];
            _BreathingExerciseDataRaw.mantener = jsonParsed["mantener"];
            _BreathingExerciseDataRaw.exhalacion = jsonParsed["exhalacion"];
            _BreathingExerciseDataRaw.descanso = jsonParsed["descanso"];
            _BreathingExerciseDataRaw.fecha = jsonParsed["fecha"];
            _BreathingExerciseDataRaw.status = jsonParsed["status"];
            _BreathingExerciseDataRaw.tutorial = jsonParsed["tutorial"];
            _BreathingExerciseDataRaw.perrito = jsonParsed["perrito"];

            ParseBreathingVariables();
        }

        // --------------------------------------
        private void ParseBreathingVariables()
        {
            _BreathingExerciseData = new BreathingExerciseData();

            int scenario = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.id_escenario, out scenario))
            {
                _BreathingExerciseData.Scenario = scenario;
            }
            else
            {
                _BreathingExerciseData.Scenario = 1;
            }

            int rawTime = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.id_escenario, out rawTime))
            {
                _BreathingExerciseData.DayTime = (TimeOfDay)rawTime;
            }
            else
            {
                _BreathingExerciseData.DayTime = TimeOfDay.Morning;
            }

            int repeticiones = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.repeticiones, out repeticiones))
            {
                _BreathingExerciseData.Repeticiones = repeticiones;
            }
            else
            {
                _BreathingExerciseData.Repeticiones = 1;
            }

            int inhalacion = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.inhalacion, out inhalacion))
            {
                _BreathingExerciseData.Inhalacion = inhalacion;
            }
            else
            {
                _BreathingExerciseData.Inhalacion = 1;
            }

            int mantener = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.mantener, out mantener))
            {
                _BreathingExerciseData.Mantener = mantener;
            }
            else
            {
                _BreathingExerciseData.Mantener = 1;
            }

            int exhalacion = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.exhalacion, out exhalacion))
            {
                _BreathingExerciseData.Exhalacion = exhalacion;
            }
            else
            {
                _BreathingExerciseData.Exhalacion = 1;
            }

            int descanso = 1;
            if (int.TryParse(_BreathingExerciseDataRaw.descanso, out descanso))
            {
                _BreathingExerciseData.Descanso = descanso;
            }
            else
            {
                _BreathingExerciseData.Descanso = 1;
            }

            _BreathingExerciseData.IsTutorial = _BreathingExerciseDataRaw.tutorial.Contains("on");
            _BreathingExerciseData.DogActive = _BreathingExerciseDataRaw.perrito.Contains("on");
        }

        /// <summary>
        /// Write Head Data
        /// </summary>
        public void SendExerciseComplete()
        {
            if (!_AreValid)
                return;

            string id_ejercicio = string.Empty;

            switch (currentExerciseId)
            {
                case breathingExerciseId:
                    id_ejercicio = _BreathingExerciseDataRaw.id_ejercicio;
                    break;

                case headExerciseId:
                    id_ejercicio = _HeadExerciseDataRaw.idEjercicio;
                    break;

                default:
                    id_ejercicio = _HeadExerciseDataRaw.idEjercicio;
                    break;
            }

            WWWForm formData = new WWWForm();

            formData.AddField("tipo", currentExerciseId);
            formData.AddField("id_ejercicio", id_ejercicio);

            StartCoroutine(SendPos(formData));
        }

        /// <summary>
        /// Send Pos
        /// </summary>
        private IEnumerator SendPos(WWWForm formData)
        {
            WWW www = new WWW("http://feelsgood.care/neuralvr/api/completaEjercicio", formData);

            yield return www;

            if (www.error != null)
            {
                Debug.LogErrorFormat("Error: {0}", www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }

        /// <summary>
        /// Read Head Exercise
        /// </summary>
        private void ReadHeadExercise(JSONNode jsonParsed)
        {
            _HeadExerciseDataRaw = new HeadExerciseDataRaw();

            _HeadExerciseDataRaw.idEjercicio = jsonParsed["id_ejercicio"];
            _HeadExerciseDataRaw.idPaciente = jsonParsed["id_paciente"];
            _HeadExerciseDataRaw.fecha = jsonParsed["fecha"];
            _HeadExerciseDataRaw.tipo = jsonParsed["tipo"];
            _HeadExerciseDataRaw.tiempoEjercicio = jsonParsed["tiempo_ejercicio"];
            _HeadExerciseDataRaw.grados = jsonParsed["grados"];

            JSONNode jsonParsedAux = JSON.Parse(jsonParsed["repeticiones"]);
            _HeadExerciseDataRaw.repeticiones = new RepeticionesHeadRaw[jsonParsedAux.Count];

            for (int i = 0; i < jsonParsedAux.Count; i++)
                _HeadExerciseDataRaw.repeticiones[i] = new RepeticionesHeadRaw(
                    jsonParsedAux[i]["TipoRepeticion"], jsonParsedAux[i]["grados"]);

            _HeadExerciseDataRaw.status = jsonParsed["status"];
            _HeadExerciseDataRaw.promedio = jsonParsed["promedio"];
            _HeadExerciseDataRaw.cantidad = jsonParsed["cantidad"];
            _HeadExerciseDataRaw.gano = jsonParsed["gano"];
            _HeadExerciseDataRaw.tiros = jsonParsed["tiros"];
            _HeadExerciseDataRaw.tutorial = jsonParsed["tutorial"];
            _HeadExerciseDataRaw.perrito = jsonParsed["perrito"];

            ParseHeadVariables();
        }

        /// <summary>
        /// Parse Variables
        /// </summary>
        private void ParseHeadVariables()
        {
            _HeadExerciseData = new HeadExerciseData();

            _HeadExerciseData.CantidadRepeticiones = _HeadExerciseDataRaw.repeticiones.Length;

            float dur = 0f;
            if (float.TryParse(_HeadExerciseDataRaw.tiempoEjercicio, out dur))
            {
                _HeadExerciseData.TiempoEjercicio = dur >= 2f ? dur : 2f;
            }
            else
            {
                _HeadExerciseData.TiempoEjercicio = 2f;
            }

            int prGnr = 0;
            if (int.TryParse(_HeadExerciseDataRaw.tipo, out prGnr))
            {
                int temp = prGnr == 1 || prGnr == 2 ? prGnr : 1;

                if (temp == 1)
                    _HeadExerciseData.IsProcedurallyGenerated = false;
                else
                    _HeadExerciseData.IsProcedurallyGenerated = true;
            }
            else
            {
                _HeadExerciseData.IsProcedurallyGenerated = false;
            }

            _HeadExerciseData.TipoMovimiento = new int[_HeadExerciseData.CantidadRepeticiones];
            _HeadExerciseData.Grados = new Grades[_HeadExerciseData.CantidadRepeticiones];

            for (int i = 0; i < _HeadExerciseData.CantidadRepeticiones; i++)
            {
                int temp = 0;
                if (int.TryParse(_HeadExerciseDataRaw.repeticiones[i].tipoRepeticion, out temp))
                {
                    _HeadExerciseData.TipoMovimiento[i] = temp == 1 || temp == 2 ? temp - 1 : 0;
                }
                else
                {
                    _HeadExerciseData.TipoMovimiento[i] = 0;
                }

                int grados = 0;
                if (int.TryParse(_HeadExerciseDataRaw.repeticiones[i].grados, out grados))
                {
                    _HeadExerciseData.Grados[i] = ParseGrades(grados);
                }
                else
                {
                    _HeadExerciseData.Grados[i] = ParseGrades(80);
                }
            }

            _HeadExerciseData.IsTutorial = _HeadExerciseDataRaw.tutorial.Contains("on");
            _HeadExerciseData.DogActive = _HeadExerciseDataRaw.perrito.Contains("on");
        }

        // -------------------------------
        private Grades ParseGrades(int i)
        {
            Grades grades;

            switch (i)
            {
                case 100:
                    grades = Grades.G100;
                    break;
                case 120:
                    grades = Grades.G120;
                    break;
                case 140:
                    grades = Grades.G140;
                    break;
                case 160:
                    grades = Grades.G160;
                    break;
                case 180:
                    grades = Grades.G180;
                    break;

                default:
                    grades = Grades.G80;
                    break;
            }

            return grades;
        }

        /// <summary>
        /// Send a form
        /// </summary>
        public void SendEvolution(FormWrapper[] datas)
        {
            WWWForm form = new WWWForm();

            for (int i = 0; i < datas.Length; i++)
            {
                form.AddField(datas[i].fieldName, datas[i].value);
            }

            WWW www = new WWW(urlSaveEvolution, form);

            StartCoroutine(SendPos(www));
        }

        /// <summary>
        /// Coroutine: Send Pos
        /// </summary>
        private IEnumerator SendPos(WWW request)
        {
            yield return request;

            if (request.error != null)
            {
                Debug.LogErrorFormat("Error: {0}", request.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }

    }

    #region AUXILIARS_CLASSES

    /// <summary>
    /// Head Exercise Raw Data
    /// </summary>
    [Serializable]
    public class HeadExerciseDataRaw
    {
        public string idEjercicio;
        public string idPaciente;
        public string fecha;
        public string tipo;
        public string tiempoEjercicio;
        public string grados;
        public RepeticionesHeadRaw[] repeticiones;
        public string status;
        public string promedio;
        public string cantidad;
        public string gano;
        public string tiros;
        public string tutorial;
        public string perrito;
    }

    /// <summary>
    /// Head Exercise Data
    /// </summary>
    public class HeadExerciseData
    {
        public bool IsProcedurallyGenerated;
        public float TiempoEjercicio;
        public int CantidadRepeticiones;
        public Grades[] Grados;
        public int[] TipoMovimiento;
        public bool IsTutorial;
        public bool DogActive;
    }

    /// <summary>
    /// Repeticiones Raw
    /// </summary>
    [Serializable]
    public class RepeticionesHeadRaw
    {
        public string grados;
        public string tipoRepeticion;

        public RepeticionesHeadRaw(string tipoRepeticion, string grados)
        {
            this.grados = grados;
            this.tipoRepeticion = tipoRepeticion;
        }
    }

    /// <summary>
    /// Breathing Exercise Data
    /// </summary>
    [Serializable]
    public class BreathingExerciseDataRaw
    {
        public string id_ejercicio;
        public string id_escenario;
        public string id_paciente;
        public string momento;
        public string repeticiones;
        public string inhalacion;
        public string mantener;
        public string exhalacion;
        public string descanso;
        public string fecha;
        public string status;
        public string tutorial;
        public string perrito;
    }

    /// <summary>
    /// Breathing Exercise Data
    /// </summary>
    public class BreathingExerciseData
    {
        public int Scenario;
        public TimeOfDay DayTime;
        public int Repeticiones;
        public int Inhalacion;
        public int Mantener;
        public int Exhalacion;
        public int Descanso;
        public bool IsTutorial;
        public bool DogActive;
    }

    #endregion

    #region ENUMS

    public enum TimeOfDay
    {
        Morning,
        Afternoon,
        Evening
    }

    #endregion
}