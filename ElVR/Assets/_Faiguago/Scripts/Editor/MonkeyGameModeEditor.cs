﻿using FI.GM.MONKEY;
using UnityEditor;
using UnityEngine;

namespace FI.ED
{

    [CustomEditor(typeof(MonkeyGameMode))]
    public class MonkeyGameModeEditor : Editor
    {

        /// <summary>
        /// OnInspectorGUI
        /// </summary>
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Reset PlayerPrefs"))
            {
                PlayerPrefs.DeleteAll();
            }
        }
    }

}