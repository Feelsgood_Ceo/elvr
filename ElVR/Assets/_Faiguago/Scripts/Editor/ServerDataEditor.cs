﻿using FI.TOOLS;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FI.TOOLS
{

    [CustomEditor(typeof(ServerData))]
    public class ServerDataEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            ServerData data = target as ServerData;
            DrawDefaultInspector();

            if (GUILayout.Button("FastQuery"))
            {
                data.FastQuery(data.patientInEditor);
            }
            if (GUILayout.Button("ReadData"))
            {
                data.ReadData(data.patientInEditor, data.exerciseInEditor);
            }
            if (GUILayout.Button("WriteData"))
            {
                data.SendExerciseComplete();
            }
        }
    }

}