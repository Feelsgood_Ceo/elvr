﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using FI.GM.MONKEY;
using System;
using System.Reflection;
using System.IO;
using FI.TOOLS;
using FI.AI.MONKEY;

namespace FI.Rep
{

    public class DatasRecorder : MonoBehaviour
    {

        [SerializeField, Range(0.1f, 1f)]
        private float minTimeToSave = 1f;

        private float timer = 0f, turnDuration = 0f;

        private DatasMovement datasWrapper;

        private List<DatasMovement> datasMovementList = new List<DatasMovement>();
        private List<MovementsWrapper> movementsList = new List<MovementsWrapper>();
        private List<MyQuaternion> myQuaternionList = new List<MyQuaternion>();

        public int State { get; set; }

        private bool stopRecord;

        [SerializeField]
        private bool saveLocally = false;

        private bool canSaveDatas;

        [SerializeField]
        private bool internalTest;

        private int turns;

        private float maxAngle;

        // Use this for initialization
        private void Start()
        {
#if !UNITY_EDITOR
            internalTest = false;
#endif
            InitialState.EntityWasSeen += EntityWasSeen;
            MonkeyGameMode.MonkeyGameFinishedEvent += MonkeyGameFinishedEvent;

            ClimbingState.TurnCompletedEvent += TurnCompletedEvent;
        }

        // --------------------------------------------------------------------------------------------
        private void TurnCompletedEvent()
        {
            datasMovementList.Add(new DatasMovement(new string[] { turnDuration.ToString("N2") + " s",
                (maxAngle * 2).ToString("N2"),
                ServerData.Instance._HeadExerciseData.Grados[turns++]
                    .GradesToInt().ToString() },
                new MyDatas[] { new MyDatas(turnDuration, myQuaternionList.ToArray()) }));

            maxAngle = 0f;
            turnDuration = 0f;
            myQuaternionList.Clear();
        }

        // ---------------------------
        private void EntityWasSeen()
        {
            canSaveDatas = true;
        }

        // ---------------------------------------------------------------------------------------------
        void Update()
        {
            if (!canSaveDatas)
                return;

            if (!stopRecord)
            {
                timer += Time.deltaTime;
                turnDuration += Time.deltaTime;

                if (timer >= minTimeToSave)
                {
                    timer = 0f;

                    MyQuaternion myQuat = new MyQuaternion { x = "0.000", y = "0.000", z = "0.000" };

                    float yawAngle = transform.localRotation.eulerAngles.y.SanitizeAngle();
                    if (yawAngle > maxAngle)
                        maxAngle = yawAngle;

                    myQuat = new MyQuaternion()
                    {
                        x = transform.localRotation.eulerAngles.x.ToString("N3").Replace(',', '.'),
                        y = transform.localRotation.eulerAngles.y.ToString("N3").Replace(',', '.'),
                        z = transform.localRotation.eulerAngles.z.ToString("N3").Replace(',', '.')
                    };

                    myQuaternionList.Add(myQuat);
                }
            }
        }

        // -------------------------------------------------------------------
        private void OnDestroy()
        {
            if (internalTest)
                MonkeyGameFinishedEvent("Test!");

            InitialState.EntityWasSeen -= EntityWasSeen;
            MonkeyGameMode.MonkeyGameFinishedEvent -= MonkeyGameFinishedEvent;

            ClimbingState.TurnCompletedEvent -= TurnCompletedEvent;
        }

        // ----------------------------------------------------------------------
        private void MonkeyGameFinishedEvent(string status)
        {
            if (ServerData.Instance._AreValid || internalTest)
            {
                DatasToSend datasToSend = new DatasToSend();

                datasToSend.tipo = "8";
                datasToSend.id_ejercicio = ServerData
                    .Instance._HeadExerciseDataRaw.idEjercicio;

                if (status.Equals("Complete"))
                    turns++;

                datasToSend.promedio = (((float)turns / ServerData.Instance
                    ._HeadExerciseData.Grados.Length) * 100).ToString("N0"); 
                datasToSend.status = status;
                datasToSend.cantidad = "0";
                datasToSend.movimientos = RetrieveMovements();

                if (!internalTest)
                {
                    // Using reflection to do a generic method later
                    FieldInfo[] fieldInfo = datasToSend.GetType().GetFields();

                    FormWrapper[] form = new FormWrapper[fieldInfo.Length];
                    for (int i = 0; i < form.Length; i++)
                    {
                        form[i].fieldName = fieldInfo[i].Name;
                        form[i].value = fieldInfo[i].GetValue(datasToSend).ToString();
                    }

                    // Sent datas to server trough singletons, CHANGE THIS BEHAVIOR LATER
                    ServerData.Instance.SendExerciseComplete();
                    ServerData.Instance.SendEvolution(form);
                }

#if UNITY_STANDALONE || UNITY_EDITOR
                if (saveLocally)
                {
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Feelsgood";

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    File.WriteAllText(path + string.Format("\\Datas_{0}_{1}_{2}_{3}.txt",
                        DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                        JsonConvert.SerializeObject(datasToSend));
                }
#endif
            }
        }

        /// <summary>
        /// Get the movements saved
        /// </summary>
        private string RetrieveMovements()
        {
            stopRecord = true;

            datasMovementList.Add(new DatasMovement(new string[] { turnDuration.ToString("N2") + " s",
                (maxAngle * 2).ToString("N2"),
                ServerData.Instance._HeadExerciseData.Grados[
                    turns < ServerData.Instance._HeadExerciseData.Grados.Length ? turns : turns - 1]
                    .GradesToInt().ToString() },
                new MyDatas[] { new MyDatas(turnDuration, myQuaternionList.ToArray()) }));

            movementsList.Add(new MovementsWrapper() { turno = datasMovementList.ToArray() });
            MovementsWrapper[] movements = movementsList.ToArray();

            string toSend = JsonConvert.SerializeObject(movements);

            return toSend;
        }
    }

}