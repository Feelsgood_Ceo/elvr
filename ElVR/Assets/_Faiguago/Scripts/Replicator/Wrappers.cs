﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FI.Rep
{
    [System.Serializable]
    public struct MyVector3
    {
        public string x;
        public string y;
        public string z;
    }

    [System.Serializable]
    public struct MyQuaternion
    {
        public string x;
        public string y;
        public string z;
    }

    [System.Serializable]
    public class MovementsWrapper
    {
        public DatasMovement[] turno;
    }

    [System.Serializable]
    public struct MyDatas
    {
        public float d;
        public MyQuaternion[] r;
        
        public MyDatas(float d, MyQuaternion[] r)
        {
            this.d = d;
            this.r = r;
        }
    }

    [System.Serializable]
    public struct DatasMovement
    {
        public string[] stats;
        public MyDatas[] datas;
        
        public DatasMovement(string[] stats, MyDatas[] datas)
        {
            this.stats = stats;
            this.datas = datas;
        }
    }

    public struct DatasToSend
    {
        public string tipo;
        public string id_ejercicio;

        public string promedio;
        public string status;
        public string cantidad;
        public string movimientos;
    }

    public struct FormWrapper
    {
        public string fieldName;
        public string value;
    }

}