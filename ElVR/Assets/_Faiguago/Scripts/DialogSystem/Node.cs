﻿namespace FI.DS
{

    public class Node
    {
        /// <summary>
        /// Root of the tree
        /// </summary>
        private Node root;
        public Node _Root
        {
            get
            {
                return root;
            }
        }

        /// <summary>
        /// Dialogue of the node
        /// </summary>
        private Dialogue dialogue;
        public Dialogue _Dialogue
        {
            get
            {
                return dialogue;
            }
        }

        /// <summary>
        /// List of children
        /// </summary>
        private Node[] children;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Node(Dialogue dialogue, params Node[] nodes)
        {
            this.dialogue = dialogue;

            if (nodes != null && nodes.Length > 0)
            {
                children = new Node[nodes.Length];
                AddChildren(nodes);
            }
        }

        /// <summary>
        /// Fill the array of nodes
        /// </summary>
        private void AddChildren(Node[] nodes)
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                children[i] = nodes[i];
            }
        }

        /// <summary>
        /// Get child using the index
        /// </summary>
        public Node GetChild(int index)
        {
            return children[index];
        }

        /// <summary>
        /// The current children amount
        /// </summary>
        public int ChildrenAmount()
        {
            if (children == null)
                return -1;
            else
                return children.Length;
        }
        
    }

}