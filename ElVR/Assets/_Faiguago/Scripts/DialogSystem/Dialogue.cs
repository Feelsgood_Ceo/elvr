﻿using UnityEngine;
using System;

namespace FI.DS
{
    
    public class Dialogue
    {
        /// <summary>
        /// Text to display
        /// </summary>
        public string dialogue;

        /// <summary>
        /// Option selected
        /// </summary>
        public int optionSelected;

        /// <summary>
        /// Execute an event at the
        /// end of the dialogue
        /// </summary>
        public Action dialogueAction;

        // ----------------------------------------------------
        public AudioClip audioClip;

        // ----------------------------------------------------
        public Dialogue(string dialogue, 
            Action dialogueAction, AudioClip audioClip = null)
        {
            this.audioClip = audioClip;
            this.dialogue = dialogue;
            this.dialogueAction = dialogueAction;
        }
    }

}