﻿using UnityEngine;
using TMPro;
using System.Text;
using UnityEngine.UI;
using System;

namespace FI.DS
{

    public interface IDialogInterface
    {
        // Main Panel
        void ShowPanel();
        void HidePanel();

        // Animated Text
        void ShowAnimatedText();
        void HideAnimatedText();

        // Set text
        void CleanText();
        void SetNameText(string name);
        void SetDialogText(StringBuilder dialogTextBuilder);

        // Set Audio Clip
        void PlayAudioClip(AudioClip clip);

        // Buttons
        void ShowContinueButtons(bool show);
        void ShowYesNoButtons(bool show);

        // Events
        event EventHandler continueButtonClicked;
        event EventHandler noButtonClicked;
        event EventHandler yesButtonClicked;
    }

    public class DialogueComponent : MonoBehaviour, IDialogInterface
    {
        #region VARIABLES

        /// <summary>
        /// Reference to the transform of the dialogUI
        /// </summary>
        [SerializeField]
        private GameObject dialogueUIGO;

        /// <summary>
        /// Animator reference
        /// </summary>
        private Animator animator;

        // ----------------------------------------------
        private GameObject panelGO, animatedTextGO,
            continueButtonGO, yesButtonGO, noButtonGO;

        // ----------------------------------------------
        private Button continueButton, yesButton, noButton;

        // ----------------------------------------------
        private TextMeshProUGUI nameText, dialogueText;

        // ----------------------------------------------
        private AudioSource audioSource;

        #endregion

        #region DIALOG_INTERFACE

        /// <summary>
        /// Empty Text
        /// </summary>
        private const string emptyText = "";

        /// <summary>
        /// Buttons event to handle
        /// </summary>
        public event EventHandler continueButtonClicked;
        public event EventHandler noButtonClicked;
        public event EventHandler yesButtonClicked;

        /// <summary>
        /// Show the panel
        /// </summary>
        public void ShowPanel()
        {
            animator.SetTrigger("Show");
        }

        /// <summary>
        /// Hide the panel
        /// </summary>
        public void HidePanel()
        {
            animator.SetTrigger("Hide");
        }

        /// <summary>
        /// Show the text to interact
        /// </summary>
        public void ShowAnimatedText()
        {
            CleanText();
            animatedTextGO.SetActive(true);
        }

        /// <summary>
        /// Hide the text to interact
        /// </summary>
        public void HideAnimatedText()
        {
            animatedTextGO.SetActive(false);
        }

        /// <summary>
        /// Set the name of the entity
        /// </summary>
        public void SetNameText(string name)
        {
            nameText.SetText(name);
        }

        /// <summary>
        /// Set the dialog of the text
        /// </summary>
        public void SetDialogText(StringBuilder dialogTextBuilder)
        {
            dialogueText.SetText(dialogTextBuilder);
        }

        // ----------------------------------------------
        public void ShowContinueButtons(bool show)
        {
            //continueButtonGO.SetActive(show);
        }

        // ----------------------------------------------
        public void ShowYesNoButtons(bool show)
        {
            //yesButtonGO.SetActive(show);
            //noButtonGO.SetActive(show);
        }

        // ----------------------------------------------
        public void CleanText()
        {
            dialogueText.SetText(emptyText);
            nameText.SetText(emptyText);
        }

        // ----------------------------------------------
        public void PlayAudioClip(AudioClip clip)
        {
            if (clip)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        #endregion

        // ----------------------------------------------
        private void Awake()
        {
            panelGO = dialogueUIGO.transform.Find("Panel").gameObject;
            animatedTextGO = dialogueUIGO.transform.Find("AnimatedText").gameObject;
            continueButtonGO = panelGO.transform.Find("ContinueButton").gameObject;
            yesButtonGO = panelGO.transform.Find("YesButton").gameObject;
            noButtonGO = panelGO.transform.Find("NoButton").gameObject;

            continueButton = continueButtonGO.GetComponent<Button>();
            noButton = noButtonGO.GetComponent<Button>();
            yesButton = yesButtonGO.GetComponent<Button>();

            continueButton.onClick.AddListener(() =>
            {
                if (continueButtonClicked != null)
                    continueButtonClicked(this, null);
            });

            noButton.onClick.AddListener(() =>
            {
                if (noButtonClicked != null)
                    noButtonClicked(this, null);
            });

            yesButton.onClick.AddListener(() =>
            {
                if (yesButtonClicked != null)
                    yesButtonClicked(this, null);
            });

            nameText = panelGO.transform.Find("NameText").GetComponent<TextMeshProUGUI>();
            dialogueText = panelGO.transform.Find("DialogueText").GetComponent<TextMeshProUGUI>();
            
            animator = panelGO.GetComponent<Animator>();
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        /// <summary>
        /// Default start method
        /// </summary>
        private void Start() { }
    }

}