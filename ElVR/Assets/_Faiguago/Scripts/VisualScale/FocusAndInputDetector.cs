﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

namespace FI
{

    public abstract class FocusAndInputDetector : MonoBehaviour, IInputClickHandler, IFocusable
    {
        [SerializeField]
        protected int id;

        private bool focused;
        private ParticleSystem particles;

        private Vector3 initialSize;

        [SerializeField, Range(1f, 2f)]
        private float maxSizeMultiplier;

        private float timer = 10f;

        // Use this for initialization
        protected virtual void Start()
        {
            initialSize = transform.localScale;
            particles = transform.GetChild(0).GetComponent<ParticleSystem>();
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (timer > 1f)
                return;

            if (focused)
            {
                timer += Time.deltaTime * 3f;
                transform.localScale = Vector3.Lerp(
                    initialSize, new Vector3(
                        initialSize.x * maxSizeMultiplier,
                        initialSize.y * maxSizeMultiplier,
                        initialSize.z), timer);
            }
            else
            {
                timer += Time.deltaTime * 3f;
                transform.localScale = Vector3.Lerp(new Vector3(
                    initialSize.x * maxSizeMultiplier,
                    initialSize.y * maxSizeMultiplier,
                    initialSize.z), initialSize, timer);
            }
        }

        #region INTERACTION_INTERFACE

        // -------------------------------
        public virtual void OnFocusEnter()
        {
            timer = 0f;
            focused = true;

            if (particles)
                particles.Play();
        }

        // -------------------------------
        public virtual void OnFocusExit()
        {
            timer = 0f;
            focused = false;

            if (particles)
                particles.Stop();
        }

        // ------------------------------------------------------------------
        public abstract void OnInputClicked(InputClickedEventData eventData);

        #endregion
    }

}