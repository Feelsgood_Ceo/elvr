﻿using HoloToolkit.Unity.InputModule;
using System;

namespace FI
{

    public class YesNoSelection : FocusAndInputDetector
    {
        public static event Action<int> YesNoSelectedEvent;

        // ------------------------------------------------------------------
        public override void OnInputClicked(InputClickedEventData eventData)
        {
            if (YesNoSelectedEvent != null)
                YesNoSelectedEvent(id);
        }
    }

}