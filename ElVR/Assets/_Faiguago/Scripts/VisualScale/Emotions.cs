﻿using HoloToolkit.Unity.InputModule;
using System;

namespace FI
{

    public class Emotions : FocusAndInputDetector
    {
        public static event Action<int> EmotionSelectedEvent;

        // ------------------------------------------------------------------
        public override void OnInputClicked(InputClickedEventData eventData)
        {
            if (EmotionSelectedEvent != null)
                EmotionSelectedEvent(id);
        }
    }

}