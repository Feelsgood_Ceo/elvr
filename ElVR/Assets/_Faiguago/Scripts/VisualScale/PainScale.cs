﻿using HoloToolkit.Unity.InputModule;
using System;

namespace FI
{

    public class PainScale : FocusAndInputDetector
    {
        public static event Action<int> PainSelectedEvent;

        // ------------------------------------------------------------------
        public override void OnInputClicked(InputClickedEventData eventData)
        {
            if (PainSelectedEvent != null)
                PainSelectedEvent(id);
        }
    }

}