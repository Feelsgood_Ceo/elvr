﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FI
{

    public static class ExtensionMethods
    {

        // -----------------------------------------------
        public static int GradesToInt(this Grades grades)
        {
            int angle = 0;

            switch (grades)
            {
                case Grades.G100:
                    angle = 100;
                    break;
                case Grades.G120:
                    angle = 120;
                    break;
                case Grades.G140:
                    angle = 140;
                    break;
                case Grades.G160:
                    angle = 160;
                    break;
                case Grades.G180:
                    angle = 180;
                    break;
                default:
                    angle = 80;
                    break;
            }

            return angle;
        }

        // ------------------------------------------------
        public static float SanitizeAngle(this float angle)
        {
            angle = Mathf.Abs(angle);

            if (angle > 100)
                angle = 360 - angle;

            return angle;
        }

    }

    public static class Utils
    {

        // -------------------------------------------------------------------------
        public static IEnumerator LoadNextScene(string scene, bool asyncMode = false)
        {
            if (asyncMode)
            {
                AsyncOperation ao = SceneManager.LoadSceneAsync(scene);
                yield return ao;
            }
            else
            {
                yield return null;
                SceneManager.LoadScene(scene);
            }
        }
    }

}