﻿namespace FI.TAGS
{

    public static class TagsPhotos
    {
        /// <summary>
        /// Tag for the monkey's photos
        /// </summary>
        public static string[] monkeysPose = { "MonkeyAirplanePose",
            "MonkeyBalletPose", "MonkeyBreakPose", "MonkeyTarzanPose",
            "MonkeyRobotPose", "MonkeyHiPose" };
    }

}