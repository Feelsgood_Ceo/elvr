﻿namespace FI.TAGS
{

    public static class ScenesTags
    {
        public const string splashScreen = "SplashScreen";
                     
        public const string intro = "Intro";
                     
        public const string monkeyGame = "MonkeyGame";

        public const string tutorial = "Tutorial";

        public const string authentication = "Authentication";

        public const string breathingScene = "BreathingScene";

        public const string visualScale = "AnalogScale";
    }

}