﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FI.TAGS
{

    public static class PlayerPrefsTags
    {

        /// <summary>
        /// 0 to start the tutorial;
        /// 1 to start the game
        /// </summary>
        public const string monkeyGameMode = "MonkeyGameMode";

        /// <summary>
        /// 0 if the game was not played;
        /// 1 if the game was played
        /// </summary>
        public const string monkeyGameWasPlayed = "MonkeyGamePlayed";

    }

}