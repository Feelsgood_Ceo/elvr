﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PathCreator : MonoBehaviour
{

    private Vector3[] points;
    public Vector3[] Points { get { return points; } }

    [SerializeField, Range(20, 100)]
    private int numberOfPoints = 20;

    [SerializeField, Range(100, 180)]
    private float desiredAngle = 100;
    public float DesiredAngle
    {
        get
        {
            return desiredAngle;
        }
        set
        {
            desiredAngle = value;
            Generate();
        }
    }

    [SerializeField]
    private float sphereSize = 0.1f;
    public float SphereSize { get { return sphereSize; } }

    [SerializeField]
    private float radius = 1f;

    public float extensionAngle = 15f;

    [SerializeField, Range(1, 3)]
    private float zOffset = 1f;

    [SerializeField]
    private float yOffset = 1f;

    [SerializeField]
    private bool autoUpdate;
    public bool AutoUpdate { get { return autoUpdate; } }

    private float startAngle;

    public Vector3 LeftPoint { get; private set; }
    public Vector3 RightPoint { get; private set; }

    private Vector3 previousPos;
    private Quaternion previousRot;

    private Mesh mesh;

    public static PathCreator Instance { get; private set; }

    // ----------------------------------------
    private void Start()
    {
        Instance = this;

        Generate();
    }

    // -----------------------------------------------------------------------------------
    public void Generate()
    {
        points = new Vector3[numberOfPoints];
        startAngle = (180 - desiredAngle) / 2f;

        float t = 0f;
        float steps = 1f / points.Length;

        for (int i = 0; i < points.Length; i++)
        {
            points[i] = GetPointInCircle(t, false);
            t += steps;
        }

        GenerateMesh();

        for (int i = 0; i < points.Length; i++)
            points[i] = transform.TransformPoint(points[i]);

        float height = radius * Mathf.Tan(extensionAngle * Mathf.Deg2Rad);

        LeftPoint = GetPointInCircle(1f) - Vector3.up * height;
        RightPoint = GetPointInCircle(0f) - Vector3.up * height;
    }

    public Vector3 GetPointInCircle(float t, bool requireWorldSpace = true)
    {
        startAngle = (180 - desiredAngle) / 2f;

        float x = radius * Mathf.Cos((startAngle + t * desiredAngle) * Mathf.Deg2Rad);
        float z = Mathf.Sqrt(radius * radius - x * x) * zOffset;

        if (!requireWorldSpace)
            return new Vector3(x, 0f, z);
        else
            return transform.TransformPoint(new Vector3(x, 0f, z));
    }

    // -------------------------------------------------------------
    private void GenerateMesh()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().sharedMesh = mesh;

        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();

        for (int i = 0, t = 0; i < points.Length - 1; i++, t += 3)
        {
            vertices.Add(points[i] - Vector3.up * yOffset);
            vertices.Add(Vector3.zero - Vector3.up * yOffset);
            vertices.Add(points[i + 1] - Vector3.up * yOffset);

            triangles.Add(t);
            triangles.Add(t + 1);
            triangles.Add(t + 2);
        }

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
    }

    // ------------------------------------------
    private void Update()
    {
        if (!Application.isPlaying
            && previousPos != transform.position
            || previousRot != transform.rotation)
        {
            Generate();

            previousPos = transform.position;
            previousRot = transform.rotation;
        }
    }
}
