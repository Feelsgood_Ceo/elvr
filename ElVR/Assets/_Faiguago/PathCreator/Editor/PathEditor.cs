﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PathCreator))]
public class PathEditor : Editor
{

    private PathCreator circle;

    private void OnEnable()
    {
        circle = target as PathCreator;

        if (circle.Points == null)
            circle.Generate();
    }

    private void OnSceneGUI()
    {
        Draw();
    }

    public override void OnInspectorGUI()
    {
        if (DrawDefaultInspector()
            && circle.AutoUpdate
            || GUILayout.Button("Update"))
        {
            circle.Generate();
        }
    }

    private void Draw()
    {
        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

        Handles.color = Color.white;
        Handles.DrawLine(circle.LeftPoint, circle.Points[circle.Points.Length - 1]);
        Handles.DrawLine(circle.RightPoint, circle.Points[0]);

        for (int i = 0; i < circle.Points.Length - 1; i++)
            Handles.DrawLine(circle.Points[i], circle.Points[i + 1]);

        Handles.color = Color.red;

        Handles.SphereHandleCap(0, circle.Points[0], 
            Quaternion.identity, circle.SphereSize, EventType.Repaint);
        Handles.SphereHandleCap(0, circle.Points[circle.Points.Length / 2],
            Quaternion.identity, circle.SphereSize, EventType.Repaint);
        Handles.SphereHandleCap(1, circle.Points[circle.Points.Length - 1], 
            Quaternion.identity, circle.SphereSize, EventType.Repaint);

        Handles.SphereHandleCap(1, circle.LeftPoint,
            Quaternion.identity, circle.SphereSize, EventType.Repaint);
        Handles.SphereHandleCap(1, circle.GetPointInCircle(0.5f),
            Quaternion.identity, circle.SphereSize, EventType.Repaint);
        Handles.SphereHandleCap(1, circle.RightPoint,
            Quaternion.identity, circle.SphereSize, EventType.Repaint);
        Handles.color = Color.green;
        Handles.SphereHandleCap(1, circle.transform.position,
            Quaternion.identity, circle.SphereSize, EventType.Repaint);

        Handles.color = Color.green;
        Handles.DrawLine(circle.transform.position, circle.Points[0]);
        Handles.DrawLine(circle.transform.position, circle.Points[circle.Points.Length - 1]);
    }
}
