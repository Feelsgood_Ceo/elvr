﻿namespace FI.TOOLS
{

    public enum BezierControlPointMode
    {
        Free,
        Aligned,
        Mirrored
    }

}