﻿using UnityEngine;

namespace FI.TOOLS
{

    public class SplineWalker : MonoBehaviour
    {

        public enum SplineWalkerMode
        {
            Once,
            Loop,
            PingPong
        }

        public BezierSpline spline;

        public float duration;

        private float progress;

        public bool lookAtVelocity;

        public bool lookForward;

        public SplineWalkerMode mode;

        private bool goingForward = true;

        private Vector3 oldPos;

        private void Update()
        {
            if (goingForward)
            {
                progress += Time.deltaTime / duration;
                if (progress > 1f)
                {
                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else
                    {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }
            }
            else
            {
                progress -= Time.deltaTime / duration;
                if (progress < 0f)
                {
                    progress = -progress;
                    goingForward = true;
                }
            }

            Vector3 position = spline.GetPoint(progress);
            transform.localPosition = position;
            if (lookAtVelocity)
            {
                transform.LookAt(position + spline.GetDirection(progress));
            }

            if (lookForward)
            {
                Vector3 dir = transform.position - oldPos;
                dir.y = 0f;
                dir.Normalize();

                oldPos = transform.position;
                transform.rotation = Quaternion.LookRotation(dir);
            }
        }
    }

}