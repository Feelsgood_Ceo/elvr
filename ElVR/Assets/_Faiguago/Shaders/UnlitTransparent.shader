﻿Shader "FI/UnlitTransparent"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue"="Transparent" }
		LOD 100

		Pass
		{
			Cull Off
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
		
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			fixed4 _Color;

			float4 vert (appdata_base v) : SV_POSITION
			{
				return UnityObjectToClipPos(v.vertex);
			}
			
			fixed4 frag () : SV_Target
			{
				return _Color;
			}

			ENDCG
		}
	}
}
