﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransPuntoCamera : MonoBehaviour {
	/// <summary>
	/// Porfavor elimina los sectores de testing que digan "Solo Valido para Test en Editor"
	/// solo son utilies dentro del editor, para probar el scritps, solo de debe llamar al metodo
	/// "TransCamera (GameObject IrHasta)" y pasarle el objeto para que desplace a la siguente Escena
	/// </summary>

	// Objeto con todos los Puntos de Camera
	public GameObject AllPoint;

	//Solo Valido para Test en Editor///////////
	// Array de Puntos de Camera
	private GameObject[] PuntosCameras;
	///////////////////////////////////////////

	// Camare del jugador
	public GameObject CamaraPlayer;

	// Punto medio entre camera
	public GameObject PuntoTras;

	// nuevo punto de llegada
	private GameObject PuntoHasta;

	// Variable para ejecutar la transicion
	public bool SwichOn = false;

	// Velocidad de Posicion
	[Range(0.0001f, 0.01f)]
	public float VelocidadTrans;

	// Velocidad de Rotacion
	[Range(0.0001f, 0.01f)]
	public float VelocidadRota;

	// Velocidad de Rotacion
	public string TagCamera;

	//Valores de Seteo de posicion inicial
	public float TransX; 
	public float TransY; 
	public float TransZ;

	public float RotX; 
	public float RotY;
	public float RotZ;
	public float RotW;



	void Start (){


		//Inicializamo el Objeto
		PuntoHasta = new GameObject("PuntoHasta");

		//Solo Valido para Test en Editor///////////
		// Creamos el array de puntos desde los hijos del objeto
		PuntosCameras = GameObject.FindGameObjectsWithTag(TagCamera);
		///////////////////////////////////////////

		PuntoTras.transform.position = new Vector3 (TransX, TransY, TransZ);
		PuntoTras.transform.Rotate(RotX, RotY, RotZ);

	}

	public void TransCamera (GameObject IrHasta) {

		SwichOn = false;

		//Solo Valido para Test en Editor///////////
		int Punto = Random.Range (0, 11);
		IrHasta = PuntosCameras[Punto];
		Debug.Log (PuntosCameras[Punto].name);
		///////////////////////////////////////////

		PuntoHasta.GetComponent<Transform> ().position = IrHasta.GetComponent<Transform> ().position;
		PuntoHasta.GetComponent<Transform> ().rotation = IrHasta.GetComponent<Transform> ().rotation;
		SwichOn = true;
	}
		
	//Solo Valido para Test en Editor///////////
	void Update(){

		if (Input.GetKeyUp(KeyCode.Q)) {
			TransCamera (null);
		}
	}
	///////////////////////////////////////////

	void FixedUpdate () {
		
		if (SwichOn == true) {
			
			PuntoTras.transform.position = Vector3.Lerp (PuntoTras.transform.position,  PuntoHasta.GetComponent<Transform> ().position, VelocidadTrans);
			PuntoTras.transform.rotation = Quaternion.Lerp (PuntoTras.transform.rotation, PuntoHasta.GetComponent<Transform> ().rotation, VelocidadRota);

		}

	}

}
