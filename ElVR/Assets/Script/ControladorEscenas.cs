﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public enum Ejercicio
{
	Aventurero=0,Inicio=1,Respiracion=2,Diagnostico=3,Hongos_Musicales=4,Bote=5
}
public class ControladorEscenas : MonoBehaviour {
	public Ejercicio tipo;
	public int TipoEscenarioRespiracion;
	public Transform ELVR;
	private Transform player;
	public ControladorEfectos controladorefectos;
	public Transform[] controladores;
	// Use this for initialization
	void Start () {
		player = controladores [0].root.transform;
		if ((int)tipo == 1) {
			ELVR.gameObject.GetComponent<PlayableDirector> ().Play ();
		}else if ((int)tipo == 2) {
			controladores [2].GetComponent<ControladorCirculoRespiracion> ().escenario = TipoEscenarioRespiracion;
		}else if ((int)tipo == 3) {
			ELVR.position = this.transform.GetChild ((int)tipo).GetChild (0).position;
			ELVR.rotation = this.transform.GetChild ((int)tipo).GetChild (0).rotation;
			this.transform.GetChild ((int)tipo).GetChild (1).gameObject.SetActive (true);
		}else if ((int)tipo == 4) {
		}
		else if ((int)tipo == 5) {
		}
		else {
		}

		player.position = this.transform.GetChild ((int)tipo).position;
		player.rotation = this.transform.GetChild ((int)tipo).rotation;


		controladorefectos.EfectoCambioEscena ("abrir");
		foreach (Transform children in controladores) {
			children.gameObject.SetActive (false);
		}
		controladores [(int)tipo].gameObject.SetActive (true);

	}
	
	// Update is called once per frame
	void Update () {
		if ((int)tipo == 5) {
			ELVR.position = this.transform.GetChild ((int)tipo).GetChild (1).position;
			ELVR.rotation = this.transform.GetChild ((int)tipo).GetChild (1).rotation;
			player.position = this.transform.GetChild ((int)tipo).GetChild (0).position;
			player.rotation = this.transform.GetChild ((int)tipo).GetChild (0).rotation;
		}
	}

	public void CambiarEjercicio(){
	
	}
}
