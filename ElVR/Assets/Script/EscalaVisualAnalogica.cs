﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FI.TOOLS;

public class EscalaVisualAnalogica : MonoBehaviour {

    [Header("Escala visual analógica")]
    public GameObject caritasImagenes;

	// Use this for initialization
	void Start () {
        StartCoroutine(Delay());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void CheckEscala()
    {
        Debug.Log(ServerData.Instance._HeadExerciseData.DogActive);

        if (ServerData.Instance._HeadExerciseData.DogActive)
        {
            caritasImagenes.SetActive(true);
        }
        else
        {
            caritasImagenes.SetActive(false);
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2f);
        CheckEscala();
    }
}
