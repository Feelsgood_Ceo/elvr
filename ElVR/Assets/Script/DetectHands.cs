﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectHands : MonoBehaviour {
	public Material guiaDesac;
	public Material guiaActiv;
	public Transform circuloGuia;
	private bool siguiendo=false;
	public float speed;
	public Transform remo;
	private float angulo=90;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.R)) {
			Debug.Log ("MOVIMIENTOS CORRECTOS: "+ControladorBote.movimientosCorrectos);
			Debug.Log ("MOVIMIENTOS INCORRECTOS: "+ControladorBote.movimientosIncorrectos);
		}
		remo.localRotation = Quaternion.Slerp(remo.rotation,Quaternion.Euler(new Vector3(angulo-90f,0,-30f)), Time.time * speed);
		//	remo.rotation = this.transform.rotation;
	}
	private void OnTriggerExit(Collider col)
	{
		if (col.name == "Remo") {
		}
		//	siguiendo = false;
	}
	private void OnTriggerEnter(Collider col)
	{
		if (col.name == "1"||col.name == "2"||col.name == "3"||col.name == "4"||col.name == "5"||
			col.name == "6"||col.name == "7"||col.name == "8") {
			angulo = (int.Parse (col.name) - 1) * 45f;
			Debug.Log ("" + col.name);
			Debug.Log ("" + angulo);
			if (ControladorBote.patron != ""&&ControladorBote.patron.Length>=2) {
				if (ControladorBote.patron[0].ToString() == col.name) {
					ValidarPatron (ControladorBote.patron);
					ControladorBote.patron = "";
					foreach (Transform children in circuloGuia) {
						children.GetComponent<Renderer> ().material = guiaDesac;
					}
				}
			}

			if (ControladorBote.patron.Contains (col.name)) {

			}else {
				col.GetComponent<Renderer> ().material = guiaActiv;
				ControladorBote.patron = ControladorBote.patron + col.name;
			//	Debug.Log (ControladorBote.patron);
			}
		}


	}

	void ValidarPatron(string pat){
		if (pat.Length == 8) {
			for (int i = 0; i <= pat.Length-1; i++) { 
				if (i == pat.Length-1) {
					if (pat [i].ToString () == "8") {
						if (pat [0].ToString () == "1") {

						} else {
							ControladorBote.movimientosIncorrectos += 1;
							return;
						}
					} else {
						if (int.Parse (pat [i].ToString ()) + 1 == int.Parse (pat [0].ToString ())) {

						} else {
							ControladorBote.movimientosIncorrectos += 1;
							return;
						}
					}
				} else {
					if (pat [i].ToString () == "8") {
						if (pat [i + 1].ToString () == "1") {

						} else {
							ControladorBote.movimientosIncorrectos += 1;
							return;
						}
					} else {
						if (int.Parse (pat [i].ToString ()) + 1 == int.Parse (pat [i + 1].ToString ())) {

						} else {
							ControladorBote.movimientosIncorrectos += 1;
							return;
						}
					}
				}
			}
			ControladorBote.movimientosCorrectos += 1;
		} else {
			ControladorBote.movimientosIncorrectos += 1;
			return;
		}
		return;

	}
}
