﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorBote : MonoBehaviour {
	public Transform bote;
	public static string patron="";
	public static int movimientosCorrectos;
	public static int movimientosIncorrectos;
	public float speed;
	// Use this for initialization
	void Start () {
		
	}

	void Enable(){
		
	}

	// Update is called once per frame
	void Update () {
		//bote.GetChild (2).position = this.transform.root.GetChild (6).position;

		bote.GetChild (2).GetChild(0).Rotate (Vector3.right * Time.deltaTime*speed);	
	}
}
