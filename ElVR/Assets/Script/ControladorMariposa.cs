﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorMariposa : MonoBehaviour {
	public Transform controlDerecho;
	public Transform controlIzquierdo;
	public int niveldif;
	public float distancia;
	public string tipo;
	public float speed;
	public Transform target;
	public bool completado=false;
	private Transform mariposa;
	private Vector3 posicionInicioControlDerecho;
	private Vector3 posicionInicioControlIzquierdo;
	// Use this for initialization
	void Start () {
		
	}

	void OnEnable(){
		mariposa = this.transform;
		StartCoroutine (checkposition ());
	}

	IEnumerator checkposition(){
		yield return new WaitForSeconds(3f);
		guardarPosicionInicio ();
	}

	void guardarPosicionInicio(){
		posicionInicioControlDerecho = controlDerecho.localPosition;
		posicionarMariposa (niveldif,distancia,tipo);
	}
	void posicionarMariposa(int dif,float dis,string tip){
		
		if (tip == "x") {
			target.localPosition = new Vector3(posicionInicioControlDerecho.x+dis*dif,posicionInicioControlDerecho.y,posicionInicioControlDerecho.z+0.05f);
		} else if (tip == "y") {
			target.localPosition = new Vector3(posicionInicioControlDerecho.x,posicionInicioControlDerecho.y+dis*dif,posicionInicioControlDerecho.z);
		} else {
			target.localPosition = new Vector3(posicionInicioControlDerecho.x,posicionInicioControlDerecho.y,posicionInicioControlDerecho.z+dis*dif);
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == "body") {
			niveldif += 1;
			if (niveldif == 6) {
				if (tipo == "x") {
					tipo = "y";
				} else if (tipo == "y") {
					tipo = "z";
				} else {
					return;
				}
				niveldif = 1;
			}
			posicionarMariposa (niveldif, distancia, tipo);
		}
	}

	// Update is called once per frame
	void Update () {
		float step = speed * Time.deltaTime;
		mariposa.position = Vector3.MoveTowards (mariposa.position, target.position, step);
	}
}
