﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorEfectos : MonoBehaviour {
	public Transform capaOscura;
	public Transform luzPrincipal;
	public string estadoSol;


	private bool estaEjecutandoCambioEscena;
	private bool estadoCapaOscura;
	private bool estaInterpolandoAudio;

	public static AudioSource MusicaGeneral;
	public static AudioSource EfectoInhalar;
	public static AudioSource EfectoExhalar;

	private float val=0;
	void Start () {
		if (estadoSol == "dia") {
			luzPrincipal.GetChild (0).rotation = luzPrincipal.GetChild (1).rotation;
			luzPrincipal.GetChild (0).GetComponent<Light> ().intensity = 1.2f;
			luzPrincipal.GetChild (4).gameObject.SetActive (false);
		} else if (estadoSol == "tarde") {
			luzPrincipal.GetChild (0).rotation = luzPrincipal.GetChild (2).rotation;
			luzPrincipal.GetChild (0).GetComponent<Light> ().intensity = 0.6f;
			luzPrincipal.GetChild (4).gameObject.SetActive (false);
		} else {
			luzPrincipal.GetChild (0).rotation = luzPrincipal.GetChild (3).rotation;
			luzPrincipal.GetChild (0).GetComponent<Light> ().intensity = 0.3f;
			luzPrincipal.GetChild (4).gameObject.SetActive (true);
		}

		estaEjecutandoCambioEscena = false;
		if (capaOscura.GetComponent<SpriteRenderer> ().color.a == 0f) {
			estadoCapaOscura = false;
		} else if (capaOscura.GetComponent<SpriteRenderer> ().color.a == 1f) {
			estadoCapaOscura = true;
		}

		MusicaGeneral = this.transform.GetComponent<AudioSource> ();
		EfectoInhalar = this.transform.GetChild (0).GetComponent<AudioSource> ();
		EfectoExhalar = this.transform.GetChild (1).GetComponent<AudioSource> ();

	}
	void FixedUpdate(){

	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A)) {
			EfectoCambioEscena ("abrir");
		} else if (Input.GetKeyDown (KeyCode.C)) {
			EfectoCambioEscena ("cerrar");
		}

		if (estaEjecutandoCambioEscena == true) {
			if (estadoCapaOscura == true) {
				if (capaOscura.GetComponent<SpriteRenderer> ().color.a <= 0.8f) {
					capaOscura.GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, capaOscura.GetComponent<SpriteRenderer> ().color.a - 0.005f);
				} else {
					capaOscura.GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, capaOscura.GetComponent<SpriteRenderer> ().color.a - 0.001f);
				}
			} else {
				if (capaOscura.GetComponent<SpriteRenderer> ().color.a <= 0.8f) {
					capaOscura.GetComponent<SpriteRenderer> ().color = new Color(0,0,0,capaOscura.GetComponent<SpriteRenderer> ().color.a+0.01f);
				} else {
					capaOscura.GetComponent<SpriteRenderer> ().color = new Color(0,0,0,capaOscura.GetComponent<SpriteRenderer> ().color.a+0.004f);
				}
			}
		} else {
		}

		if (capaOscura.GetComponent<SpriteRenderer> ().color.a <= 0f) {
			capaOscura.GetComponent<SpriteRenderer> ().color = new Color(0,0,0,0);
			estaEjecutandoCambioEscena = false;
		}

		if (capaOscura.GetComponent<SpriteRenderer> ().color.a >= 1f) {
			capaOscura.GetComponent<SpriteRenderer> ().color = new Color(0,0,0,1);
			estaEjecutandoCambioEscena = false;
		}

		if (estaInterpolandoAudio) {
			MusicaGeneral.volume += val;
			if (MusicaGeneral.volume <= 0) {
				estaInterpolandoAudio = false;
				MusicaGeneral.volume = 0f;
			} else if (MusicaGeneral.volume >= 0.6f) {
				estaInterpolandoAudio = false;
				MusicaGeneral.volume = 0.6f;
			}
		}

	}

	public void EfectoCambioEscena(string tipo){
		estaEjecutandoCambioEscena = true;
		if (tipo == "abrir")
			estadoCapaOscura = true;
		else
			estadoCapaOscura = false;
	}

	public void interpolarSonidoFondo (string tipo, float tiempo){
		if (tipo == "subir") {
			val = 0.6f/(60*(tiempo));
			Debug.Log ("subir audio");
		}else {
			val =(0.6f/(60*(tiempo)))* -1;
			Debug.Log ("bajar audio");
		}
		StartCoroutine (ChangeValInter (tiempo));
	}

	IEnumerator ChangeValInter(float t){
		estaInterpolandoAudio = true;
		yield return new WaitForSeconds(t);

	}

}
