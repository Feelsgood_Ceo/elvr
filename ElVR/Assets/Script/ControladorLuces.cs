﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorLuces : MonoBehaviour {

    public List<GameObject> _luces = new List<GameObject>();

	public bool activarAreaLuces = false;
	public int _areaLuces;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		PrenderLuz (activarAreaLuces, _luces [_areaLuces]);

	}

	void PrenderLuz (bool enabled, GameObject Area){
		if(enabled == true)	
		{		
			Area.SetActive(enabled);
		}
		if(enabled == false)
		{
			Area.SetActive(enabled);
		}
	}

}
