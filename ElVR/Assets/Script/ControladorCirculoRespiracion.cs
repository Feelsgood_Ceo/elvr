﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCirculoRespiracion : MonoBehaviour {
	public ControladorEfectos controladorefectos;
	public Transform target;
	public int escenario;
	public Transform[] puntosRespiracion;
	public float repeticiones;
	private float speed=1;
	private float velaparicion=15;
	private int estadoRespiracion=3;
	private int contadorRespiraciones;
	// Use this for initialization
	void Start () {
	}

	void OnEnable(){
		//OPTIMIZAR CODIGO !!!!
		this.transform.root.transform.position = puntosRespiracion[escenario - 1].transform.position;
		this.transform.root.transform.rotation = puntosRespiracion[escenario - 1].transform.rotation;
		StartCoroutine(mostrarcirculo());
	}

	IEnumerator mostrarcirculo(){
		yield return new WaitForSeconds (velaparicion);
		foreach(Transform children in this.transform)
			children.gameObject.SetActive(true);
		StartCoroutine(CadenaTextosInicio());

	}

	IEnumerator CadenaTextosInicio(){
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "";
		yield return new WaitForSeconds (8f);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "ES MOMENTO\nDE RESPIRAR";
		yield return new WaitForSeconds (4f);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "INICIEMOS";
		yield return new WaitForSeconds (4f);
		controladorefectos.interpolarSonidoFondo("bajar",5);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().fontSize = 150;
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "3";
		yield return new WaitForSeconds (2f);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "2";
		yield return new WaitForSeconds (2f);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "1";
		yield return new WaitForSeconds (2f);
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().fontSize = 60;
		StartCoroutine(Respiracion());
	}
	IEnumerator CadenaTextosFinal(){
		contadorRespiraciones += 1;
		this.transform.GetChild (0).GetChild(0).GetComponent<TextMesh>().text = "LO HICISTE MUY BIEN";
		controladorefectos.interpolarSonidoFondo("subir",5);
		yield return new WaitForSeconds (5f);
		foreach(Transform children in this.transform)
			children.gameObject.SetActive(false);
		
		yield return new WaitForSeconds (7f);
		controladorefectos.EfectoCambioEscena ("cerrar");
	}


	IEnumerator Respiracion(){
		estadoRespiracion += 1;
		if (estadoRespiracion == 4) {
			estadoRespiracion = 0;
		}
		contadorRespiraciones += 1;
		if (estadoRespiracion == 0) {
			ControladorEfectos.EfectoExhalar.Stop ();
			ControladorEfectos.EfectoInhalar.Play ();
		} else if (estadoRespiracion == 2) {
			ControladorEfectos.EfectoInhalar.Stop ();
			ControladorEfectos.EfectoExhalar.Play ();
		} else {
			ControladorEfectos.EfectoInhalar.Stop ();
			ControladorEfectos.EfectoExhalar.Stop ();
		}
	
		yield return new WaitForSeconds (4f);

		if (contadorRespiraciones+1 > repeticiones*4) {
			StartCoroutine(CadenaTextosFinal());
		} else {
			StartCoroutine(Respiracion());
		}
	}

	void Update() {
				if (this.transform.GetChild(0).gameObject.activeSelf) {
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, target.position, step);
			transform.rotation = target.rotation;
		}

		if (contadorRespiraciones > 0 && contadorRespiraciones<=repeticiones*4) {
			if (estadoRespiracion == 0) {
				this.transform.GetChild (0).GetChild (0).GetComponent<TextMesh> ().text = "INHALA";
				this.transform.GetChild (0).localScale = new Vector3 (this.transform.GetChild (0).localScale.x + 0.002f,
					this.transform.GetChild (0).localScale.y + 0.002f, this.transform.GetChild (0).localScale.z + 0.002f);
				this.transform.GetChild (1).localScale = new Vector3 (this.transform.GetChild (1).localScale.x + 0.01f,
					this.transform.GetChild (1).localScale.y + 0.01f, this.transform.GetChild (1).localScale.z + 0.01f);
				this.transform.GetChild (2).localScale = new Vector3 (this.transform.GetChild (2).localScale.x + 0.008f,
					this.transform.GetChild (2).localScale.y + 0.008f, this.transform.GetChild (2).localScale.z + 0.008f);
				this.transform.GetChild (3).localScale = new Vector3 (this.transform.GetChild (3).localScale.x + 0.006f,
					this.transform.GetChild (3).localScale.y + 0.006f, this.transform.GetChild (3).localScale.z + 0.006f);
			} else if (estadoRespiracion == 2) {
				this.transform.GetChild (0).GetChild (0).GetComponent<TextMesh> ().text = "EXHALA";
				this.transform.GetChild (0).localScale = new Vector3 (this.transform.GetChild (0).localScale.x - 0.002f,
					this.transform.GetChild (0).localScale.y - 0.002f, this.transform.GetChild (0).localScale.z - 0.002f);
				this.transform.GetChild (1).localScale = new Vector3 (this.transform.GetChild (1).localScale.x - 0.01f,
					this.transform.GetChild (1).localScale.y - 0.01f, this.transform.GetChild (1).localScale.z - 0.01f);
				this.transform.GetChild (2).localScale = new Vector3 (this.transform.GetChild (2).localScale.x - 0.008f,
					this.transform.GetChild (2).localScale.y - 0.008f, this.transform.GetChild (2).localScale.z - 0.008f);
				this.transform.GetChild (3).localScale = new Vector3 (this.transform.GetChild (3).localScale.x - 0.006f,
					this.transform.GetChild (3).localScale.y - 0.006f, this.transform.GetChild (3).localScale.z - 0.006f);
			} else if (estadoRespiracion == 1) {
				this.transform.GetChild (0).GetChild (0).GetComponent<TextMesh> ().text = "MANTEN EL AIRE";
			} else {
				this.transform.GetChild (0).GetChild (0).GetComponent<TextMesh> ().text = "DESCANSA";
			}
		} else {
		}
		
	}
}
