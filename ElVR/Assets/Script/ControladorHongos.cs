﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorHongos : MonoBehaviour {

	private AudioSource sonido;
	// Use this for initialization
	void Start () {
		sonido = this.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == "body") {
				this.GetComponent<Animator> ().SetBool ("golpeado", true);
				sonido.Play ();
		}
	}

	private void OnTriggerExit(Collider col)
	{
		if (col.gameObject.name == "body") {
			this.GetComponent<Animator> ().SetBool ("golpeado", false);
		}
	}
}
