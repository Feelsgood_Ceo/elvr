﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimaControl : MonoBehaviour {
	//Grupo de Objectos para el efecto
	public GameObject ObjSol;
	public GameObject ObjLuna;
	public GameObject PuntoCentro;
	public ParticleSystem ParticulasLluvia;
	public ParticleSystem ParticulasNieve;

	public Color ColorSol;
	public Color ColorLuna;

	//Grupo de Variables para el efecto
	[Range(0.01f,5f)]
	public float IntensidadSol;
	[Range(0.01f,5f)]
	public float IntensidadLuna;

	[Range(0.01f,1f)]
	public float Incrementador;
	private float VelociadaBase = 0.001f;

	public bool Nevando = false;
	public bool Soleado = false;
	public bool Lluvioso = false;

	void Start () {
		
		//Cambio de colores
		ObjSol.GetComponent<Light> ().color = ColorSol;
		ObjLuna.GetComponent<Light> ().color = ColorLuna;

		//Cambio de Interncia luminica del el sol y la luna
		ObjSol.GetComponent<Light> ().intensity = IntensidadSol;
		ObjLuna.GetComponent<Light> ().intensity = IntensidadLuna;
	}

	void FixedUpdate () {
		
		//Cambio de colores en tiempo de ejecucion
		ObjSol.GetComponent<Light> ().color = ColorSol;
		ObjLuna.GetComponent<Light> ().color = ColorLuna;

		//Incremetador gradual de rotacion de PuntoCentro
		VelociadaBase = VelociadaBase + Incrementador;

		PuntoCentro.transform.rotation = Quaternion.Euler (VelociadaBase, 0f, 0f);

		//Apuntar a la terreno para hacer el efecto de luz direccional del sol y luna.
		ObjSol.transform.LookAt (PuntoCentro.transform);
		ObjLuna.transform.LookAt (PuntoCentro.transform);

		//Cambio de Interncia luminica del el sol y la luna en tiempo de ejecucion
		ObjSol.GetComponent<Light> ().intensity = IntensidadSol;
		ObjLuna.GetComponent<Light> ().intensity = IntensidadLuna;


		if (Lluvioso == true || Input.GetKeyDown(KeyCode.P)) {

			ParticulasNieve.Stop ();
			ParticulasLluvia.Play ();
		
		}

		if (Soleado == true || Input.GetKeyDown(KeyCode.O)) {

			ParticulasNieve.Stop ();
			ParticulasLluvia.Stop ();

		}

		if (Nevando == true || Input.GetKeyDown(KeyCode.I)) {
			
			ParticulasLluvia.Stop ();
			ParticulasNieve.Play ();

		}
			


	}
		
}
